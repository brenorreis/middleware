using System;
using Hangfire;
using System.Data;
using System.Text;
using System.Linq;
using SfCustom.Models;
using System.Net.Http;
using Newtonsoft.Json;
using NetCoreForce.Client;
using NetCoreForce.Models;
using Newtonsoft.Json.Linq;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using NetCoreForce.Client.Models;
using InnovaPharmaMiddleware.Models;
using InnovaPharmaMiddleware.Helpers;
using System.Text.RegularExpressions;
using InnovaPharmaMiddleware.Controllers;
using Microsoft.Extensions.Caching.Memory;

namespace innovaPharma_meddleware.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class SalesforceController : ControllerBase
    {
        private InnovaDbContext innovaDbContext;
        private ProdutoHelper produtoHelper;
        private IMemoryCache cache;

        public SalesforceController(InnovaDbContext sc, ProdutoHelper _produtoHelper, IMemoryCache _cache)
        {
            cache = _cache;
            innovaDbContext = sc;
            produtoHelper = _produtoHelper;
        }

        public HandlerController controller()
        {
            HandlerController controller = new HandlerController(innovaDbContext, produtoHelper, cache);
            return controller;
        }

        // #### METODOS PARA ACOES NO SALESFORCE ####
        [AutomaticRetry(Attempts = 0)]
        [HttpGet("upsertClientes")]
        public object SincronizarClientes()
        {
            string retorno = null;

            List<String> idsExternos = new List<String>();
            List<SfCustomAccount> clientesSF = new List<SfCustomAccount>();
            List<SfCustomIntegracao> integracoes = new List<SfCustomIntegracao>();
            
            try
            {
                var client = controller().GetForceClient();
                DateTime diaFixo = DateTime.Now.AddDays(-2);
                //int qtdMin = -30;
                //DateTime agora = System.DateTime.Now.AddMinutes(qtdMin);
                //DateTime hoje = System.DateTime.Today.AddMinutes(qtdMin);
                int qtdMin = -28;
                int qtdMinFim = -600;
                //DateTime agora = System.DateTime.Now.AddDays(qtdMin);
                DateTime hoje = System.DateTime.Today.AddDays(qtdMin);
                DateTime hojeFim = System.DateTime.Today.AddMinutes(qtdMinFim);
                //String hora = agora.Hour + agora.Minute.ToString().PadLeft(2, '0') + "00";
                //Int32 horaInt = Convert.ToInt32(hora);
                List<String> cpfsAdicionados = new List<String>();

                //List<CLIENTE_EQUILIBRIUM> dadosCliente = innovaDbContext.CLIENTE_EQUILIBRIUM.Where(cliente => ((cliente.DtAtualizaSaldo >= agora || cliente.Cadastro >= hoje || cliente.DtUltCompra >= hoje || (cliente.DataAtualizacao >= hoje && cliente.HoraAtualizacao >= horaInt))) && cliente.CPF_CNPJ != null).ToList();

                List<CLIENTE_EQUILIBRIUM> dadosCliente = innovaDbContext.CLIENTE_EQUILIBRIUM.Where(cliente => (cliente.DataAtualizacao >= hoje || cliente.DtAtualizaSaldo >= hoje || cliente.DtUltCompra >= hoje || cliente.Cadastro >= hoje) && cliente.CPF_CNPJ != null).ToList();

                //List<CLIENTE_EQUILIBRIUM> dadosCliente = innovaDbContext.CLIENTE_EQUILIBRIUM.Where(cliente => (cliente.CardCode == "C005035")).ToList();

                SObjectAttributes atributo = new SObjectAttributes();
                atributo.Type = "Account";
                
                SObjectAttributes atributoEnd = new SObjectAttributes();
                atributoEnd.Type = "Endereco__c";

                SObjectAttributes atributoContact = new SObjectAttributes();
                atributoContact.Type = "Contact";

                List<SfCidade> cidades = controller().GetCidades(client);
                List<SfSegmento> segmentos = controller().GetSegmentos(client);
                List<SfCustomUser> vendedores = controller().GetVendedores(client);
                List<Grupo_VDM> grupo = innovaDbContext.Grupo_VDM.ToList();
                List<SfEspecialidade> especialidades = controller().GetEspecialidades(client);
                List<SfCustomRecordType> tipoDeRegistro = controller().GetTipoDeRegistro(client);
                List<SfTransportadora> transportadoras = controller().GetTransportadoras(client);

                int qtdContas = 0;

                foreach (var dadoSAP in dadosCliente)
                {
                    try
                    {
                        var usuario = vendedores.FirstOrDefault(u => u.CodigoVendedorInt == dadoSAP.SlpCode || u.CodigoVendedorMadInt == dadoSAP.SlpCode);
                        
                        string idUser = null;

                        if(usuario == null){
                            /*string servico = "upsertClientes / "+dadoSAP.CardCode;            
                            if(!idsExternos.Contains(servico))
                            {
                                string mensagemErro ="Erro ao processar cliente \"" + dadoSAP.CardCode + "\" pois não possui usuário cadastrado/ativo com o SlpCode informado: " + dadoSAP.SlpCode;
                                SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                                integracoes.Add(integracao);
                                idsExternos.Add(servico);
                            }
                            continue;*/
                            idUser = vendedores.FirstOrDefault(u => u.CodigoVendedorInt == 1150).Id;
                        }else{
                            idUser = usuario.Id;
                        }

                        var cpfCnpjReplace = controller().RemoveCharSpecial(dadoSAP.CPF_CNPJ);

                        SfCustomAccount acc = new SfCustomAccount();
                        
                        acc.Attributes = atributo;
                        if (cpfCnpjReplace.Length > 11)
                        {   // Conta PJ
                            acc.Name = dadoSAP.CardName;
                            acc.Celular = dadoSAP.Fone_Cel;
                            acc.TipoDeRegistro = tipoDeRegistro.FirstOrDefault(t => t.IsPersonType == false).Id;
                        }
                        else
                        {   // Conta PF
                            acc.Email = controller().getEmail(dadoSAP.Email);
                            acc.CelularPessoal = dadoSAP.Fone_Cel;
                            int quantNome = dadoSAP.CardName.Split(" ").Length;
                            string[] nomeCompleto = dadoSAP.CardName.Split(" ");
                            acc.TipoDeRegistro = tipoDeRegistro.FirstOrDefault(t => t.IsPersonType == true).Id;
                            if(quantNome == 1)
                            {
                                acc.Sobrenome = dadoSAP.CardName;
                            }else if (quantNome >= 2)
                            {
                                acc.PrimeiroNome = nomeCompleto[0];
                                acc.MeioNome = "";
                                string restoNome = null;
                                for(int i = 1; i < quantNome; i++)
                                {
                                    restoNome = restoNome + " " + nomeCompleto[i];  
                                }
                                acc.Sobrenome = restoNome;
                            }
                        }

                        //Dados Básicos
                        acc.Rede = dadoSAP.Rede;
                        acc.CNPJCPF = cpfCnpjReplace;
                        acc.Suframa = dadoSAP.SUFRAMA;
                        acc.Fantasia = dadoSAP.CardFName;
                        acc.InscricaoMunicipal = dadoSAP.IM;
                        acc.Medicamento__c = dadoSAP.Medicamento == "S" ? "Sim" : "Não";
                        acc.Produto_Saude__c = dadoSAP.ProdSaude == "S" ? "Sim" : "Não";
                        acc.GrupoCliente = grupo.FirstOrDefault(g => g.GroupCode == dadoSAP.Grupo_Cliente).GroupName;
                        // Dados de Contato
                        acc.Fax = dadoSAP.Fone_2;
                        acc.Phone = dadoSAP.Fone_1;
                        acc.Website = dadoSAP.Site;
                        acc.EmailXML = controller().getEmail(dadoSAP.Email);
                        // Dados Financeiros
                        //acc.Saldo = dadoSAP.Saldo;
                        acc.SaldoSAP = dadoSAP.Saldo;
                        acc.LimiteCredito = dadoSAP.CreditLine;
                        acc.SubstitudoTributario = dadoSAP.SUBS_TRIB;
                        // Informações adicionais
                        acc.OwnerId = idUser;
                        acc.Sincronizado = true;
                        acc.CardCode = dadoSAP.CardCode;
                        acc.NrAlvara = dadoSAP.NR_ALVARA;
                        acc.VIP = dadoSAP.VIP == "1";
                        acc.DataAlvara = dadoSAP.DATA_ALVARA;
                        acc.PontosScore = dadoSAP.PontosScore;
                        acc.DataAbertura = dadoSAP.DataAbertura;
                        acc.Distribuidor = dadoSAP.Distribuidor;
                        acc.CapitalSocial = dadoSAP.CapitalSocial;
                        acc.DataUltimaCompra = dadoSAP.DtUltCompra;
                        acc.Indicador_da_IE__c = dadoSAP.IndicadorDaIE;
                        acc.Simples_Nacional__c = dadoSAP.SimplesNacional;
                        acc.ObservacoesVendedor = dadoSAP.Observacao_venda;
                        acc.Indicador_de_Natureza__c = dadoSAP.IndicadorNatureza;
                        acc.Indicador_de_Op_Consumidor__c = dadoSAP.IndicadorDeOpConsumidor;
                        acc.Description = dadoSAP.Free_Text != null? dadoSAP.Free_Text.Length < 32000 ? dadoSAP.Free_Text : dadoSAP.Free_Text.Substring(0, 32000) : null;
                        if(!String.IsNullOrEmpty(dadoSAP.SituacaoIE.ToString())){acc.SituacaoIE = Convert.ToString(dadoSAP.SituacaoIE);}
                        if(!String.IsNullOrEmpty(dadoSAP.PorteEmpresa.ToString())){acc.PorteEmpresa = Convert.ToString(dadoSAP.PorteEmpresa);}
                        if(!String.IsNullOrEmpty(dadoSAP.DescricaoNatureza.ToString())){acc.DescricaoNatureza = Convert.ToString(dadoSAP.DescricaoNatureza);}
                        if(!String.IsNullOrEmpty(dadoSAP.SituacaoCadastralCnpj.ToString())){acc.SituacaoCadastralCnpj = Convert.ToString(dadoSAP.SituacaoCadastralCnpj);}
                        if(!String.IsNullOrEmpty(dadoSAP.CategoriaClientes.ToString())){acc.CategoriaClientes = Convert.ToString(dadoSAP.CategoriaClientes);}

                        acc.AceitaEmailMarketing = dadoSAP.U_Recebe_Email_Marketing == "S" ? "Sim" : "Não";
                        acc.ExclusaoLgpd = dadoSAP.U_Exclusao_Lgpd == "S" ? "Sim" : "Não";

                        acc.ObservacoesSerasa = dadoSAP.ObservacaoSerasa;
                        acc.DataSerasa = dadoSAP.DataSerasa;
                        if(dadoSAP.StatusSerasa.HasValue)
                            acc.StatusSerasa = dadoSAP.StatusSerasa.Value == 0 ? "Positivo" : "Negativo";

                        if(dadoSAP.CodigoTransportadora != null && transportadoras.FirstOrDefault(t => t.CardCode == dadoSAP.CodigoTransportadora) != null){
                            acc.Transportadora = transportadoras.FirstOrDefault(t => t.CardCode == dadoSAP.CodigoTransportadora).Id;
                        }
                        if (segmentos.Any(s => s.Codigo == dadoSAP.CodSegmento))
                        {
                            acc.Segmento = segmentos.FirstOrDefault(s => s.Codigo == dadoSAP.CodSegmento).Id;
                        }

                        if (especialidades.Any(e => e.Codigo == dadoSAP.CodEspecialidade))
                        {
                            acc.Especialidade = especialidades.FirstOrDefault(e => e.Codigo == dadoSAP.CodEspecialidade).Id;
                        }
                        if (dadoSAP.IE == null)
                        {
                            acc.TipoIE = "Isento";
                        }
                        else
                        {
                            if (dadoSAP.IE.Contains("Isento"))
                            {
                                acc.TipoIE = dadoSAP.IE;
                            }
                            else
                            {
                                acc.TipoIE = "Contribuinte";
                                acc.InscricaoEstadual = dadoSAP.IE;
                            }
                        }

                        if(!cpfsAdicionados.Contains(cpfCnpjReplace)){
                            clientesSF.Add(acc);
                            cpfsAdicionados.Add(cpfCnpjReplace);
                            qtdContas = qtdContas + 1;
                        }

                        if(qtdContas == 200){
                            var accountJSON = JsonConvert.SerializeObject(clientesSF, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                            var resultContas = controller().UpsertDados(accountJSON, "Account", "CNPJ_CPF__c", "upsertClientes");
                            //var resultJSON = JsonConvert.DeserializeObject(resultContas.ToString());
                            qtdContas = 0;
                            clientesSF = new List<SfCustomAccount>();
                        }
                    }
                    catch(Exception ex)
                    {
                        string servico = "upsertClientes / "+dadoSAP.CardCode;
                        if(!idsExternos.Contains(servico))
                        {
                            var message = ex.InnerException != null ? ex.InnerException.Message : null;
                            string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                            SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                            integracoes.Add(integracao);
                            idsExternos.Add(servico);
                        }
                        continue;
                    }
                }

                var accountsJSON = JsonConvert.SerializeObject(clientesSF, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultado = controller().UpsertDados(accountsJSON, "Account", "CNPJ_CPF__c", "upsertClientes");

                /*List<SfEndereco> enderecosSF = new List<SfEndereco>();

                int qtdEndereco = 0;

                cpfsAdicionados = new List<string>();

                foreach (var dadoSAP in dadosCliente)
                {
                    try
                    {
                        var cpfCnpjReplace = RemoveCharSpecial(dadoSAP.CPF_CNPJ);

                        var idConta = ""; 
                        var Conta = innovaDbContext.ClienteNew.FirstOrDefault(c => c.CliCnpj == dadoSAP.CPF_CNPJ || c.CliCpf == dadoSAP.CPF_CNPJ);
                        var contaLog = innovaDbContext.LogCadastro.FirstOrDefault(c => c.CardCode_Cliente == dadoSAP.CardCode);

                        if(Conta != null){
                            idConta = Conta.CliSf;
                        }else if(contaLog != null){
                            idConta = contaLog.Id_SF_Cliente;
                        } else {
                            continue;
                        }
                        
                        SfEndereco endEntrega = new SfEndereco();
                        endEntrega.Conta = idConta;
                        endEntrega.Bairro = dadoSAP.Setor;
                        endEntrega.Rua = dadoSAP.Endereco;
                        endEntrega.Attributes = atributoEnd;
                        endEntrega.CardCode = cpfCnpjReplace;
                        endEntrega.Numero = dadoSAP.Endereco_Nr;
                        endEntrega.Tipo = "Endereço de Entrega";
                        endEntrega.Name = "Endereço de Entrega";
                        endEntrega.Complemento = dadoSAP.Endereco_Comp.Length > 100 ? dadoSAP.Endereco_Comp.Substring(0, 100) : dadoSAP.Endereco_Comp;

                        if (dadoSAP.CEP != null){
                            var cep = dadoSAP.CEP.Replace(".", "").Replace(" ", "");
                            endEntrega.CEP = cep.Length > 9 ? cep.Substring(0, 9) : endEntrega.CEP = cep;
                        }

                        if (dadoSAP.CIDADE_CODIGO != "" && dadoSAP.CIDADE_CODIGO != "0" && dadoSAP.CIDADE_CODIGO != null){
                            endEntrega.CidadePesquisa = cidades.FirstOrDefault(c => c.Codigo == dadoSAP.CIDADE_CODIGO).Id;
                        }
                        
                        if(!cpfsAdicionados.Contains(cpfCnpjReplace)){
                            enderecosSF.Add(endEntrega);
                            cpfsAdicionados.Add(cpfCnpjReplace);
                            qtdEndereco = qtdEndereco + 1;
                        }

                        if(qtdEndereco == 200){
                            var enderecosEntrega = JsonConvert.SerializeObject(enderecosSF, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                            var resultEndereco = controller().UpsertDados(enderecosEntrega, "Endereco__c", "CardCode__c", "upsertEndereco");
                            qtdEndereco = 0;
                            enderecosSF = new List<SfEndereco>();
                        }
                    }
                    catch(Exception ex)
                    {
                        string servico = "upsertEndereco / "+dadoSAP.CardCode;
                        if(!idsExternos.Contains(servico))
                        {
                            var message = ex.InnerException != null ? ex.InnerException.Message : null;
                            string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                            SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                            integracoes.Add(integracao);
                            idsExternos.Add(servico);
                        }
                        continue;
                    }
                }
                
                var enderecosJSON = JsonConvert.SerializeObject(enderecosSF, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultados = controller().UpsertDados(enderecosJSON, "Endereco__c", "CardCode__c", "upsertEndereco");

                List<SfCustomContact> contatosSF = new List<SfCustomContact>();

                int qtdContatos = 0;
                
                cpfsAdicionados = new List<string>();

                foreach (var dadoSAP in dadosCliente)
                {
                    try
                    {
                        var usuario = vendedores.FirstOrDefault(u => u.CodigoVendedorInt == dadoSAP.SlpCode || u.CodigoVendedorMadInt == dadoSAP.SlpCode);
                        
                        string idUser = null;

                        if(usuario == null){
                            /*string servico = "upsertClientes / "+dadoSAP.CardCode;            
                            if(!idsExternos.Contains(servico))
                            {
                                string mensagemErro ="Erro ao processar cliente \"" + dadoSAP.CardCode + "\" pois não possui usuário cadastrado/ativo com o SlpCode informado: " + dadoSAP.SlpCode;
                                SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                                integracoes.Add(integracao);
                                idsExternos.Add(servico);
                            }
                            continue;
                            idUser = vendedores.FirstOrDefault(u => u.CodigoVendedorInt == 1150).Id;
                        }else{
                            idUser = usuario.Id;
                        }

                        var cpfCnpjReplace = RemoveCharSpecial(dadoSAP.CPF_CNPJ);

                        var idConta = ""; 
                        var Conta = innovaDbContext.ClienteNew.FirstOrDefault(c => c.CliCnpj == dadoSAP.CPF_CNPJ || c.CliCpf == dadoSAP.CPF_CNPJ);
                        var contaLog = innovaDbContext.LogCadastro.FirstOrDefault(c => c.CardCode_Cliente == dadoSAP.CardCode);

                        if(Conta != null){
                            idConta = Conta.CliSf;
                        }else if(contaLog != null){
                            idConta = contaLog.Id_SF_Cliente;
                        } else {
                            continue;
                        }
                        
                        if (cpfCnpjReplace.Length > 11 && dadoSAP.Contato != "Sem Contato")
                        {
                            SfCustomContact cont = new SfCustomContact();

                            if(dadoSAP.Contato.Length > 50) {continue;}
                            cont.AccountId = idConta;
                            cont.OwnerId = idUser;
                            cont.Fax = dadoSAP.Fone_2;
                            cont.Phone = dadoSAP.Fone_1;
                            cont.CardCode = dadoSAP.CardCode;
                            cont.Attributes = atributoContact;
                            cont.MobilePhone = dadoSAP.Fone_Cel;
                            cont.Email = getEmail(dadoSAP.Email);
                            int quantNome = dadoSAP.Contato.Split(" ").Length;
                            string[] nomeCompleto = dadoSAP.Contato.Split(" ");
                            if(quantNome == 1)
                            {
                                cont.LastName = dadoSAP.CardName;
                            }else if (quantNome >= 2)
                            {
                                cont.FirstName = nomeCompleto[0];
                                string restoNome = null;
                                for(int i = 1; i < quantNome; i++)
                                {
                                    restoNome = restoNome + " " + nomeCompleto[i];  
                                }
                                cont.LastName = restoNome;
                            }

                            if(!cpfsAdicionados.Contains(dadoSAP.CardCode)){
                                contatosSF.Add(cont);
                                cpfsAdicionados.Add(dadoSAP.CardCode);
                                qtdContatos = qtdContatos + 1;
                            }

                            if(qtdContatos == 200){
                                var contatos = JsonConvert.SerializeObject(contatosSF, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                                var resultContato = controller().UpsertDados(contatos, "Contact", "CardCode__c", "upsertContatos");
                                qtdContatos = 0;
                                contatosSF = new List<SfCustomContact>();
                            }
                        }
                    }
                    catch(Exception ex)
                    {
                        string servico = "upsertContatos / "+dadoSAP.CardCode;
                        if(!idsExternos.Contains(servico))
                        {
                            var message = ex.InnerException != null ? ex.InnerException.Message : null;
                            string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                            SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                            integracoes.Add(integracao);
                            idsExternos.Add(servico);
                        }
                        continue;
                    }
                }
                if(contatosSF.Count() > 0){
                    var contatosJSON = JsonConvert.SerializeObject(contatosSF, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                    var resultadoContatos = controller().UpsertDados(contatosJSON, "Contact", "CardCode__c", "upsertContatos");
                }*/
            }
            catch (Exception ex)
            {
                string servico = "upsertClientes";
                if(!idsExternos.Contains(servico))
                {
                    var message = ex.InnerException != null ? ex.InnerException.Message : null;
                string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                    SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                    integracoes.Add(integracao);
                    idsExternos.Add(servico);
                }
            }

            if(integracoes.Any()){
                var integracaoJSON = JsonConvert.SerializeObject(integracoes, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultadoIntegracao = controller().UpsertDados(integracaoJSON, "Integracao__c", "Id_Externo__c", "upsertContatos");
            }
            
            return retorno;
        }

        [AutomaticRetry(Attempts = 0)]
        [HttpGet("updateClienteNovo")]
        public void SincronizarClienteNovo()
        {
            List<String> idsExternos = new List<String>();
            List<SfCustomAccount> clientesSF = new List<SfCustomAccount>();
            List<SfCustomIntegracao> integracoes = new List<SfCustomIntegracao>();

            SObjectAttributes atributo = new SObjectAttributes();
            atributo.Type = "Account";

            try
            {
                var clientes = innovaDbContext.ClienteNew.Where(cliente => (cliente.CliStatusDta >= System.DateTime.Now.AddHours(-1))).ToList();

                var client = controller().GetForceClient();

                var qtdClientes = 0;

                foreach (var cliente in clientes)
                {
                    try
                    {
                        SfCustomAccount conta = new SfCustomAccount();

                        conta.Id = cliente.CliSf;
                        conta.Sincronizado = true;
                        conta.Attributes = atributo;
                        conta.CardCode = cliente.CliIdOrg;
                        conta.StatusSAP = cliente.CliObsImp;

                        clientesSF.Add(conta);

                        qtdClientes = qtdClientes + 1;

                        if(qtdClientes == 200){
                            var accountJSON = JsonConvert.SerializeObject(clientesSF, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                            var resultContas = controller().UpsertDados(accountJSON, "Account", "Id", "updateNovosClientes");
                            qtdClientes = 0;
                            clientesSF = new List<SfCustomAccount>();
                        }

                    }
                    catch(Exception ex)
                    {
                        string servico = "updateNovosClientes / "+ cliente.CliIdOrg;
                        if(!idsExternos.Contains(servico))
                        {
                            var message = ex.InnerException != null ? ex.InnerException.Message : null;
                string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                            SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                            integracoes.Add(integracao);
                            idsExternos.Add(servico);
                        }
                        continue;
                    }
                }

                var accountsJSON = JsonConvert.SerializeObject(clientesSF, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultado = controller().UpsertDados(accountsJSON, "Account", "Id", "updateNovosClientes");
            }
            catch(Exception ex)
            {
                string servico = "updateNovosClientes";
                if(!idsExternos.Contains(servico))
                {
                    var message = ex.InnerException != null ? ex.InnerException.Message : null;
                string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                    SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                    integracoes.Add(integracao);
                    idsExternos.Add(servico);
                }
            }

            if(integracoes.Any()){
                var integracaoJSON = JsonConvert.SerializeObject(integracoes, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultadoIntegracao = controller().UpsertDados(integracaoJSON, "Integracao__c", "Id_Externo__c", "updateNovosClientes");
            }

        }
        
        [AutomaticRetry(Attempts = 0)]
        [HttpGet("upsertProdutos")]
        public void SincronizarProduto()
        {
            List<string> codigos = new List<string>();
            List<String> idsExternos = new List<String>();
            List<SfCustomProduct2> produtosSF = new List<SfCustomProduct2>();
            List<SfCustomIntegracao> integracoes = new List<SfCustomIntegracao>();
            
            SObjectAttributes atributoProduto = new SObjectAttributes();
            atributoProduto.Type = "Product2";

            try{
                var qtdProdutos = 0;

                var client = controller().GetForceClient();
                List<SfProduto> produtos = controller().GetProdutos(client);
                
                foreach (var produtoSF in produtos){
                    codigos.Add(produtoSF.Codigo);
                }

                List<Itens_VDM> produtosDB = innovaDbContext.Itens_VDM.Where(p => codigos.Contains(p.ItemCodeVida)).ToList();

                foreach (var dadoSAP in produtosDB)
                {
                    try
                    {
                        SfCustomProduct2 prod = new SfCustomProduct2();

                        prod.IsActive = true;
                        prod.Name = dadoSAP.ItemName;
                        prod.Empresa = dadoSAP.Empresas;
                        prod.Attributes = atributoProduto;
                        prod.Description = dadoSAP.ItemName;
                        prod.ProductCode = dadoSAP.ItemCodeVida;
                        prod.ItemCodeVida = dadoSAP.ItemCodeVida;
                        prod.AlvaraClasse = dadoSAP.U_AlvaraClasse;
                        prod.Family = produtoHelper.GetFamilia(dadoSAP.U_Familia);
                        prod.Estoque = dadoSAP.Empresas == "SBOInnovaLab" ? 1000 : dadoSAP.EstoqueCD;
                        prod.EstoqueEvento = dadoSAP.Estoque_Evento;
                        prod.EstoqueNavarro = dadoSAP.Estoque_Navarro;
                        prod.Segmento__c = dadoSAP.Secao;
                        prod.Deposito_Evento__c = dadoSAP.CodigoDepositoEvento;
                        prod.Deposito_Revenda__c = dadoSAP.CodigoDepositoRevenda;
                        prod.Deposito_Navarro__c = dadoSAP.CodigoDepositoNavarro;
                        prod.Categoria__c = dadoSAP.Categoria;

                        produtosSF.Add(prod);

                        qtdProdutos = qtdProdutos + 1;

                        if(qtdProdutos == 20){
                            var produtoJSON = JsonConvert.SerializeObject(produtosSF, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                            var resultContas = controller().UpsertDados(produtoJSON, "Product2", "ItemCodeVida__c", "upsertProdutos");
                            qtdProdutos = 0;
                            produtosSF = new List<SfCustomProduct2>();
                        }

                    }
                    catch(Exception ex)
                    {
                        string servico = "upsertProdutos / "+dadoSAP.ItemCodeVida;
                        if(!idsExternos.Contains(servico))
                        {
                            var message = ex.InnerException != null ? ex.InnerException.Message : null;
                            string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                            SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                            integracoes.Add(integracao);
                            idsExternos.Add(servico);
                        }
                        continue;
                    }
                }

                var produtosJSON = JsonConvert.SerializeObject(produtosSF, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultado = controller().UpsertDados(produtosJSON, "Product2", "ItemCodeVida__c", "upsertProdutos");
            }
            catch(Exception ex)
            {
                string servico = "sincronizarProdutos";
                if(!idsExternos.Contains(servico))
                {
                    var message = ex.InnerException != null ? ex.InnerException.Message : null;
                string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                    SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                    integracoes.Add(integracao);
                    idsExternos.Add(servico);
                }
            }

            if(integracoes.Any()){
                var integracaoJSON = JsonConvert.SerializeObject(integracoes, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultadoIntegracao = controller().UpsertDados(integracaoJSON, "Integracao__c", "Id_Externo__c", "upsertProdutos");
            }
        }

        [AutomaticRetry(Attempts = 0)]
        [HttpGet("upsertStatusPedido")]
        public void SincronizarStatusPedido()
        {
            List<String> idsExternos = new List<String>();
            List<SfCustomIntegracao> integracoes = new List<SfCustomIntegracao>();
            List<SfCustomOpportunity> oportunidadesSF = new List<SfCustomOpportunity>();
            List<SfCustomOpportunity> oportunidadesAll = new List<SfCustomOpportunity>();

            SObjectAttributes atributoOpportunity = new SObjectAttributes();
            atributoOpportunity.Type = "Opportunity";

            try
            {
                int qtd = -30;
                int qtdOportunidades = 0;
                string codigosOportunidades = "";

                var client = controller().GetForceClient();
                var dataAtualizacao = System.DateTime.Now.AddMinutes(qtd);

                var pedidos = innovaDbContext.StatusPedidoSap.Where(pedido => (pedido.DataAtualizacao >= dataAtualizacao || pedido.DataAtualizacaoNota >= dataAtualizacao)).ToList();

                foreach (var item in pedidos)
                {
                    var codigo = item.PedidoSales.PadLeft(7, '0');
                    if(qtdOportunidades < 1)
                    {
                        codigosOportunidades = "'" + codigo + "'";
                    }
                    else
                    {
                        codigosOportunidades = codigosOportunidades + ", '" + codigo + "'";
                    }
                    
                    qtdOportunidades++;

                    if(qtdOportunidades == 200){
                        var oportunidades = client.Query<SfCustomOpportunity>(SfCustomOpportunity.BuildSelect() + " WHERE Codigo__c in (" + codigosOportunidades + ")").Result;
                        oportunidadesAll.AddRange(oportunidades);
                        codigosOportunidades = "";
                        qtdOportunidades = 0;
                    }
                }
                qtdOportunidades = 0;

                if(codigosOportunidades.Any()){
                    var oportunidades = client.Query<SfCustomOpportunity>(SfCustomOpportunity.BuildSelect() + " WHERE Codigo__c in (" + codigosOportunidades + ")").Result;
                    oportunidadesAll.AddRange(oportunidades);
                    codigosOportunidades = "";
                    qtdOportunidades = 0;
                }

                if(oportunidadesAll.Any()){
                    foreach (var pedido in pedidos)
                    {
                        try
                        {
                            var codigo = pedido.PedidoSales.PadLeft(7, '0');
                            var codigoNum = Decimal.Parse(codigo);
                            SfCustomOpportunity oportunidadeSF = oportunidadesAll.FirstOrDefault(o => o.CodigoSf == codigoNum);
                            string idOpp = oportunidadeSF.Id;

                            SfCustomOpportunity oportunidade = new SfCustomOpportunity();
                            oportunidade.Id = idOpp;
                            oportunidade.bypass__c = true;
                            oportunidade.Sincronizado = true;
                            oportunidade.nNF = pedido.Danfe != null? pedido.Danfe.ToString() : null;
                            oportunidade.ChaveNF = pedido.Chave;
                            oportunidade.Codigo_SAP__c = pedido.PedidoSap!= null? pedido.PedidoSap.ToString() : null;
                            oportunidade.Attributes = atributoOpportunity;
                            oportunidade.StatusPedido = pedido.StatusPedido;
                            oportunidade.StatusSAP = pedido.StatusTabelaPEdido;
                            oportunidade.StatusPedidoSAP = pedido.StatusPedidoSap2;
                            oportunidade.Status_Integracao__c = "Importado";
                            if(pedido.StatusPedidoSap2 == "CANCELADO"){
                                oportunidade.StageName = "Cancelado";
                                oportunidade.Financeiro__c = "";
                                oportunidade.Dep_Desconto__c = "";
                                oportunidade.Dep_Estoque__c = "";
                            }

                            oportunidadesSF.Add(oportunidade);
                            qtdOportunidades = qtdOportunidades + 1;

                            if(qtdOportunidades == 50){
                                var oportunidadeJSON = JsonConvert.SerializeObject(oportunidadesSF, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                                var resultOportunidade = controller().UpsertDados(oportunidadeJSON, "Opportunity", "Id", "upsertStatusPedido");
                                qtdOportunidades = 0;
                                oportunidadesSF = new List<SfCustomOpportunity>();
                            }
                        }
                        catch(Exception ex)
                        {
                            string servico = "upsertStatusPedido / "+ pedido.PedidoSales;
                            if(!idsExternos.Contains(servico))
                            {
                                var message = ex.InnerException != null ? ex.InnerException.Message : null;
                                string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                                SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                                integracoes.Add(integracao);
                                idsExternos.Add(servico);
                            }
                            continue;
                        }
                    }
                    var oportunidadesJSON = JsonConvert.SerializeObject(oportunidadesSF, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                    var resultOportunidades = controller().UpsertDados(oportunidadesJSON, "Opportunity", "Id", "upsertStatusPedido");
                }
            }
            catch(Exception ex)
            {
                string servico = "upsertStatusPedido";
                if(!idsExternos.Contains(servico))
                {
                    var message = ex.InnerException != null ? ex.InnerException.Message : null;
                string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                    SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                    integracoes.Add(integracao);
                    idsExternos.Add(servico);
                }
            }

            if(integracoes.Any()){
                var integracaoJSON = JsonConvert.SerializeObject(integracoes, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultadoIntegracao = controller().UpsertDados(integracaoJSON, "Integracao__c", "Id_Externo__c", "upsertStatusPedido");
            }
        }

        [AutomaticRetry(Attempts = 0)]
        [HttpGet("upsertPagamentos")]
        public void SincronizarPagamentos()
        {
            List<SfPagamento> pagamentosSF = new List<SfPagamento>();
            List<SfCustomIntegracao> integracoes = new List<SfCustomIntegracao>();

            SObjectAttributes atributoPagamento = new SObjectAttributes();
            atributoPagamento.Type = "Pagamento__c";
            
            SObjectAttributes atributoIntegracao = new SObjectAttributes();
            atributoIntegracao.Type = "Integracao__c";

            try
            {
                int qtdDias = -1;
                int qtdPagamentos = 0;

                var client = controller().GetForceClient();
                DateTime? dateTime = new DateTime(1900, 01, 01, 0, 0, 0);
                var contaAReceber = innovaDbContext.GetContasReceberEQ.Where(c => c.CriacaoBaixa >= System.DateTime.Today.AddDays(qtdDias) || c.Emissao >= System.DateTime.Today.AddDays(qtdDias) || c.Baixa >= System.DateTime.Today.AddDays(qtdDias)).ToList();
                //var contaAReceber = innovaDbContext.GetContasReceberEQ.Where(c => c.CodigoCliente == "C012301").ToList();
                List<SfCustomUser> vendedores = controller().GetVendedores(client);

                foreach (var pagamento in contaAReceber)
                {
                    try
                    {
                        /*var idConta = ""; 
                        var Conta = innovaDbContext.LogCadastro.FirstOrDefault(c => c.CardCode_Cliente == pagamento.CodigoCliente);

                        if(Conta != null){
                            idConta = Conta.Id_SF_Cliente;
                        }else{
                            continue;
                        }*/

                        var usuario = vendedores.FirstOrDefault(u => u.CodigoVendedor == pagamento.CodigoVendedor || u.CodigoVendedorMed == pagamento.CodigoVendedor);
                        if(usuario == null){
                            usuario = vendedores.FirstOrDefault(u => u.CodigoVendedor == 916 || u.CodigoVendedorMed == 916);
                        }

                        SfPagamento pag = new SfPagamento();
                        pag.OwnerId = usuario.Id;
                        pag.Status = pagamento.Status;
                        pag.Name = pagamento.Documento;
                        pag.Emissao = pagamento.Emissao;
                        pag.ContaRelacionamento = new SfCustomAccount();
                        pag.ContaRelacionamento.CardCode = pagamento.CodigoCliente;
                        pag.NF = pagamento.NF.ToString();
                        pag.Attributes = atributoPagamento;
                        string idExterno = pagamento.Filial + pagamento.Documento;
                        pag.Documento = idExterno;
                        pag.Vencimento = pagamento.Vencimento;
                        pag.ValorTitulo = pagamento.ValorTitulo;
                        pag.Empresa = pagamento.Filial.ToUpper();
                        pag.Codigo_do_Vendedor = pagamento.CodigoVendedor.ToString();
                        pag.Baixa = pagamento.Baixa != dateTime ? pagamento.Baixa : null;

                        pagamentosSF.Add(pag);
                        qtdPagamentos = qtdPagamentos + 1;

                        if(qtdPagamentos == 200){
                            var pagamentos = JsonConvert.SerializeObject(pagamentosSF, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                            var resultPagamento = controller().UpsertDados(pagamentos, "Pagamento__c", "Documento__c", "upsertPagamentos");
                            qtdPagamentos = 0;
                            pagamentosSF = new List<SfPagamento>();
                        }
                    }
                    catch(Exception ex)
                    {
                        string servico = "upsertPagamentos / "+ pagamento.CodigoCliente + " / " + pagamento.Documento;
                        SfCustomIntegracao integracao = new SfCustomIntegracao();
                        integracao.Metodo = servico;
                        integracao.IdExterno = servico;
                        integracao.Mensagem = ex.Message;
                        integracao.Attributes = atributoIntegracao;
                        integracoes.Add(integracao);
                        continue;
                    }
                }
                var pagamentosAll = JsonConvert.SerializeObject(pagamentosSF, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultPagamentos = controller().UpsertDados(pagamentosAll, "Pagamento__c", "Documento__c", "upsertPagamentos");
            }
            catch(Exception ex)
            {
                string servico = "upsertPagamentos";
                SfCustomIntegracao integracao = new SfCustomIntegracao();
                integracao.Metodo = servico;
                integracao.IdExterno = servico;
                integracao.Mensagem = ex.Message;
                integracao.Attributes = atributoIntegracao;
                integracoes.Add(integracao);
            }

            if(integracoes.Any()){
                var integracaoJSON = JsonConvert.SerializeObject(integracoes, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultadoIntegracao = controller().UpsertDados(integracaoJSON, "Integracao__c", "Id_Externo__c", "upsertPagamentos");
            }
        }

        [AutomaticRetry(Attempts = 0)]
        [HttpGet("upsertPagamentosAjuste")]
        public void SincronizarPagamentosAjuste()
        {
            List<SfPagamento> pagamentosSF = new List<SfPagamento>();
            List<SfCustomIntegracao> integracoes = new List<SfCustomIntegracao>();

            SObjectAttributes atributoPagamento = new SObjectAttributes();
            atributoPagamento.Type = "Pagamento__c";
            
            SObjectAttributes atributoIntegracao = new SObjectAttributes();
            atributoIntegracao.Type = "Integracao__c";


            try
            {
                int qtdPagamentos = 0;

                var client = controller().GetForceClient();
                DateTime? dateTime = new DateTime(1900, 01, 01, 0, 0, 0);
                //var contaAReceber = innovaDbContext.GetContasReceberEQ.Where(c => c.CriacaoBaixa >= System.DateTime.Today.AddDays(qtdDias) || c.Emissao >= System.DateTime.Today.AddDays(qtdDias) || c.Baixa >= System.DateTime.Today.AddDays(qtdDias)).ToList();

                List<SfPagamento> pagamentosTeste = client.Query<SfPagamento>(SfPagamento.BuildSelect() + " WHERE Empresa__c = 'SBOINNOVABR' AND Status__c = 'ABERTO' AND LastModifiedDate <= 2023-07-28T00:00:00.000+0000 order by createddate asc limit 1000").Result;

                List<string> nfs = new List<string>();

                foreach (var item in pagamentosTeste)
                {
                    if(!nfs.Contains(item.Name)){
                        nfs.Add(item.Name);
                    }
                }

                var contaAReceber = innovaDbContext.GetContasReceberEQ.Where(c => c.Filial == "SBOInnovaBR" && nfs.Contains(c.Documento)).ToList();
                List<SfCustomUser> vendedores = controller().GetVendedores(client);

                List<string> idsExterno = new List<string>();

                foreach (var pagamento in contaAReceber)
                {
                    try
                    {
                        /*var idConta = ""; 
                        var Conta = innovaDbContext.LogCadastro.FirstOrDefault(c => c.CardCode_Cliente == pagamento.CodigoCliente);

                        if(Conta != null){
                            idConta = Conta.Id_SF_Cliente;
                        }else{
                            continue;
                        }*/

                        var usuario = vendedores.FirstOrDefault(u => u.CodigoVendedor == pagamento.CodigoVendedor || u.CodigoVendedorMed == pagamento.CodigoVendedor);
                        if(usuario == null){
                            usuario = vendedores.FirstOrDefault(u => u.CodigoVendedor == 916 || u.CodigoVendedorMed == 916);
                        }

                        SfPagamento pag = new SfPagamento();
                        pag.OwnerId = usuario.Id;
                        pag.Status = pagamento.Status;
                        pag.Name = pagamento.Documento;
                        pag.Emissao = pagamento.Emissao;
                        pag.ContaRelacionamento = new SfCustomAccount();
                        pag.ContaRelacionamento.CardCode = pagamento.CodigoCliente;
                        pag.NF = pagamento.NF.ToString();
                        pag.Attributes = atributoPagamento;
                        string idExterno = pagamento.Filial + pagamento.Documento;
                        pag.Documento = idExterno;
                        pag.Vencimento = pagamento.Vencimento;
                        pag.ValorTitulo = pagamento.ValorTitulo;
                        pag.Empresa = pagamento.Filial.ToUpper();
                        pag.Codigo_do_Vendedor = pagamento.CodigoVendedor.ToString();
                        pag.Baixa = pagamento.Baixa != dateTime ? pagamento.Baixa : null;

                        if(!idsExterno.Contains(idExterno)){
                            pagamentosSF.Add(pag);
                            qtdPagamentos = qtdPagamentos + 1;
                            idsExterno.Add(idExterno);
                        }

                        if(qtdPagamentos == 200){
                            var pagamentos = JsonConvert.SerializeObject(pagamentosSF, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                            var resultPagamento = controller().UpsertDados(pagamentos, "Pagamento__c", "Documento__c", "upsertPagamentos");
                            qtdPagamentos = 0;
                            pagamentosSF = new List<SfPagamento>();
                        }
                    }
                    catch(Exception ex)
                    {
                        string servico = "upsertPagamentos / "+ pagamento.CodigoCliente + " / " + pagamento.Documento;
                        SfCustomIntegracao integracao = new SfCustomIntegracao();
                        integracao.Metodo = servico;
                        integracao.IdExterno = servico;
                        integracao.Mensagem = ex.Message;
                        integracao.Attributes = atributoIntegracao;
                        integracoes.Add(integracao);
                        continue;
                    }
                }
                var pagamentosAll = JsonConvert.SerializeObject(pagamentosSF, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultPagamentos = controller().UpsertDados(pagamentosAll, "Pagamento__c", "Documento__c", "upsertPagamentos");
            }
            catch(Exception ex)
            {
                string servico = "upsertPagamentos";
                SfCustomIntegracao integracao = new SfCustomIntegracao();
                integracao.Metodo = servico;
                integracao.IdExterno = servico;
                integracao.Mensagem = ex.Message;
                integracao.Attributes = atributoIntegracao;
                integracoes.Add(integracao);
            }

            if(integracoes.Any()){
                var integracaoJSON = JsonConvert.SerializeObject(integracoes, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultadoIntegracao = controller().UpsertDados(integracaoJSON, "Integracao__c", "Id_Externo__c", "upsertPagamentos");
            }
        }
        
        [AutomaticRetry(Attempts = 0)]
        [HttpGet("deletePagamentosCancelados")]
        public void DeletarPagamentosCancelados()
        {
            List<SfPagamento> pagamentosSF = new List<SfPagamento>();
            List<SfCustomIntegracao> integracoes = new List<SfCustomIntegracao>();
            
            SObjectAttributes atributoIntegracao = new SObjectAttributes();
            atributoIntegracao.Type = "Integracao__c";

            try
            {
                int qtdDias = -1;
                //int qtdDiasFim = -0;
                int qtdPagamentos = 0;
                string nfEmpresa = "";
                string idPagamentos = "";

                var client = controller().GetForceClient();
                DateTime? dateTime = new DateTime(1900, 01, 01, 0, 0, 0);
                var pagamentsCancelados = innovaDbContext.GetDevDocsEQ.Where(c => c.DtDev >= System.DateTime.Today.AddDays(qtdDias)).ToList();
                //var pagamentsCancelados = innovaDbContext.GetDevDocsEQ.Where(c => c.DtDev >= System.DateTime.Today.AddDays(qtdDias) && c.DtDev <= System.DateTime.Today.AddDays(qtdDiasFim)).ToList();
                
                if(pagamentsCancelados.Count < 1){
                    return;
                }

                List<SfPagamento> pagamentosCancelados = new List<SfPagamento>();

                foreach (var item in pagamentsCancelados)
                {
                    if(qtdPagamentos < 1)
                    {
                        nfEmpresa = "'" + item.NF + item.BASE + "'";
                    }
                    else
                    {
                        nfEmpresa = nfEmpresa + ", '" + item.NF + item.BASE + "'";
                    }
                    
                    qtdPagamentos++;

                    if(qtdPagamentos == 200){
                        var pagamentos = client.Query<SfPagamento>("SELECT Id FROM Pagamento__c WHERE NF_Empresa__c in (" + nfEmpresa + ")").Result;
                        pagamentosCancelados.AddRange(pagamentos);
                        nfEmpresa = "";
                        qtdPagamentos = 0;
                    }
                }
                qtdPagamentos = 0;

                if(nfEmpresa.Any()){
                    var pagamentos = client.Query<SfPagamento>("SELECT Id FROM Pagamento__c WHERE NF_Empresa__c in (" + nfEmpresa + ")").Result;
                    pagamentosCancelados.AddRange(pagamentos);
                    nfEmpresa = "";
                    qtdPagamentos = 0;
                }


                foreach (var pagamento in pagamentosCancelados)
                {
                    try
                    {
                        if(qtdPagamentos < 1)
                        {
                            idPagamentos = pagamento.Id;
                        }
                        else
                        {
                            idPagamentos = idPagamentos + "," + pagamento.Id;
                        }
                        
                        qtdPagamentos++;

                        if(qtdPagamentos == 200){
                            var resultPagamento = controller().DeletartDados(idPagamentos);
                            qtdPagamentos = 0;
                            idPagamentos = "";
                        }
                    }
                    catch(Exception ex)
                    {
                        string servico = "deletarPagamento / "+ pagamento.NF + " / " + pagamento.Empresa;
                        SfCustomIntegracao integracao = new SfCustomIntegracao();
                        integracao.Metodo = servico;
                        integracao.IdExterno = servico;
                        integracao.Mensagem = ex.Message;
                        integracao.Attributes = atributoIntegracao;
                        integracoes.Add(integracao);
                        continue;
                    }
                }
                if(idPagamentos != ""){
                    var resultPagamentos = controller().DeletartDados(idPagamentos);
                }
            }
            catch(Exception ex)
            {
                string servico = "deletarPagamentos";
                SfCustomIntegracao integracao = new SfCustomIntegracao();
                integracao.Metodo = servico;
                integracao.IdExterno = servico;
                integracao.Mensagem = ex.Message;
                integracao.Attributes = atributoIntegracao;
                integracoes.Add(integracao);
            }

            if(integracoes.Any()){
                var integracaoJSON = JsonConvert.SerializeObject(integracoes, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultadoIntegracao = controller().UpsertDados(integracaoJSON, "Integracao__c", "Id_Externo__c", "deletarPagamentos");
            }
        }

        [AutomaticRetry(Attempts = 0)]
        [HttpGet("deletePagamentosCanceladosMensal")]
        public void DeletarPagamentosCanceladosMensal()
        {
            List<SfPagamento> pagamentosSF = new List<SfPagamento>();
            List<SfCustomIntegracao> integracoes = new List<SfCustomIntegracao>();
            
            SObjectAttributes atributoIntegracao = new SObjectAttributes();
            atributoIntegracao.Type = "Integracao__c";

            try
            {
                int qtdDias = -90;
                int qtdPagamentos = 0;
                string nfEmpresa = "";
                string idPagamentos = "";

                var client = controller().GetForceClient();
                DateTime? dateTime = new DateTime(1900, 01, 01, 0, 0, 0);
                var pagamentsCancelados = innovaDbContext.GetDevDocsEQ.Where(c => c.DtDev >= System.DateTime.Today.AddDays(qtdDias)).ToList();
                
                if(pagamentsCancelados.Count < 1){
                    return;
                }

                List<SfPagamento> pagamentosCancelados = new List<SfPagamento>();

                foreach (var item in pagamentsCancelados)
                {
                    if(qtdPagamentos < 1)
                    {
                        nfEmpresa = "'" + item.NF + item.BASE + "'";
                    }
                    else
                    {
                        nfEmpresa = nfEmpresa + ", '" + item.NF + item.BASE + "'";
                    }
                    
                    qtdPagamentos++;

                    if(qtdPagamentos == 200){
                        var pagamentos = client.Query<SfPagamento>("SELECT Id FROM Pagamento__c WHERE NF_Empresa__c in (" + nfEmpresa + ")").Result;
                        pagamentosCancelados.AddRange(pagamentos);
                        nfEmpresa = "";
                        qtdPagamentos = 0;
                    }
                }
                qtdPagamentos = 0;

                if(nfEmpresa.Any()){
                    var pagamentos = client.Query<SfPagamento>("SELECT Id FROM Pagamento__c WHERE NF_Empresa__c in (" + nfEmpresa + ")").Result;
                    pagamentosCancelados.AddRange(pagamentos);
                    nfEmpresa = "";
                    qtdPagamentos = 0;
                }


                foreach (var pagamento in pagamentosCancelados)
                {
                    try
                    {
                        if(qtdPagamentos < 1)
                        {
                            idPagamentos = pagamento.Id;
                        }
                        else
                        {
                            idPagamentos = idPagamentos + "," + pagamento.Id;
                        }
                        
                        qtdPagamentos++;

                        if(qtdPagamentos == 200){
                            var resultPagamento = controller().DeletartDados(idPagamentos);
                            qtdPagamentos = 0;
                            idPagamentos = "";
                        }
                    }
                    catch(Exception ex)
                    {
                        string servico = "deletarPagamento / "+ pagamento.NF + " / " + pagamento.Empresa;
                        SfCustomIntegracao integracao = new SfCustomIntegracao();
                        integracao.Metodo = servico;
                        integracao.IdExterno = servico;
                        integracao.Mensagem = ex.Message;
                        integracao.Attributes = atributoIntegracao;
                        integracoes.Add(integracao);
                        continue;
                    }
                }
                if(idPagamentos != ""){
                    var resultPagamentos = controller().DeletartDados(idPagamentos);
                }
            }
            catch(Exception ex)
            {
                string servico = "deletarPagamentos";
                SfCustomIntegracao integracao = new SfCustomIntegracao();
                integracao.Metodo = servico;
                integracao.IdExterno = servico;
                integracao.Mensagem = ex.Message;
                integracao.Attributes = atributoIntegracao;
                integracoes.Add(integracao);
            }

            if(integracoes.Any()){
                var integracaoJSON = JsonConvert.SerializeObject(integracoes, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultadoIntegracao = controller().UpsertDados(integracaoJSON, "Integracao__c", "Id_Externo__c", "deletarPagamentos");
            }
        }

        [AutomaticRetry(Attempts = 0)]
        [HttpGet("upsertNF")]
        public void SincronizarNFs()
        {
            List<String> idsExternos = new List<String>();
            List<SfCustomItemNF> itensNotasSF = new List<SfCustomItemNF>();
            List<SfCustomNotaFiscal> notasSF = new List<SfCustomNotaFiscal>();
            List<SfCustomIntegracao> integracoes = new List<SfCustomIntegracao>();
            List<SfCustomOpportunity> oportunidades = new List<SfCustomOpportunity>();
            List<SfCustomOpportunityLineItem>  produtosOpp = new List<SfCustomOpportunityLineItem>();

            SObjectAttributes attributoNF = new SObjectAttributes();
            attributoNF.Type = "Nota_Fiscal__c";
            
            SObjectAttributes attributoItemNF = new SObjectAttributes();
            attributoItemNF.Type = "Item_da_Nota_Fiscal__c";

            try
            {
                int qtdNFs = 0;
                int qtdOportunidades = 0;
                string idsOportunidades = "";

                var client = controller().GetForceClient();
                string idsProdOportunidades = "";
                List<SfCustomUser> usuarios = controller().GetVendedores(client);
                
                int qtdMin = -2;
                var notasFiscais = innovaDbContext.Margem_NOVO.Where(Margem_NOVO => !String.IsNullOrEmpty(Margem_NOVO.LinhaItemSf) && Margem_NOVO.DataNF >= System.DateTime.Now.AddHours(qtdMin)).ToList();

                //int qtdDias = -30;
                //var notasFiscais = innovaDbContext.Margem_NOVO.Where(Margem_NOVO => !String.IsNullOrEmpty(Margem_NOVO.LinhaItemSf) && Margem_NOVO.DataNF >= System.DateTime.Today.AddDays(qtdDias)).ToList();


                foreach (var nf in notasFiscais)
                {
                    var id = nf.LinhaItemSf;

                    if(qtdOportunidades < 1)
                    {
                        idsProdOportunidades = "'" + id + "'";
                    }
                    else
                    {
                        idsProdOportunidades = idsProdOportunidades + ", '" + id + "'";
                    }
                    
                    qtdOportunidades++;

                    if(qtdOportunidades == 200){
                        var produtosOppLimit = client.Query<SfCustomOpportunityLineItem>(SfCustomOpportunityLineItem.BuildSelect() + " WHERE Id in ("+ idsProdOportunidades +")").Result;
                        produtosOpp.AddRange(produtosOppLimit);
                        idsProdOportunidades = "";
                        qtdOportunidades = 0;
                    }
                }
                qtdOportunidades = 0;

                if(idsProdOportunidades.Any()){
                    var produtosOppLimitAll = client.Query<SfCustomOpportunityLineItem>(SfCustomOpportunityLineItem.BuildSelect() + " WHERE Id in ("+ idsProdOportunidades +")").Result;
                    produtosOpp.AddRange(produtosOppLimitAll);
                }

                foreach (var prodOpp in produtosOpp)
                {
                    if(qtdOportunidades < 1)
                    {
                        idsOportunidades = "'" + prodOpp.OpportunityId + "'";
                    }
                    else
                    {
                        idsOportunidades = idsOportunidades + ", '" + prodOpp.OpportunityId + "'";
                    }
                    
                    qtdOportunidades++;

                    if(qtdOportunidades == 200){
                        var oppLimit = client.Query<SfCustomOpportunity>(SfCustomOpportunity.BuildSelect()  + $@" WHERE Id in (" + idsOportunidades + ")").Result;
                        oportunidades.AddRange(oppLimit);
                        idsOportunidades = "";
                        qtdOportunidades = 0;
                    }
                }
                qtdOportunidades = 0;

                if(idsOportunidades.Any()){
                    var oppLimitAll = client.Query<SfCustomOpportunity>(SfCustomOpportunity.BuildSelect()  + $@" WHERE Id in (" + idsOportunidades + ")").Result;
                    oportunidades.AddRange(oppLimitAll);
                }

                List<string> oppAdicionadas = new List<string>(); 

                foreach (var dadoSAP in notasFiscais)
                {
                    SfCustomOpportunityLineItem produto = produtosOpp.FirstOrDefault(p => p.Id == dadoSAP.LinhaItemSf);
                    
                    if(produto == null) { continue; }

                    var numNota = dadoSAP.NF;
                    var idPedidoSF = dadoSAP.CodigoNuvemSaas;
                    
                    try{
                        int idPedidoInt = idPedidoSF != null ? Int32.Parse(idPedidoSF) : 0;
                        Pedido pedido = innovaDbContext.Pedido.FirstOrDefault(c => c.IdPedido == idPedidoInt);

                        SfCustomOpportunity oportunidade = oportunidades.FirstOrDefault(o => o.Id == produto.OpportunityId);

                        var usuario = usuarios.FirstOrDefault(u => u.CodigoVendedorInt == dadoSAP.Vendedor_Cod || u.CodigoVendedorMadInt == dadoSAP.Vendedor_Cod);
                        SfCustomNotaFiscal notaFiscal = new SfCustomNotaFiscal();

                        var numeroNF = dadoSAP.NF.ToString();

                        notaFiscal.Conta__c = oportunidade.AccountId;
                        notaFiscal.Data_emissao_NF__c = dadoSAP.Data;
                        notaFiscal.Name = numeroNF;
                        notaFiscal.Oportunidade__c = oportunidade.Id;
                        var idExternoNF = numeroNF + oportunidade.Id;
                        notaFiscal.Id_Externo__c = idExternoNF;
                        notaFiscal.Attributes = attributoNF;
                        if(!oppAdicionadas.Contains(idExternoNF)){
                            oppAdicionadas.Add(idExternoNF);
                            
                            notasSF.Add(notaFiscal);

                            qtdNFs = qtdNFs + 1;

                            if(qtdNFs == 200){
                                var NFJSON = JsonConvert.SerializeObject(notasSF, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                                var resultNF = controller().UpsertDados(NFJSON, "Nota_Fiscal__c", "Id_Externo__c", "upsertNF");
                                qtdNFs = 0;
                                notasSF = new List<SfCustomNotaFiscal>();
                            }
                        }
                    }
                    catch(Exception ex)
                    {
                        string servico = "upsertNF / "+ produto.OpportunityId;
                        if(!idsExternos.Contains(servico))
                        {
                            var message = ex.InnerException != null ? ex.InnerException.Message : null;
                            string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                            SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                            integracoes.Add(integracao);
                            idsExternos.Add(servico);
                        }
                        continue;
                    }
                }

                var NFsJSON = JsonConvert.SerializeObject(notasSF, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultNFs = controller().UpsertDados(NFsJSON, "Nota_Fiscal__c", "Id_Externo__c", "upsertNF");

                qtdNFs = 0;
                
                foreach (var dadoProduto in notasFiscais)
                {
                    SfCustomOpportunityLineItem item = produtosOpp.FirstOrDefault(p => p.Id == dadoProduto.LinhaItemSf);
                    
                    if(item == null) { continue; }
                    try
                    {
                        var numeroNF = dadoProduto.NF.ToString();
                        var idExternoNF = numeroNF + item.OpportunityId;

                        SfCustomNotaFiscal nf = new SfCustomNotaFiscal();
                        nf.Id_Externo__c = idExternoNF;

                        SfCustomItemNF itemNF = new SfCustomItemNF();

                        var idExterno = dadoProduto.NF + item.Id;
                        itemNF.NotaFiscal = nf;
                        itemNF.Id_Externo__c = idExterno;
                        itemNF.Produto__c = item.Product2Id;
                        itemNF.Attributes = attributoItemNF;
                        itemNF.Imposto__c = dadoProduto.Imposto;
                        itemNF.Devolucao__c = dadoProduto.Devolucao;
                        itemNF.Quantidade__c = dadoProduto.Quantidade;
                        itemNF.Contribuicao__c = dadoProduto.Contribuicao;
                        itemNF.Valor_Liquido__c = dadoProduto.ValorLiquido;
                        itemNF.Custo_Mercadoria_Vendida__c = dadoProduto.CMV;
                        itemNF.Receita_Liquida__c = dadoProduto.ReceitaLiquida;
                        itemNF.Valor_unitario__c = dadoProduto.ValorLiquido / dadoProduto.Quantidade;

                        itensNotasSF.Add(itemNF);

                        qtdNFs = qtdNFs + 1;

                        if(qtdNFs == 200){
                            var ItemNFJSON = JsonConvert.SerializeObject(itensNotasSF, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                            var resultNF = controller().UpsertDados(ItemNFJSON, "Item_da_Nota_Fiscal__c", "Id_Externo__c", "upsertItemNF");
                            qtdNFs = 0;
                            itensNotasSF = new List<SfCustomItemNF>();
                        }
                    }
                    catch(Exception ex)
                    {
                        string servico = "upsertItemNF / "+ dadoProduto.LinhaItemSf;
                        if(!idsExternos.Contains(servico))
                        {
                            var message = ex.InnerException != null ? ex.InnerException.Message : null;
                            string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                            SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                            integracoes.Add(integracao);
                            idsExternos.Add(servico);
                        }
                        continue;
                    }
                }
                
                var ItensNFJSON = JsonConvert.SerializeObject(itensNotasSF, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultadoContatos = controller().UpsertDados(ItensNFJSON, "Item_da_Nota_Fiscal__c", "Id_Externo__c", "upsertItemNF");
            }
            catch(Exception ex)
            {
                string servico = "upsertNF";
                if(!idsExternos.Contains(servico))
                {
                    var message = ex.InnerException != null ? ex.InnerException.Message : null;
                string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                    SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                    integracoes.Add(integracao);
                    idsExternos.Add(servico);
                }
            }

            if(integracoes.Any()){
                var integracaoJSON = JsonConvert.SerializeObject(integracoes, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultadoIntegracao = controller().UpsertDados(integracaoJSON, "Integracao__c", "Id_Externo__c", "upsertItemNF");
            }
        }

        [AutomaticRetry(Attempts = 0)]
        [HttpGet("updateOportunidades")]
        public void SincronizarOportunidades()
        {
            List<String> idsExternos = new List<String>();
            List<SfCustomIntegracao> integracoes = new List<SfCustomIntegracao>();
            List<SfCustomOpportunity> oportunidadesUpdate = new List<SfCustomOpportunity>();
            List<SfCustomOpportunityLineItem>  produtosOpp = new List<SfCustomOpportunityLineItem>();
            List<SfCustomOpportunityLineItem> itensOpportunidade = new List<SfCustomOpportunityLineItem>();
            
            SObjectAttributes attributoOpp = new SObjectAttributes();
            attributoOpp.Type = "Opportunity";
            
            SObjectAttributes attributoItemOpp = new SObjectAttributes();
            attributoItemOpp.Type = "OpportunityLineItem";

            try
            {
                int qtdOpps = 0;
                int qtdOportunidades = 0;

                var client = controller().GetForceClient();
                string idsProdOportunidades = "";
                List<SfCustomUser> usuarios = client.Query<SfCustomUser>("SELECT Id, Codigo_do_Vendedor__c, Codigo_Vendedor_Area_Medica__c, Codigo_SAP__c FROM User WHERE IsActive = True AND (Codigo_do_Vendedor__c != null OR Codigo_Vendedor_Area_Medica__c != null)").Result;
                
                int qtdMin = -2;
                var notasFiscais = innovaDbContext.Margem_NOVO.Where(Margem_NOVO => !String.IsNullOrEmpty(Margem_NOVO.LinhaItemSf) && Margem_NOVO.DataNF >= System.DateTime.Now.AddHours(qtdMin)).ToList();

                //int qtdDias = -30;
                //var notasFiscais = innovaDbContext.Margem_NOVO.Where(Margem_NOVO => !String.IsNullOrEmpty(Margem_NOVO.LinhaItemSf) && Margem_NOVO.DataNF >= System.DateTime.Now.AddDays(qtdDias)).ToList();

                foreach (var nf in notasFiscais)
                {
                    var id = nf.LinhaItemSf;

                    if(qtdOportunidades < 1)
                    {
                        idsProdOportunidades = "'" + id + "'";
                    }
                    else
                    {
                        idsProdOportunidades = idsProdOportunidades + ", '" + id + "'";
                    }
                    
                    qtdOportunidades++;

                    if(qtdOportunidades == 200){
                        var produtosOppLimit = client.Query<SfCustomOpportunityLineItem>(SfCustomOpportunityLineItem.BuildSelect() + " WHERE Id in ("+ idsProdOportunidades +")").Result;
                        produtosOpp.AddRange(produtosOppLimit);
                        idsProdOportunidades = "";
                        qtdOportunidades = 0;
                    }
                }
                qtdOportunidades = 0;

                if(idsProdOportunidades.Any()){
                    var produtosOppLimitAll = client.Query<SfCustomOpportunityLineItem>(SfCustomOpportunityLineItem.BuildSelect() + " WHERE Id in ("+ idsProdOportunidades +")").Result;
                    produtosOpp.AddRange(produtosOppLimitAll);
                }

                List<string> oppAdicionadas = new List<string>(); 

                foreach (var dadoSAP in notasFiscais)
                {
                    SfCustomOpportunityLineItem produto = produtosOpp.FirstOrDefault(p => p.Id == dadoSAP.LinhaItemSf);
                    
                    if(produto == null) { continue; }

                    var numNota = dadoSAP.NF;
                    var idPedidoSF = dadoSAP.CodigoNuvemSaas;
                    
                    try
                    {
                        if(!oppAdicionadas.Contains(produto.OpportunityId))
                        {
                            int idPedidoInt = idPedidoSF != null ? Int32.Parse(idPedidoSF) : 0;
                            Pedido pedido = innovaDbContext.Pedido.FirstOrDefault(c => c.IdPedido == idPedidoInt);

                            var usuario = usuarios.FirstOrDefault(u => u.CodigoVendedorInt == dadoSAP.Vendedor_Cod || u.CodigoVendedorMadInt == dadoSAP.Vendedor_Cod);
                            
                            SfCustomOpportunity oportunidade = new SfCustomOpportunity();

                            string codigoSAP = pedido.IdPedidoSap;
                            string respostaStatus = pedido.Status;

                            if(respostaStatus != null && respostaStatus.StartsWith(">>")){
                                string[] listaRetorno = respostaStatus.Split(" ");
                                int tamanhoLista = listaRetorno.Count() -1;
                                codigoSAP = listaRetorno[tamanhoLista];
                            }
                            oportunidade.bypass__c = true;
                            oportunidade.Sincronizado = true;
                            oportunidade.StatusSAP = pedido.Status;
                            oportunidade.Codigo_SAP__c = codigoSAP;
                            oportunidade.Attributes = attributoOpp;
                            oportunidade.Id = produto.OpportunityId;
                            oportunidade.nNF = dadoSAP.NF.ToString();
                            oportunidade.DataEmissaoNF = dadoSAP.Data;
                            oportunidade.ChaveNF = dadoSAP.ChaveAcesso;
                            if (usuario != null) { oportunidade.OwnerId = usuario.Id; }
                        
                            oppAdicionadas.Add(produto.OpportunityId);
                            
                            oportunidadesUpdate.Add(oportunidade);

                            qtdOpps = qtdOpps + 1;

                            if(qtdOpps == 5){
                                var OppJSON = JsonConvert.SerializeObject(oportunidadesUpdate, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                                var resultOpp = controller().UpsertDados(OppJSON, "Opportunity", "Id", "updateOportunidade");
                                qtdOpps = 0;
                                oportunidadesUpdate = new List<SfCustomOpportunity>();
                            }
                        }
                    }
                    catch(Exception ex)
                    {
                        string servico = "updateOportunidade / "+ produto.OpportunityId;
                        if(!idsExternos.Contains(servico))
                        {
                            var message = ex.InnerException != null ? ex.InnerException.Message : null;
                            string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                            SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                            integracoes.Add(integracao);
                            idsExternos.Add(servico);
                        }
                        continue;
                    }
                }

                var OppsJSON = JsonConvert.SerializeObject(oportunidadesUpdate, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultNFs = controller().UpsertDados(OppsJSON, "Opportunity", "Id", "updateOportunidade");

                qtdOpps = 0;
                
                foreach (var dadoProduto in notasFiscais)
                {
                    try
                    {
                        SfCustomOpportunityLineItem produto = produtosOpp.FirstOrDefault(p => p.Id == dadoProduto.LinhaItemSf);
                    
                        if(produto == null) { continue; }

                        SfCustomOpportunityLineItem produtoNew = new SfCustomOpportunityLineItem();

                         
                        produtoNew.Attributes = attributoItemOpp;
                        produtoNew.Imposto = dadoProduto.Imposto;
                        produtoNew.Devolucao = dadoProduto.Devolucao;
                        produtoNew.Contribuicao = dadoProduto.Contribuicao;
                        produtoNew.CustoMercadoriaVendida = dadoProduto.CMV;
                        produtoNew.ReceitaLiquida = dadoProduto.ReceitaLiquida;
                        produtoNew.QuantidadeFaturada = dadoProduto.Quantidade;
                        produtoNew.Quantity = Convert.ToDouble(dadoProduto.Quantidade);
                        produtoNew.Id = produto.Id;
                        if (dadoProduto.ValorLiquido != 0){ produtoNew.UnitPrice = dadoProduto.ValorLiquido / dadoProduto.Quantidade; }

                        itensOpportunidade.Add(produtoNew);

                        qtdOpps = qtdOpps + 1;

                        if(qtdOpps == 1){
                            var ItemOppJSON = JsonConvert.SerializeObject(itensOpportunidade, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                            var resultNF = controller().UpsertDados(ItemOppJSON, "OpportunityLineItem", "Id", "updateItemOportunidade");
                            qtdOpps = 0;
                            itensOpportunidade = new List<SfCustomOpportunityLineItem>();
                        }
                    }
                    catch(Exception ex)
                    {
                        string servico = "updateItemOportunidade / "+ dadoProduto.LinhaItemSf;
                        if(!idsExternos.Contains(servico))
                        {
                            var message = ex.InnerException != null ? ex.InnerException.Message : null;
                            string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                            SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                            integracoes.Add(integracao);
                            idsExternos.Add(servico);
                        }
                        continue;
                    }
                }
                
                var ItensOppJSON = JsonConvert.SerializeObject(itensOpportunidade, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultadoContatos = controller().UpsertDados(ItensOppJSON, "OpportunityLineItem", "Id", "updateItemOportunidade");
            }
            catch(Exception ex)
            {
                string servico = "updateOportunidade";
                if(!idsExternos.Contains(servico))
                {
                    var message = ex.InnerException != null ? ex.InnerException.Message : null;
                    string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                    SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                    integracoes.Add(integracao);
                    idsExternos.Add(servico);
                }
            }

            if(integracoes.Any()){
                var integracaoJSON = JsonConvert.SerializeObject(integracoes, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultadoIntegracao = controller().UpsertDados(integracaoJSON, "Integracao__c", "Id_Externo__c", "upsertItemNF");
            }
        }

        [AutomaticRetry(Attempts = 0)]
        [HttpGet("updateStatusOportunidade")]
        public void SincronizarStatusOportunidade()
        {
            List<String> idsExternos = new List<String>();
            List<SfCustomIntegracao> integracoes = new List<SfCustomIntegracao>();
            List<SfCustomOpportunity> oportunidadesUpdate = new List<SfCustomOpportunity>();
            List<SfCustomOpportunityLineItem>  produtosOpp = new List<SfCustomOpportunityLineItem>();
            List<SfCustomOpportunityLineItem> itensOpportunidade = new List<SfCustomOpportunityLineItem>();
            
            SObjectAttributes attributoOpp = new SObjectAttributes();
            attributoOpp.Type = "Opportunity";

            try
            {
                int qtdOpps = 0;

                var client = controller().GetForceClient();
                List<SfCustomUser> usuarios = client.Query<SfCustomUser>("SELECT Id, Codigo_do_Vendedor__c, Codigo_Vendedor_Area_Medica__c, Codigo_SAP__c FROM User WHERE IsActive = True AND (Codigo_do_Vendedor__c != null OR Codigo_Vendedor_Area_Medica__c != null)").Result;
                
                int qtdMin = -15;
                var pedidos = innovaDbContext.Pedido.Where(Pedido => Pedido.DataStatus >= System.DateTime.Now.AddMinutes(qtdMin)).ToList();
                //var pedidos = innovaDbContext.Pedido.Where(Pedido => Pedido.Id_Salesforce != null && Pedido.Id_Salesforce != "").ToList();

                List<String> oppAdicionadas = new List<String>();

                foreach (var pedido in pedidos)
                {
                    try
                    {
                        String statusIntegracao = "Pendente";
                        String idPedidoSap = pedido.IdPedidoSap;

                        Boolean somenteNumero = idPedidoSap.All(char.IsDigit);

                        if(somenteNumero){
                            statusIntegracao = "Importado";
                        } else if(idPedidoSap == "E"){
                            statusIntegracao = "Erro";
                        }

                        SfCustomOpportunity oportunidade = new SfCustomOpportunity();

                        string codigoSAP = pedido.IdPedidoSap;
                        string respostaStatus = pedido.Status;

                        oportunidade.bypass__c = true;
                        oportunidade.Sincronizado = true;
                        oportunidade.StatusSAP = pedido.Status;
                        oportunidade.Codigo_SAP__c = codigoSAP;
                        oportunidade.Attributes = attributoOpp;
                        oportunidade.Id = pedido.Id_Salesforce;
                        oportunidade.Status_Integracao__c = statusIntegracao;

                        if(!oppAdicionadas.Contains(pedido.Id_Salesforce)){
                            oportunidadesUpdate.Add(oportunidade); 
                        }
                        
                        oppAdicionadas.Add(pedido.Id_Salesforce);

                        qtdOpps = qtdOpps + 1;

                        if(qtdOpps == 5){
                            var OppJSON = JsonConvert.SerializeObject(oportunidadesUpdate, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                            var resultOpp = controller().UpsertDados(OppJSON, "Opportunity", "Id", "updateOportunidade");
                            qtdOpps = 0;
                            oportunidadesUpdate = new List<SfCustomOpportunity>();
                        }
                    }
                    catch(Exception ex)
                    {
                        string servico = "updateOportunidade / " + pedido.Id_Salesforce;
                        if(!idsExternos.Contains(servico))
                        {
                            var message = ex.InnerException != null ? ex.InnerException.Message : null;
                            string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                            SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                            integracoes.Add(integracao);
                            idsExternos.Add(servico);
                        }
                        continue;
                    }
                }

                var OppsJSON = JsonConvert.SerializeObject(oportunidadesUpdate, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultNFs = controller().UpsertDados(OppsJSON, "Opportunity", "Id", "updateOportunidade");
            }
            catch(Exception ex)
            {
                string servico = "updateOportunidade";
                if(!idsExternos.Contains(servico))
                {
                    var message = ex.InnerException != null ? ex.InnerException.Message : null;
                    string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                    SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                    integracoes.Add(integracao);
                    idsExternos.Add(servico);
                }
            }

            if(integracoes.Any()){
                var integracaoJSON = JsonConvert.SerializeObject(integracoes, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultadoIntegracao = controller().UpsertDados(integracaoJSON, "Integracao__c", "Id_Externo__c", "upsertItemNF");
            }
        }

        [AutomaticRetry(Attempts = 0)]
        [HttpGet("updateOportunidadesCartao")]
        public void SincronizarOportunidadesCartao()
        {
            var qtdOpps = 0;

            var client = controller().GetForceClient();
            List<string> idOpps = new List<string>();
            List<String> idsExternos = new List<String>();
            List<SfCustomIntegracao> integracoes = new List<SfCustomIntegracao>();
            List<SfCustomOpportunity> oportunidadesUpdate = new List<SfCustomOpportunity>();
            
            SObjectAttributes attributoOpp = new SObjectAttributes();
            attributoOpp.Type = "Opportunity";
            
            try
            {
                List<SfCustomOpportunity> oportunidades = client.Query<SfCustomOpportunity>(SfCustomOpportunity.BuildSelect() + " WHERE StageName = 'Finalizado' AND Condicao__c = 'Cartão' AND ((Parado_no_Cadastro__c = False AND Parado_no_Financeiro__c = False AND Parado_no_desconto__c = False AND Parado_no_farmaceutico__c = False  AND Concluiu_aprovacao__c = null) OR Concluiu_aprovacao__c = 'Sim') AND Endere_o_valido__c = true").Result;

                //List<SfCustomOpportunity> oportunidades = client.Query<SfCustomOpportunity>(SfCustomOpportunity.BuildSelect() + " WHERE Id = '006HY000004qE2GYAU'").Result;

                foreach (var oportunidadeSF in oportunidades)
                {
                    try
                    {
                        var pedido = innovaDbContext.PedidoMundiPagg.FirstOrDefault(p => oportunidadeSF.Id == p.IdSalesForce);
                        if(pedido == null){continue;}
                        var codigo = pedido.IdPedido.ToString().PadLeft(7, '0');

                        SfCustomOpportunity oportunidade = new SfCustomOpportunity();

                        oportunidade.bypass__c = true;
                        oportunidade.Id = oportunidadeSF.Id;
                        oportunidade.Attributes = attributoOpp;
                        oportunidade.Enviado_MundiPagg__c = true;
                        oportunidade.Link_de_Pagamento__c = pedido.LinkPagamento;
                        oportunidade.Codigo_de_Autorizacao__c = pedido.CodAutorizacao;
                        oportunidade.Status_MundiPagg__c = pedido.StatusMundiPagg == 3 ? "Recusado" : pedido.StatusMundiPagg == 2 ? "Pago" : pedido.StatusMundiPagg == 4 ? "Cancelado" : pedido.StatusMundiPagg == 5 ? "Pendente AntiFraude" : "Pendente";
                        
                        if(pedido.StatusMundiPagg == 4) 
                        {
                            oportunidade.StageName = "Cancelado";
                            oportunidade.Financeiro__c = "";
                            oportunidade.Dep_Desconto__c = "";
                            oportunidade.Dep_Estoque__c = "";
                        }
                        
                        if(pedido.StatusMundiPagg == 2) 
                        {
                            oportunidade.StageName = "Exportado";
                            oportunidade.Financeiro__c = "Liberado";
                            oportunidade.Dep_Desconto__c = "Liberado";
                            oportunidade.Dep_Estoque__c = "Liberado";
                            oportunidade.Status_Integracao__c = "Pendente";
                        }

                        oportunidadesUpdate.Add(oportunidade);
                        

                        qtdOpps = qtdOpps + 1;

                        if(qtdOpps == 10){
                            var OppJSON = JsonConvert.SerializeObject(oportunidadesUpdate, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                            var resultOpp = controller().UpsertDados(OppJSON, "Opportunity", "Id", "updateOportunidade");
                            qtdOpps = 0;
                            oportunidadesUpdate = new List<SfCustomOpportunity>();
                        }
                    }
                    catch(Exception ex)
                    {
                        string servico = "updateOportunidadeCartao // " + oportunidadeSF.Id;
                        if(!idsExternos.Contains(servico))
                        {
                            var message = ex.InnerException != null ? ex.InnerException.Message : null;
                            string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                            SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                            integracoes.Add(integracao);
                            idsExternos.Add(servico);
                        }
                    }
                }

                var OppsJSON = JsonConvert.SerializeObject(oportunidadesUpdate, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultNFs = controller().UpsertDados(OppsJSON, "Opportunity", "Id", "updateOportunidadeCartao");
            }
            catch(Exception ex)
            {
                string servico = "updateOportunidadeCartao";
                if(!idsExternos.Contains(servico))
                {
                    var message = ex.InnerException != null ? ex.InnerException.Message : null;
                    string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                    SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                    integracoes.Add(integracao);
                    idsExternos.Add(servico);
                }
            }

            if(integracoes.Any()){
                var integracaoJSON = JsonConvert.SerializeObject(integracoes, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultadoIntegracao = controller().UpsertDados(integracaoJSON, "Opportunity", "Id", "updateOportunidadeCartao");
            }
        }

        [AutomaticRetry(Attempts = 0)]
        [HttpGet("updateOportunidadesPIX")]
        public void SincronizarOportunidadesPIX()
        {
            var qtdOpps = 0;

            var client = controller().GetForceClient();

            List<string> idOpps = new List<string>();
            List<String> idsExternos = new List<String>();
            List<SfCustomIntegracao> integracoes = new List<SfCustomIntegracao>();
            List<SfCustomOpportunity> oportunidadesUpdate = new List<SfCustomOpportunity>();
            
            SObjectAttributes attributoOpp = new SObjectAttributes();
            attributoOpp.Type = "Opportunity";
            
            try
            {
                List<SfCustomOpportunity> oportunidades = client.Query<SfCustomOpportunity>(SfCustomOpportunity.BuildSelect() + " WHERE StageName = 'Finalizado' AND Condicao__c = 'PIX' AND ((Parado_no_Cadastro__c = False AND Parado_no_Financeiro__c = False AND Parado_no_desconto__c = False AND Parado_no_farmaceutico__c = False AND Concluiu_aprovacao__c = null) OR Concluiu_aprovacao__c = 'Sim') AND Endere_o_valido__c = true").Result;

                //List<SfCustomOpportunity> oportunidades = client.Query<SfCustomOpportunity>(SfCustomOpportunity.BuildSelect() + " WHERE Id = '006HY000004qE2GYAU'").Result;

                foreach (var oportunidadeSF in oportunidades)
                {
                    try
                    {
                        var pedido = innovaDbContext.PedidoBankPlus.FirstOrDefault(p => oportunidadeSF.Codigo == p.NumeroPedido);
                        if(pedido == null){continue;}
                        var codigo = pedido.NumeroPedido.ToString().PadLeft(7, '0');

                        SfCustomOpportunity oportunidade = new SfCustomOpportunity();

                        oportunidade.bypass__c = true;
                        oportunidade.Id = oportunidadeSF.Id;
                        oportunidade.Attributes = attributoOpp;
                        oportunidade.Enviado_MundiPagg__c = true;
                        //oportunidade.Link_de_Pagamento__c = pedido.LinkPagamento;
                        oportunidade.QR_Code_PIX__c = pedido.QrCodePix;
                        oportunidade.DataQRCode = pedido.DataEnvio;
                        oportunidade.Status_MundiPagg__c = pedido.IDStatusPagamento == 3 ? "ERRO" : pedido.IDStatusPagamento == 2 ? "Pago" : pedido.IDStatusPagamento == 4 ? "Cancelado" : "Processado";
                        
                        if(pedido.IDStatusPagamento == 4) 
                        {
                            oportunidade.StageName = "Cancelado";
                            oportunidade.Financeiro__c = "";
                            oportunidade.Dep_Desconto__c = "";
                            oportunidade.Dep_Estoque__c = "";
                        }
                        
                        if(pedido.IDStatusPagamento == 2) 
                        {
                            oportunidade.StageName = "Exportado";
                            oportunidade.Financeiro__c = "Liberado";
                            oportunidade.Dep_Desconto__c = "Liberado";
                            oportunidade.Dep_Estoque__c = "Liberado";
                            oportunidade.Status_Integracao__c = "Pendente";
                        }

                        oportunidadesUpdate.Add(oportunidade);
                        
                        qtdOpps = qtdOpps + 1;

                        if(qtdOpps == 10){
                            var OppJSON = JsonConvert.SerializeObject(oportunidadesUpdate, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                            var resultOpp = controller().UpsertDados(OppJSON, "Opportunity", "Id", "updateOportunidade");
                            qtdOpps = 0;
                            oportunidadesUpdate = new List<SfCustomOpportunity>();
                        }
                    }
                    catch(Exception ex)
                    {
                        string servico = "updateOportunidadePIX // " + oportunidadeSF.Id;
                        if(!idsExternos.Contains(servico))
                        {
                            var message = ex.InnerException != null ? ex.InnerException.Message : null;
                            string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                            SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                            integracoes.Add(integracao);
                            idsExternos.Add(servico);
                        }
                    }
                }

                var OppsJSON = JsonConvert.SerializeObject(oportunidadesUpdate, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultNFs = controller().UpsertDados(OppsJSON, "Opportunity", "Id", "updateOportunidadePIX");
            }
            catch(Exception ex)
            {
                string servico = "updateOportunidadePIX";
                if(!idsExternos.Contains(servico))
                {
                    var message = ex.InnerException != null ? ex.InnerException.Message : null;
                    string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                    SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                    integracoes.Add(integracao);
                    idsExternos.Add(servico);
                }
            }

            if(integracoes.Any()){
                var integracaoJSON = JsonConvert.SerializeObject(integracoes, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultadoIntegracao = controller().UpsertDados(integracaoJSON, "Opportunity", "Id", "updateOportunidadePIX");
            }
        }

        [AutomaticRetry(Attempts = 0)]
        [HttpGet("reenviarOportunidadesCartao")]
        public async Task RessincronizarOportunidadesCartao()
        {

            var client = controller().GetForceClient();
            List<string> idOpps = new List<string>();
            List<String> idsExternos = new List<String>();
            List<SfCustomIntegracao> integracoes = new List<SfCustomIntegracao>();
            
            SObjectAttributes attributoOpp = new SObjectAttributes();
            attributoOpp.Type = "Opportunity";
            
            try
            {
                DateTime agora = System.DateTime.Now.AddMinutes(181);
                
                String dataModificacao = agora.Year.ToString() + '-' + agora.Month.ToString().PadLeft(2, '0') + '-' + agora.Day.ToString().PadLeft(2, '0') + 'T' + agora.Hour.ToString().PadLeft(2, '0') + ':' + agora.Minute.ToString().PadLeft(2, '0') + ":00.000+0000" ;
                
                List<SfCustomOpportunity> oportunidades = client.Query<SfCustomOpportunity>(SfCustomOpportunity.BuildSelect() + " WHERE StageName = 'Finalizado' AND Condicao__c = 'Cartão' AND Link_de_Pagamento__c = null AND ((Parado_no_Cadastro__c = False AND Parado_no_Financeiro__c = False AND Parado_no_desconto__c = False AND Parado_no_farmaceutico__c = False AND Receituario_parado__c = False AND Concluiu_aprovacao__c = null) OR Concluiu_aprovacao__c = 'Sim') AND Endere_o_valido__c = true AND CardCode__c != null AND LastModifiedDate < " + dataModificacao).Result;
                
                foreach (var opp in oportunidades)
                {
                    try
                    {
                        PedidoMundiPagg pedido = innovaDbContext.PedidoMundiPagg.FirstOrDefault(p => p.IdSalesForce == opp.Id);
                        if(pedido != null){continue;}
                        SfEndereco endEntrega = client.QuerySingle<SfEndereco>("SELECT Tipo__c, CardCode__c, Codigo_Cidade__c, Contato__c, Rua__c, Complemento__c, Conta__c, Bairro__c, Numero__c, Estado__c, CEP__c, Cidade__c, CreatedById, Name, Id, CreatedDate FROM Endereco__c WHERE Tipo__c = 'Endereço de Entrega' AND Conta__c = '" + opp.AccountId + "' limit 1").Result;
                        List<SfCustomOpportunityLineItem> produtosOpp = client.Query<SfCustomOpportunityLineItem>(SfCustomOpportunityLineItem.BuildSelect() + " WHERE OpportunityId = '" + opp.Id + "'").Result;

                        PedidoMundiPagg pNew = new PedidoMundiPagg();

                        pNew.Empresa = produtosOpp.FirstOrDefault().Empresa != null ? produtosOpp.FirstOrDefault().Empresa : "SBOEquilibrium";
                        pNew.IdPedido = opp.Codigo;
                        pNew.Cancelado = 0;
                        pNew.StatusPedido = 1;
                        pNew.IdSalesForce = opp.Id;
                        pNew.ValorTotal = opp.Valor__c;
                        pNew.Email = opp.Email_do_Contato__c;
                        pNew.DataEnvio = System.DateTime.Now;
                        pNew.Codigo_Cliente = opp.ContaCardCode;
                        pNew.Parcelas = Convert.ToInt16(opp.Numero_de_Parcelas__c);
                        pNew.QuantidadeCartoes = Convert.ToInt16(opp.Quantidade_de_cartoes__c) == 0 ? 1 : Convert.ToInt16(opp.Quantidade_de_cartoes__c);
                        
                        var nabota = produtosOpp.FirstOrDefault(p => p.ProductCode == "1162957");
                        var desoxicolato = produtosOpp.FirstOrDefault(p => p.ProductCode == "0328910");
                        var outroProduto = produtosOpp.FirstOrDefault(p => p.ProductCode != "1162957");

                        bool possuiNabota = nabota != null;;
                        bool possuiDesoxicolato = desoxicolato != null;
                        bool possuiOutrosProdutos = outroProduto != null;

                        Frete frete = controller().calculaFrete(opp, possuiNabota, possuiOutrosProdutos, possuiDesoxicolato, endEntrega.Estado, endEntrega.CodigoCidade, "1");
                        
                        pNew.ValorFrete = frete.ValorFrete;

                        await innovaDbContext.AddAsync(pNew);

                        foreach (var prodOpp in produtosOpp)
                        {
                            try{
                                PedidoMundiPaggItens produtoNew = new PedidoMundiPaggItens();

                                int tipoUtilizacao = 1;

                                if (opp.Type == "Bonificação Saída") { tipoUtilizacao = 9;}
                                else if (opp.Type == "Doação"){tipoUtilizacao = 13;}
                                else if (opp.Type == "Produtos promocionais"){tipoUtilizacao = 42;}
                                else if (opp.Type == "Feiras, Cong e Eventos"){tipoUtilizacao = 59;}
                                else if (opp.Type == "Consumo Interno"){tipoUtilizacao = 65;}
                                else if (opp.Type == "Venda (Desc. Incond.)"){tipoUtilizacao = 68;}
                                
                                Decimal valorVenda = controller().getIpi(opp.ContaCardCode, prodOpp.ProductCode, tipoUtilizacao, prodOpp.UnitPrice);

                                produtoNew.IdPedido = opp.Codigo;
                                produtoNew.IdSalesForce = prodOpp.Id;
                                produtoNew.Preco = valorVenda > 0 ? valorVenda : prodOpp.UnitPrice;
                                produtoNew.Quantidade = prodOpp.Quantity;
                                produtoNew.ItemCodigo = prodOpp.CodigoProduto;
                                produtoNew.Empresa = prodOpp.Empresa != null ? prodOpp.Empresa : "SBOEquilibrium";
                                produtoNew.TotalTabela = prodOpp.ValorCombo;

                                await innovaDbContext.AddAsync(produtoNew);
                            }
                            catch (Exception ex)
                            {
                                string servico = "criarItemPedidoMundiPaggSAPReenviar // " + prodOpp.Id;
                                var message = ex.InnerException != null ? ex.InnerException.Message : null;
                                string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                                SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                                integracoes.Add(integracao);
                            }
                        }
                
                        await innovaDbContext.SaveChangesAsync();
                    }
                    catch(Exception ex)
                    {
                        string servico = "criarPedidoMundiPaggSAPReenviar // " + opp.Id;
                        if(!idsExternos.Contains(servico))
                        {
                            var message = ex.InnerException != null ? ex.InnerException.Message : null;
                            string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                            SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                            integracoes.Add(integracao);
                            idsExternos.Add(servico);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string servico = "criarPedidoMundiPaggSAPReenviar";
                var message = ex.InnerException != null ? ex.InnerException.Message : null;
                string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                integracoes.Add(integracao);
            }
            
            if(integracoes.Any()){
                var integracaoJSON = JsonConvert.SerializeObject(integracoes, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultadoIntegracao = controller().UpsertDados(integracaoJSON, "Integracao__c", "Id_Externo__c", "criarPedidoMundiPaggSAPReenviar");
            }
        }

        [AutomaticRetry(Attempts = 0)]
        [HttpGet("reenviarOportunidadesPIX")]
        public async Task RessincronizarOportunidadesPIX()
        {

            var client = controller().GetForceClient();
            List<string> idOpps = new List<string>();
            List<String> idsExternos = new List<String>();
            List<SfCustomIntegracao> integracoes = new List<SfCustomIntegracao>();
            
            SObjectAttributes attributoOpp = new SObjectAttributes();
            attributoOpp.Type = "Opportunity";
            
            try
            {
                DateTime agora = System.DateTime.Now.AddMinutes(181);
                
                String dataModificacao = agora.Year.ToString() + '-' + agora.Month.ToString().PadLeft(2, '0') + '-' + agora.Day.ToString().PadLeft(2, '0') + 'T' + agora.Hour.ToString().PadLeft(2, '0') + ':' + agora.Minute.ToString().PadLeft(2, '0') + ":00.000+0000" ;
                
                List<SfCustomOpportunity> oportunidades = client.Query<SfCustomOpportunity>(SfCustomOpportunity.BuildSelect() + " WHERE StageName = 'Finalizado' AND Condicao__c = 'PIX' AND QR_Code_PIX__c = null AND ((Parado_no_Cadastro__c = False AND Parado_no_Financeiro__c = False AND Parado_no_desconto__c = False AND Parado_no_farmaceutico__c = False AND Receituario_parado__c = False AND Concluiu_aprovacao__c = null) OR Concluiu_aprovacao__c = 'Sim') AND Endere_o_valido__c = true AND CardCode__c != null AND LastModifiedDate < " + dataModificacao).Result;
                
                foreach (var opp in oportunidades)
                {
                    PedidoBankPlus pedido = innovaDbContext.PedidoBankPlus.FirstOrDefault(c => c.NumeroPedido == opp.Codigo);
                    if(pedido != null){continue;}
                    SfEndereco endEntrega = await client.QuerySingle<SfEndereco>(SfEndereco.BuildSelect() + " WHERE Tipo__c = 'Endereço de Entrega' AND Conta__c = '" + opp.AccountId + "' limit 1", false);
                    List<SfCustomOpportunityLineItem> produtosOpp = await client.Query<SfCustomOpportunityLineItem>(SfCustomOpportunityLineItem.BuildSelect() + " WHERE OpportunityId = '" + opp.Id + "'");
                    
                    try{
                        PedidoBankPlus pNew = new PedidoBankPlus();

                        pNew.DBEmpresa = produtosOpp.FirstOrDefault().Empresa != null ? produtosOpp.FirstOrDefault().Empresa : "SBOEquilibrium";
                        pNew.NumeroPedido = opp.Codigo;
                        pNew.IDStatus = 1;
                        pNew.CodigoCliente = opp.ContaCardCode;
                        pNew.Email = opp.Email_do_Contato__c;
                        pNew.CodigoTitular = Int32.Parse(opp.CodigoEmitente);
                        pNew.CodigoVendedor = Int32.Parse(opp.CodVendedor.Split(".")[0]);
                        pNew.DataEnvio = System.DateTime.Now;
                        pNew.DataPedido = System.DateTime.Now;
                        pNew.Observacao = opp.ObservacesDoPedido;
                        pNew.Descricao = opp.Description;
                        pNew.PercentualDesconto = 0;

                        Decimal valorTotal = 0;

                        foreach (var prodOpp in produtosOpp)
                        {
                            PedidoItemBankPlus produtoNew = new PedidoItemBankPlus();

                            int tipoUtilizacao = 1;

                            if (opp.Type == "Bonificação Saída") { tipoUtilizacao = 9;}
                            else if (opp.Type == "Doação"){tipoUtilizacao = 13;}
                            else if (opp.Type == "Produtos promocionais"){tipoUtilizacao = 42;}
                            else if (opp.Type == "Feiras, Cong e Eventos"){tipoUtilizacao = 59;}
                            else if (opp.Type == "Consumo Interno"){tipoUtilizacao = 65;}
                            else if (opp.Type == "Venda (Desc. Incond.)"){tipoUtilizacao = 68;}
                            
                            decimal valorVenda = controller().getIpi(opp.ContaCardCode, prodOpp.ProductCode, tipoUtilizacao, prodOpp.UnitPrice);
                            valorVenda = valorVenda * Convert.ToInt32(prodOpp.Quantity);

                            valorTotal += valorVenda;
                        }
                        
                        var nabota = produtosOpp.FirstOrDefault(p => p.ProductCode == "1162957");
                        var desoxicolato = produtosOpp.FirstOrDefault(p => p.ProductCode == "0328910");
                        var outroProduto = produtosOpp.FirstOrDefault(p => p.ProductCode != "1162957");

                        bool possuiNabota = nabota != null;;
                        bool possuiDesoxicolato = desoxicolato != null;
                        bool possuiOutrosProdutos = outroProduto != null;

                        Frete frete = controller().calculaFrete(opp, possuiNabota, possuiOutrosProdutos, possuiDesoxicolato, endEntrega.Estado, endEntrega.CodigoCidade, "1");

                        pNew.ValorTotalFrete = frete.ValorFrete;
                        pNew.ValorTotal = opp.Type == "Venda (Desc. Incond.)" ? opp.Valor__c : valorTotal + frete.ValorFrete;
                        
                        await innovaDbContext.AddAsync(pNew);

                        foreach (var prodOpp in produtosOpp)
                        {
                            try{
                                PedidoItemBankPlus produtoNew = new PedidoItemBankPlus();

                                int tipoUtilizacao = 1;

                                if (opp.Type == "Bonificação Saída") { tipoUtilizacao = 9;}
                                else if (opp.Type == "Doação"){tipoUtilizacao = 13;}
                                else if (opp.Type == "Produtos promocionais"){tipoUtilizacao = 42;}
                                else if (opp.Type == "Feiras, Cong e Eventos"){tipoUtilizacao = 59;}
                                else if (opp.Type == "Consumo Interno"){tipoUtilizacao = 65;}
                                else if (opp.Type == "Venda (Desc. Incond.)"){tipoUtilizacao = 68;}
                                
                                Decimal valorVenda = controller().getIpi(opp.ContaCardCode, prodOpp.ProductCode, tipoUtilizacao, prodOpp.UnitPrice);

                                produtoNew.IdSalesForce = prodOpp.Id;
                                produtoNew.NumeroPedido = opp.Codigo;
                                produtoNew.CodigoItem = prodOpp.CodigoProduto;
                                produtoNew.PrecoUnitario = valorVenda > 0 ? valorVenda : prodOpp.UnitPrice;
                                produtoNew.Quantidade = prodOpp.Quantity;
                                produtoNew.DBEmpresa = prodOpp.Empresa != null ? prodOpp.Empresa : "SBOEquilibrium";
                                produtoNew.PercentualDesconto = 0;
                                produtoNew.CodigoUtilizacao = tipoUtilizacao;
                                produtoNew.TotalTabela = prodOpp.ValorCombo;

                                await innovaDbContext.AddAsync(produtoNew);
                            }
                            catch (Exception ex)
                            {
                                string servico = "criarItemPedidoPIX // " + prodOpp.Id;
                                var message = ex.InnerException != null ? ex.InnerException.Message : null;
                                string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                                SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                                integracoes.Add(integracao);
                            }
                        }

                        await innovaDbContext.SaveChangesAsync();
                    }
                    catch (Exception ex)
                    {
                        string servico = "criarPedidoPIX // " + opp.Id;
                        var message = ex.InnerException != null ? ex.InnerException.Message : null;
                        if(!message.StartsWith("Violation of PRIMARY KEY constraint")){
                            string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                            SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                            integracoes.Add(integracao);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string servico = "criarPedidoMundiPaggSAPReenviar";
                var message = ex.InnerException != null ? ex.InnerException.Message : null;
                string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                integracoes.Add(integracao);
            }
            
            if(integracoes.Any()){
                var integracaoJSON = JsonConvert.SerializeObject(integracoes, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultadoIntegracao = controller().UpsertDados(integracaoJSON, "Integracao__c", "Id_Externo__c", "criarPedidoMundiPaggSAPReenviar");
            }
        }

        [AutomaticRetry(Attempts = 0)]
        [HttpGet("reenviarOportunidadesExportadas")]
        public async Task RessincronizarOportunidadesExportadas()
        {

            var client = controller().GetForceClient();
            List<string> idOpps = new List<string>();
            List<String> idsExternos = new List<String>();
            List<SfCustomIntegracao> integracoes = new List<SfCustomIntegracao>();
            
            SObjectAttributes attributoOpp = new SObjectAttributes();
            attributoOpp.Type = "Opportunity";
            DateTime inicio = DateTime.Now.Date.AddDays(-1);
            string sInicio = $"{inicio.ToString("yyyy-MM-ddT00:00:00.000+0000")}";
            
            try
            {
                DateTime agora = System.DateTime.Now.AddMinutes(181);
                
                String dataModificacao = agora.Year.ToString() + '-' + agora.Month.ToString().PadLeft(2, '0') + '-' + agora.Day.ToString().PadLeft(2, '0') + 'T' + agora.Hour.ToString().PadLeft(2, '0') + ':' + agora.Minute.ToString().PadLeft(2, '0') + ":00.000+0000" ;

                //List<SfCustomOpportunity> oportunidades = client.Query<SfCustomOpportunity>(SfCustomOpportunity.BuildSelect() + $" WHERE StageName = 'Exportado' AND LastModifiedDate >= 2022-07-01T00:00:00.000+0000 AND n_NF__c = null").Result;
                List<SfCustomOpportunity> oportunidades = client.Query<SfCustomOpportunity>(SfCustomOpportunity.BuildSelect() + $" WHERE StageName = 'Exportado' AND LastModifiedDate >= {sInicio} AND n_NF__c = null AND Codigo_Emitente__c != null AND CardCode__c != null AND LastModifiedDate < " + dataModificacao).Result;
                //List<SfCustomOpportunity> oportunidades = client.Query<SfCustomOpportunity>(SfCustomOpportunity.BuildSelect() + $" WHERE StageName = 'Exportado' AND LastModifiedDate >= {sInicio} AND n_NF__c = null AND Codigo__c = '0272194'").Result;
                
                var ids_opp = oportunidades.Select(s=> s.Codigo).Distinct().ToList();
                var lst_pedidos = innovaDbContext.Pedido.Where(c => ids_opp.Contains(c.IdPedido)).ToList();
                var lst_Produtos = innovaDbContext.PedidoItens.Where(c => ids_opp.Contains(c.PedidoId)).ToList();

                Pedido pNew = null;
                foreach (var opp in oportunidades)
                {
                    string empresa = opp.Empresa;

                    try
                    {
                        //List<string> idDocumentos = new List<string>();
                        if(lst_Produtos.Any(a=> a.PedidoId == opp.Codigo) && lst_pedidos.Any(a=> a.IdPedido == opp.Codigo)) {continue;}
                        SfEndereco endEntrega = await client.QuerySingle<SfEndereco>(SfEndereco.BuildSelect() + " WHERE Tipo__c = 'Endereço de Entrega' AND Conta__c = '" + opp.AccountId + "' limit 1", false);
                        List<SfCustomOpportunityLineItem> produtosOpp = await client.Query<SfCustomOpportunityLineItem>(SfCustomOpportunityLineItem.BuildSelect() + " WHERE OpportunityId = '" + opp.Id + "'");
                        //List<SfContentDocumentLink> documentosdaOpp = await client.Query<SfContentDocumentLink>("SELECT Id, ContentDocumentId, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId = '" + opp.Id + "'");
                        CondicoesPagamento_VDM condPag;

                        //foreach (var documento in documentosdaOpp) { idDocumentos.Add(documento.ContentDocumentId); }

                        //List<SfContentNote> notasOpp = await client.Query<SfContentNote>("SELECT Id, Title, TextPreview FROM ContentNote WHERE Id in ('" + String.Join("','", idDocumentos) + "')");

                        if (opp.FormaDePagamento != null) 
                        { condPag = innovaDbContext.CondicoesPagamento_VDM.Where(cPag => cPag.Descricao == opp.FormaDePagamento).FirstOrDefault(); }
                        else { condPag = innovaDbContext.CondicoesPagamento_VDM.Where(cPag => cPag.Descricao == "00").FirstOrDefault(); }

                        var pedidoSAP = innovaDbContext.Pedido.Where(pedido => pedido.IdPedido == opp.Codigo).FirstOrDefault();;
                        
                        Boolean inserirItens = true;
                        try{
                            if(pedidoSAP == null){
                                pNew = new Pedido();

                                pNew.Origem = "4";
                                pNew.Status = null;
                                pNew.PcDesconto = 0;
                                pNew.IdPedidoSap = "F";
                                pNew.PcDescontoFin = 0;
                                pNew.IdPedido = opp.Codigo;
                                pNew.CodigoPlanoNegocio = "1";
                                pNew.CodigoTransportadora = "0";
                                pNew.DataEnvio = System.DateTime.Now;
                                pNew.CodigoCliente = opp.ContaCardCode;
                                pNew.ObservacaoNf = opp.ObservacesDaNF;
                                pNew.CodigoEmitente = opp.CodigoEmitente;
                                var CodigoVendedor = opp.CodVendedor.Split(".");
                                pNew.CodigoVendedor = CodigoVendedor[0];
                                pNew.CodAutorizacao = opp.Codigo_de_Autorizacao__c;
                                pNew.DataPedido = opp.CreatedDate.Value.UtcDateTime;
                                pNew.CodigoTipoPagamento = condPag.Codigo.ToString();
                                if(opp.CodigoTransportadora != null) pNew.CodigoTransportadora = opp.CodigoTransportadora;
                                pNew.Empresa = empresa != null ? empresa : "SBOEquilibrium";
                                pNew.Indicador = opp.VIP ? "VIP" : empresa == "Navarro" ? "N" : null;
                                pNew.Id_Salesforce = opp.Id;
                                pNew.Codigo_TipoEntrega = opp.Oportunidade_em_evento__c ? "2" : "1";
                                pNew.Bandeira_Cartao1 = opp.Bandeira__c;
                                pNew.Bandeira_Cartao2 = opp.Bandeira_2__c;
                                if(opp.Numero_de_Parcelas__c != null){
                                    pNew.N_Parcela_Cartao1 = Convert.ToInt32(opp.Numero_de_Parcelas__c);
                                }
                                if(opp.Numero_de_Parcelas_2__c != null){
                                    pNew.N_Parcela_Cartao2 = Convert.ToInt32(opp.Numero_de_Parcelas_2__c);
                                }
                                pNew.CodAutorizacao_Cartao1 = opp.NSU_1__c;
                                pNew.CodAutorizacao_Cartao2 = opp.NSU_2__c;
                                pNew.Valor_Cartao1 = opp.Valor_cartao_1__c;
                                pNew.Valor_Cartao2 = opp.Valor_cartao_2__c;

                                /*string ObservacesDoPedido = "";
                                Inativado por falta de tamanho no banco 
                                int contador = 1;
                                foreach (var nota in notasOpp){ObservacesDoPedido += contador + "ª Nota: " + nota.Title + " \r\nDescrição: " + nota.TextPreview + " \r\n// ";contador++;}*/
                                //1ª Nota: NÃO SEPARAR Descrição: NÃO DEVE SER FEITA A SEPARAÇÃO DO PRODUTO! 

                                pNew.ObservacaoPedido = opp.Description;

                                var nabota = produtosOpp.FirstOrDefault(p => p.ProductCode == "1162957");
                                var desoxicolato = produtosOpp.FirstOrDefault(p => p.ProductCode == "0328910");
                                var outroProduto = produtosOpp.FirstOrDefault(p => p.ProductCode != "1162957");

                                bool possuiNabota = nabota != null;;
                                bool possuiDesoxicolato = desoxicolato != null;
                                bool possuiOutrosProdutos = outroProduto != null;

                                Frete frete = controller().calculaFrete(opp, possuiNabota, possuiOutrosProdutos, possuiDesoxicolato, endEntrega.Estado, endEntrega.CodigoCidade, "1");
                                
                                pNew.CodigoTipoFrete = frete.CodigoTipoFrete;
                                pNew.ValorFrete = frete.ValorFrete;

                                if (opp.Condicao.Contains("Boleto"))
                                {
                                    pNew.CodigoFormaPagamento = "BOLETO";
                                }
                                else if (opp.Condicao.Contains("Cartão"))
                                {
                                    pNew.CodigoFormaPagamento = "CARTAO";
                                }
                                else if (opp.Condicao.Contains("Depósito Bancário"))
                                {
                                    pNew.CodigoFormaPagamento = "DEPOSITO";
                                }
                                else if (opp.Condicao.Contains("PIX"))
                                {
                                    pNew.CodigoFormaPagamento = "PIX";
                                }
                                else if (opp.Condicao.Contains("Carteira") || condPag.Codigo.ToString() == "53")
                                {
                                    pNew.CodigoFormaPagamento = "CARTEIRA";
                                }
                                pNew.FormaPagamento = opp.Condicao;

                                await innovaDbContext.Pedido.AddAsync(pNew);
                                await innovaDbContext.SaveChangesAsync();
                            }
                        }
                        catch (Exception ex)
                        {
                            string servico = "criarPedidoSAPReenviar // " + opp.Id;
                            var message = ex.InnerException != null ? ex.InnerException.Message : null;
                            string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                            if(message == null || !message.StartsWith("Violation of PRIMARY KEY constraint")){
                                SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                                integracoes.Add(integracao);
                            }
                            inserirItens = false;
                        }
                          
                        //List<string> codProdutos = new List<string>{"0063982","0064006","0064014","0064031","0064032","0064951","0064030","0064156","0064055","0064048","0064019","0064020","0064018","0064021","0064043","0064050","0064051","0064047","0064046","0064049","0309960"};

                        if(inserirItens){
                            foreach (var prodOpp in produtosOpp)
                            {
                                try{
                                    if(lst_Produtos.Any(a=> a.IdSalesforce == prodOpp.Id)) {continue;}
                                    decimal valorVenda = prodOpp.UnitPrice??0;

                                    //if(prodOpp.Empresa == "SBOInnovaBR")
                                    //if(prodOpp.CodigoProduto == "0064030" || prodOpp.CodigoProduto == "0064014")
                                    //{
                                        int tipoNegociacao = 1;

                                        if (opp.Type == "Bonificação Saída") { tipoNegociacao = 9;}
                                        else if (opp.Type == "Doação"){tipoNegociacao = 13;}
                                        else if (opp.Type == "Produtos promocionais"){tipoNegociacao = 42;}
                                        else if (opp.Type == "Feiras, Cong e Eventos"){tipoNegociacao = 59;}
                                        else if (opp.Type == "Consumo Interno"){tipoNegociacao = 65;}
                                        else if (opp.Type == "Venda (Desc. Incond.)"){tipoNegociacao = 68;}

                                        valorVenda = controller().getIpi(opp.ContaCardCode, prodOpp.ProductCode, tipoNegociacao, prodOpp.UnitPrice);
                                    //}

                                    PedidoItens produtoNew = new PedidoItens();

                                    produtoNew.Desconto = 0;
                                    produtoNew.PcDesconto = 0;
                                    produtoNew.ItemSequencia = 1;
                                    produtoNew.PedidoId = opp.Codigo;
                                    produtoNew.IdSalesforce = prodOpp.Id;
                                    produtoNew.Quantidade = Int32.Parse(prodOpp.Quantity.ToString());
                                    produtoNew.ItemCodigo = prodOpp.CodigoProduto;
                                    produtoNew.QuantidadeVenda = Int32.Parse(prodOpp.Quantity.ToString());
                                    produtoNew.ValorVenda = valorVenda > 0 ? valorVenda : prodOpp.UnitPrice;
                                    produtoNew.Empresa = empresa != null ? empresa : "SBOEquilibrium";
                                    produtoNew.TotalTabela = prodOpp.ValorCombo;
                                    
                                    if (opp.Type == "Feiras, Cong e Eventos")
                                    {
                                        produtoNew.TipoUtilizacao = "59";
                                    }
                                    else if (opp.Type == "Bonificação Saída")
                                    {
                                        produtoNew.TipoUtilizacao = "9";
                                    }
                                    else if (opp.Type == "Doação")
                                    {
                                        produtoNew.TipoUtilizacao = "13";
                                    }
                                    else if (opp.Type == "Produtos promocionais")
                                    {
                                        produtoNew.TipoUtilizacao ="31";
                                    }
                                    else if (opp.Type == "Consumo Interno")
                                    {
                                        produtoNew.TipoUtilizacao ="65";
                                    }
                                    else if (opp.Type == "Venda (Desc. Incond.)")
                                    {
                                        produtoNew.TipoUtilizacao ="68";
                                    }
                                    else
                                    {
                                        produtoNew.TipoUtilizacao = "1";
                                    }

                                    await innovaDbContext.AddAsync(produtoNew);
                                }
                                catch (Exception ex)
                                {
                                    string servico = "criarItemPedidoSAPReenviar // " + prodOpp.Id;
                                    var message = ex.InnerException != null ? ex.InnerException.Message : null;
                                    string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                                    SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                                    integracoes.Add(integracao);
                                }
                                    
                            }
                            await innovaDbContext.SaveChangesAsync();
                        }
                    }
                    catch(Exception ex)
                    {
                        string servico = "criarPedidoReenviar // " + opp.Id;
                        if(!idsExternos.Contains(servico))
                        {
                            var message = ex.InnerException != null ? ex.InnerException.Message : null;
                            string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                            SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                            integracoes.Add(integracao);
                            idsExternos.Add(servico);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string servico = "criarPedidoReenviar";
                var message = ex.InnerException != null ? ex.InnerException.Message : null;
                string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                integracoes.Add(integracao);
            }
            
            if(integracoes.Any()){
                var integracaoJSON = JsonConvert.SerializeObject(integracoes, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultadoIntegracao = controller().UpsertDados(integracaoJSON, "Integracao__c", "Id_Externo__c", "criarPedidoReenviar");
            }
        }

        [AutomaticRetry(Attempts = 0)]
        [HttpGet("upsertEquipeContas")]
        public void UpsertEquipeContas()
        {
            List<String> idsExternos = new List<String>();
            List<SfCustomIntegracao> integracoes = new List<SfCustomIntegracao>();
            List<SfAccountTeamMember> accountTeam = new List<SfAccountTeamMember>();

            SObjectAttributes atributoAccountTeam = new SObjectAttributes();
            atributoAccountTeam.Type = "AccountTeamMember";
            
            SObjectAttributes atributoIntegracao = new SObjectAttributes();
            atributoIntegracao.Type = "Integracao__c";
            
            int qtdMinutos = -30;

                var client = controller().GetForceClient();
            List<SfCustomUser> vendedores = controller().GetVendedores(client);
            
            var clientes = innovaDbContext.VW_VendedoresAdicional_InnovaBrasil.Where(c => c.Data >= System.DateTime.Now.AddMinutes(qtdMinutos) && (c.CodigoVendedor != null)).ToList();
            //var clientes = innovaDbContext.VW_VendedoresAdicional_InnovaBrasil.ToList();
            
            try
            {
                foreach (var dadoSAP in clientes)
                {
                    try
                    {
                        string idConta = null;
                        string idUser = null;

                        var contaLog = innovaDbContext.LogCadastro.FirstOrDefault(c => c.CardCode_Cliente == dadoSAP.CodigoCliente);
                        
                        if(contaLog != null){
                            idConta = contaLog.Id_SF_Cliente;
                        } else {
                            continue;
                        }

                        string papelVendedor = dadoSAP.Tecnico == "S" ? "Vendedor Técnico" : "Vendedor Omnichannel";
                    
                        var vendedorUser = vendedores.FirstOrDefault(u => u.CodigoVendedorInt == dadoSAP.CodigoVendedor || u.CodigoVendedorMadInt == dadoSAP.CodigoVendedor);

                        if(vendedorUser == null){
                            string servico = "upsertEquipeContas / " + dadoSAP.CodigoCliente;            
                            if(!idsExternos.Contains(servico))
                            {
                                string mensagemErro ="Erro ao processar\"" + papelVendedor + "\"pois não possui usuário cadastrado/ativo com o código informado: " + dadoSAP.CodigoVendedor;
                                SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                                integracoes.Add(integracao);
                                idsExternos.Add(servico);
                            }
                            continue;
                        }else{
                                    idUser = vendedorUser.Id;
                        }

                        string idExterno = dadoSAP.CodigoCliente + dadoSAP.CodigoVendedor;

                        SfAccountTeamMember accountTeamMember = new SfAccountTeamMember();
                        accountTeamMember.AccountId = idConta;
                        accountTeamMember.Attributes = atributoAccountTeam;
                        accountTeamMember.TeamMemberRole = papelVendedor;
                        accountTeamMember.UserId = idUser;
                        accountTeamMember.Id_Externo__c = idExterno;
                        accountTeamMember.AccountAccessLevel = "Edit";
                        accountTeamMember.OpportunityAccessLevel = "Read";
                        accountTeamMember.CaseAccessLevel = "Read";
                        accountTeamMember.ContactAccessLevel = "Edit";

                        accountTeam.Add(accountTeamMember);
                    }
                    catch(Exception ex)
                    {
                        string servico = "upsertEquipeContas // " + dadoSAP.CodigoCliente;
                        if(!idsExternos.Contains(servico))
                        {
                            var message = ex.InnerException != null ? ex.InnerException.Message : null;
                            string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                            SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                            integracoes.Add(integracao);
                            idsExternos.Add(servico);
                        }
                    }
                }

                var accountTeamJSON = JsonConvert.SerializeObject(accountTeam, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var results = controller().UpsertDados(accountTeamJSON, "AccountTeamMember", "Id_Externo__c", "upsertEquipeContas");
            }
            catch(Exception ex)
            {
                string servico = "upsertEquipeContas";
                if(!idsExternos.Contains(servico))
                {
                    var message = ex.InnerException != null ? ex.InnerException.Message : null;
                    string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                    SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                    integracoes.Add(integracao);
                    idsExternos.Add(servico);
                }
            }

            if(integracoes.Any()){
                var integracaoJSON = JsonConvert.SerializeObject(integracoes, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultadoIntegracao = controller().UpsertDados(integracaoJSON, "AccountTeamMember", "Id_Externo__c", "upsertEquipeContas");
            }
        }

        [AutomaticRetry(Attempts = 0)]
        [HttpGet("upsertGrupoEconomico")]
        public void UpsertGrupoEconomico()
        {

            List<String> idsExternos = new List<String>();
            List<SfCustomIntegracao> integracoes = new List<SfCustomIntegracao>();
            List<SfGrupoEconomico> gruposEconomicosList = new List<SfGrupoEconomico>();

            SObjectAttributes atributoGrupoEconomico = new SObjectAttributes();
            atributoGrupoEconomico.Type = "Grupo_economico__c";
            
            SObjectAttributes atributoIntegracao = new SObjectAttributes();
            atributoIntegracao.Type = "Integracao__c";
            
            int qtdDias = -2;

                var client = controller().GetForceClient();
            List<String> cardCodes = new List<String>();
            
            var gruposEconomicos = innovaDbContext.VW_GrupoEconomico_InnovaBrasil.Where(c => c.Data >= System.DateTime.Now.AddDays(qtdDias)).ToList();

            foreach (var dadoSAP in gruposEconomicos)
            {
                if(!cardCodes.Contains(dadoSAP.CodigoGrupo)){
                    cardCodes.Add(dadoSAP.CodigoGrupo);
                }

                if(dadoSAP.Codigo != null && !cardCodes.Contains(dadoSAP.Codigo)){
                    cardCodes.Add(dadoSAP.Codigo);
                }
            }

            String cardCodesString = "";

            foreach(var cardCode in cardCodes){
                if(cardCodesString == ""){
                    cardCodesString = "'" + cardCode + "'";
                }else{
                    cardCodesString += ", '" + cardCode + "'";
                }
            }

            if(cardCodesString == ""){
                return;
            }

            String query = SfCustomAccount.BuildSelect() + " WHERE CardCode__c in (" + cardCodesString + ")";

            List<SfCustomAccount> accs = client.Query<SfCustomAccount>(query, false).Result;                

            try
            {
                foreach (var dadoSAP in gruposEconomicos)
                {
                    string idExterno = dadoSAP.Documento + '_' + dadoSAP.CodigoGrupo;
                    
                    try
                    {
                        string idConta = null;
                        string idSocio = null;

                        var acc = accs.FirstOrDefault(c => c.CardCode == dadoSAP.CodigoGrupo);
                        var socio = accs.FirstOrDefault(c => c.CardCode == dadoSAP.Codigo);
                        
                        if(acc != null){
                            idConta = acc.Id;
                        } else {
                            continue;
                        }
                        
                        if(socio != null){
                            idSocio = socio.Id;
                        }

                        SfGrupoEconomico grupoEconomico = new SfGrupoEconomico();
                        grupoEconomico.Conta = idConta;
                        grupoEconomico.Attributes = atributoGrupoEconomico;
                        grupoEconomico.Socio = idSocio;
                        grupoEconomico.Documento = dadoSAP.Documento;
                        grupoEconomico.percCapital = dadoSAP.Capital;
                        grupoEconomico.CapitalSocial = dadoSAP.CapitalSocial;
                        grupoEconomico.DataEntrada = dadoSAP.DataEntrada;
                        grupoEconomico.Nome_do_Socio = dadoSAP.Nome;
                        grupoEconomico.Observacoes = dadoSAP.Observacao;
                        grupoEconomico.IdExterno = idExterno;

                        gruposEconomicosList.Add(grupoEconomico);
                    }
                    catch(Exception ex)
                    {
                        string servico = "upsertGrupoEconomico // " + idExterno;
                        if(!idsExternos.Contains(servico))
                        {
                            var message = ex.InnerException != null ? ex.InnerException.Message : null;
                            string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                            SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                            integracoes.Add(integracao);
                            idsExternos.Add(servico);
                        }
                    }
                }

                var grupoEconomicoJSON = JsonConvert.SerializeObject(gruposEconomicosList, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var results = controller().UpsertDados(grupoEconomicoJSON, "Grupo_economico__c", "Id_Externo__c", "upsertGrupoEconomico");
            }
            catch(Exception ex)
            {
                string servico = "upsertGrupoEconomico";
                if(!idsExternos.Contains(servico))
                {
                    var message = ex.InnerException != null ? ex.InnerException.Message : null;
                    string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                    SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                    integracoes.Add(integracao);
                    idsExternos.Add(servico);
                }
            }

            if(integracoes.Any()){
                var integracaoJSON = JsonConvert.SerializeObject(integracoes, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultadoIntegracao = controller().UpsertDados(integracaoJSON, "Grupo_economico__c", "Id_Externo__c", "upsertGrupoEconomico");
            }
        }

        [AutomaticRetry(Attempts = 0)]
        [HttpGet("sincronizarHistorico")]
        public async Task SincronizarHistorico()
        {
             

                var client = controller().GetForceClient();

            DateTime dataAlteracao = System.DateTime.Now.AddHours(-1);
            String ano = dataAlteracao.Year.ToString();
            String mes = dataAlteracao.Month.ToString().PadLeft(2, '0');
            String dia = dataAlteracao.Day.ToString().PadLeft(2, '0');

            List<String> idsExternos = new List<String>();
            List<SfCustomIntegracao> integracoes = new List<SfCustomIntegracao>();
            
            try
            {
                List<SfCustomHistoryAccount> historiesAccount = client.Query<SfCustomHistoryAccount>(SfCustomHistoryAccount.BuildSelect()  + " WHERE (CreatedById != '0051U000001AetCQAS' OR Field = 'CardCode__c') AND Field not in ('Saldo__c', 'Email_xML__c', 'Financeiro__c', 'Limite_de_Credito__c', 'locked', 'unlocked', 'PersonMobilePhone', 'personAccountUpdatedByLead', 'Description', 'Phone') AND CreatedDate > "+ano+"-"+mes+"-"+dia+"T00:00:00.000+0000", false).Result;
                

                foreach (var historic in historiesAccount)
                {
                    try
                    {
                        
                        LogCadastro logCadastro = innovaDbContext.LogCadastro.FirstOrDefault(c => c.Id == historic.Id);
                        
                        LogCadastro logCadastroNew = new LogCadastro();

                        logCadastroNew.Id = historic.Id;
                        logCadastroNew.Id_SF_Usuário = historic.CreatedById;
                        logCadastroNew.Nome_Usuario = historic.CreatedBy.Name;
                        logCadastroNew.Id_SF_Cliente = historic.AccountId;
                        logCadastroNew.Nome_Cliente = historic.CustomAccount.Name;
                        logCadastroNew.CardCode_Cliente = historic.CustomAccount.CardCode;
                        logCadastroNew.Campo_Alterado = historic.Field;
                        logCadastroNew.Data_Cadastro = historic.CustomAccount.CreatedDate.Value.UtcDateTime;
                        logCadastroNew.Data_de_Alteração = historic.CreatedDate.Value.UtcDateTime;
                        logCadastroNew.Valor_Anterior = historic.OldValue;
                        logCadastroNew.Valor_Novo = historic.NewValue;

                        if (logCadastro == null)
                        {
                            await innovaDbContext.AddAsync(logCadastroNew);
                        }

                        innovaDbContext.SaveChanges();
                    }
                    catch(Exception ex)
                    {
                        string servico = "criarHistoricoCliente // " + historic.Id;
                        if(!idsExternos.Contains(servico))
                        {
                            var message = ex.InnerException != null ? ex.InnerException.Message : null;
                            string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                            SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                            integracoes.Add(integracao);
                            idsExternos.Add(servico);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string servico = "criarHistoricoCliente";
                var message = ex.InnerException != null ? ex.InnerException.Message : null;
                string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                integracoes.Add(integracao);
            }
            
            if(integracoes.Any()){
                var integracaoJSON = JsonConvert.SerializeObject(integracoes, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultadoIntegracao = controller().UpsertDados(integracaoJSON, "Integracao__c", "Id_Externo__c", "criarHistoricoCliente");
            }
        }

        [AutomaticRetry(Attempts = 0)]
        [HttpGet("getAprovacoes")]
        public async Task GetAprovacoes()
        {
            //int diasInicio = -1;
            //int horaInicial = 0;

            //for (int i = 0; i < 1000; i++)
           //{
                //int diasFinal = diasInicio + 1;
                List<SfCustomIntegracao> integracoes = new List<SfCustomIntegracao>();
                
                
                try{

                var client = controller().GetForceClient();

                    //Data de solicatação
                    DateTime inicio = DateTime.Now;
                    DateTime fim = DateTime.Now.AddMinutes(180);
                    //DateTime fim = new DateTime(2021, 07, 20, 0, 0, 0);
                    //fim = fim.AddHours(horaInicial);
                    //DateTime inicio = fim.AddHours(-1);
                    //horaInicial++;
                    //DateTime inicio = DateTime.Today.AddDays(diasInicio);
                    //DateTime fim = DateTime.Today.AddDays(diasFinal);
                    //string sInicio = $"2022-09-25";
                    //string sInicio = $"{inicio.ToString("yyyy-MM-dd")}";
                    string sInicio = $"{inicio.ToString("yyyy-MM-ddTHH:mm:ss.000+0000")}";
                    //string sFim = $"2022-10-01";
                    //string sFim = $"{fim.ToString("yyyy-MM-dd")}";
                    string sFim = $"{fim.ToString("yyyy-MM-ddTHH:mm:ss.000+0000")}";

                    List<string> idsSolicitacao = new List<string>();
                    List<SfAprovacao> aprovacoes = await client.Query<SfAprovacao>(SfAprovacao.BuildSelect()  + $@" WHERE Oportunidade__c != null AND Tipo__c = 'Oportunidade' and LastModifiedDate >= {sInicio} and LastModifiedDate < {sFim}");

                    //if(!aprovacoes.Any()){
                        //break;
                   //}
                    
                    //List<SfAprovacao> aprovacoes = await client.Query<SfAprovacao>($@"SELECT Id, Nome_da_conta__c, Conta__c, CardCode__c, Data_da_Solicitacao__c, Data_da_resposta__c, Solicitante__c, Nome_do_solicitante__c, Aprovador__c, Nome_do_aprovador__c, Etapa__c, Status__c, Motivo_de_recusa__c, Comentario__c, Tempo_decorrido_dias__c,Tempo_decorrido_horas__c,Tempo_decorrido_minutos__c, Tipo__c, Codigo_Oportunidade__c, Codigo_Solicitante__c, Segmento__c, Tipo_da_Oportunidade__c, Codigo_SAP__c FROM Aprovacao__c WHERE Id = 'a0QHY000008SMBX2A4'");
                    
                    //List<SfAprovacao> aprovacoes = await client.Query<SfAprovacao>($@"SELECT Id, Nome_da_conta__c, Conta__c, CardCode__c, Data_da_Solicitacao__c, Data_da_resposta__c, Solicitante__c, Nome_do_solicitante__c, Aprovador__c, Nome_do_aprovador__c, Etapa__c, Status__c, Motivo_de_recusa__c, Comentario__c, Tempo_decorrido_dias__c,Tempo_decorrido_horas__c,Tempo_decorrido_minutos__c, Tipo__c, Codigo_Oportunidade__c, Codigo_Solicitante__c, Segmento__c, Tipo_da_Oportunidade__c, Codigo_SAP__c FROM Aprovacao__c WHERE (Tipo__c = 'Oportunidade' and  Oportunidade__r.StageName = 'Exportado' and (Oportunidade__r.CloseDate >= {sInicio} and Oportunidade__r.CloseDate =< {sFim}))");

                    List<string> list_ids = new List<string>();
                    List<TB_Atividade_SAP> list_TB_Atividade_SAP = new List<TB_Atividade_SAP>();
                    foreach (var item in aprovacoes.Where(w=> w.Tipo__c != "Conta"))
                    {
                        list_ids.Add(item.Id);
                    }

                    if(list_ids.Any())
                    {
                        list_TB_Atividade_SAP = innovaDbContext.TB_Atividade_SAP.Where(w => list_ids.Contains(w.ID_registro_CRM)).ToList();
                    }

                    TB_Atividade_SAP atividade = null;
                    var count = 0;
                    foreach (var item in aprovacoes)
                    {
                        count ++;
                        try{
                            if(item.Tipo__c == "Conta")
                            {
                                Cliente_Solicitacao solicitacao = new Cliente_Solicitacao();
                                solicitacao.Status = item.Status__c;
                                solicitacao.Id_Solicitacao = item.Id;
                                solicitacao.CardCode = item.CardCode__c;
                                solicitacao.ID_SF_Cliente = item.Conta__c;
                                solicitacao.Etapa_Aprovacao = item.Etapa__c;
                                solicitacao.ID_SF_Aprovador = item.Aprovador__c;
                                solicitacao.Nome_Cliente = item.Nome_da_conta__c;
                                solicitacao.ID_SF_Solicitante = item.Solicitante__c;
                                solicitacao.Motivo_Recusa = item.Motivo_de_recusa__c;
                                solicitacao.Data_Resposta = item.Data_da_resposta__c;
                                solicitacao.Nome_Aprovador = item.Nome_do_aprovador__c;
                                solicitacao.Nome_Solicitante = item.Nome_do_solicitante__c;
                                solicitacao.Data_Solicitacao = item.Data_da_Solicitacao__c;

                                if(idsSolicitacao.Contains(item.Id))
                                {
                                    innovaDbContext.Update(solicitacao);
                                }else{
                                    await innovaDbContext.AddAsync(solicitacao);
                                }  
                            }else{
                                Boolean registroCadastrado = list_TB_Atividade_SAP.Any(a=> a.ID_registro_CRM == item.Id);

                                atividade = registroCadastrado ? list_TB_Atividade_SAP.FirstOrDefault(a => a.ID_registro_CRM == item.Id) : new TB_Atividade_SAP();
                                
                                atividade.Doc_Relacionado = item.Codigo_Oportunidade__c;
                                atividade.Cod_Cliente = item.CardCode__c;
                                if(item.Data_da_Solicitacao__c != null){atividade.Data_Inicio = item.Data_da_Solicitacao__c.Value.Date;}
                                if(item.Data_da_resposta__c != null){atividade.Data_Termino = item.Data_da_resposta__c.Value.Date;}
                                if(item.Data_da_Solicitacao__c != null){atividade.Hora_Inicio = item.Data_da_Solicitacao__c.Value.TimeOfDay;}
                                if(item.Data_da_resposta__c != null){atividade.Hora_Termino = item.Data_da_resposta__c.Value.TimeOfDay;}
                                atividade.Status = item.Status__c;
                                atividade.Tipo = item.Tipo_da_Oportunidade__c;
                                if(item.Etapa__c != null){atividade.Atividade = item.Etapa__c.Length > 80 ? item.Etapa__c.Substring(0, 80) : item.Etapa__c;}
                                atividade.Conteudo = item.Comentario__c != null ? item.Comentario__c.Length > 1200 ? item.Comentario__c.Substring(1200) : item.Comentario__c : ""; //NO banco esta como nvarchar 1
                                if(!registroCadastrado){atividade.ID_registro_CRM = item.Id;}
                                if(item.Tempo_decorrido_horas__c != null){atividade.Duracao_Horas = decimal.Round(item.Tempo_decorrido_horas__c.Value, 2);} // o Campo Tempo_decorrido_horas__c é double, 
                                if(item.Tempo_decorrido_minutos__c != null){atividade.duracao_Minutos = decimal.Round(item.Tempo_decorrido_minutos__c.Value, 2);} // o Campo Tempo_decorrido_minutos__c é double
                                if(item.Tempo_decorrido_dias__c != null){atividade.duracao_Dias = decimal.Round(item.Tempo_decorrido_dias__c.Value,2 );} // o Campo Tempo_decorrido_dias__c é double
                                atividade.COD_Solicitante = item.Codigo_Solicitante__c;
                                if(item.Nome_da_conta__c != null){atividade.Nome_Conta = item.Nome_da_conta__c.Length > 255 ? item.Nome_da_conta__c.Substring(0, 255) : item.Nome_da_conta__c;}
                                atividade.Segmento = item.Segmento__c != null ? item.Segmento__c.Length > 80 ? item.Segmento__c.Substring(0, 80) : item.Segmento__c : null;
                                atividade.Aprovador = item.Nome_do_aprovador__c;
                                atividade.DOC_relacionado_SAP = item.Codigo_SAP__c;
                                atividade.DT_Criacao = DateTime.Now;
                                atividade.NM_Empresa = item.CustomOpportunity.Empresa;
                                atividade.Forma_Pagamento = item.CustomOpportunity.Condicao;
                                
                                if(registroCadastrado)
                                {
                                    innovaDbContext.Update(atividade);
                                }else{
                                    innovaDbContext.Add(atividade);
                                }  
                                var result = innovaDbContext.SaveChanges();
                            }
                        }
                        catch(Exception ex)
                        {
                            if(atividade != null)
                                innovaDbContext.Remove(atividade);
                            string servico = "getAprovacoesSAP // " + item.Id;
                            var message = ex.InnerException != null ? ex.InnerException.Message : null;
                            string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                            SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                            integracoes.Add(integracao);
                        }
                    }
                            
                    innovaDbContext.SaveChanges();
                }
                catch(Exception ex)
                {
                    string servico = "EnviarAprovacoesSAP";
                    var message = ex.InnerException != null ? ex.InnerException.Message : null;
                    string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                    SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                    integracoes.Add(integracao);
                }
                
                if(integracoes.Any()){
                    var integracaoJSON = JsonConvert.SerializeObject(integracoes, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                    var resultadoIntegracao = controller().UpsertDados(integracaoJSON, "Integracao__c", "Id_Externo__c", "EnviarAprovacoesSAP");
                }
                //diasInicio = diasFinal;
            //}
        }

        [AutomaticRetry(Attempts = 0)]
        [HttpGet("sincronizarHistoricoFases")]
        public async Task SincronizarHistoricoFases()
        {
            //int diasFinal = 1;

            //for (int i = 0; i < 1000; i++)
            //{
                //int diasInicio = diasFinal -2;
                

                var client = controller().GetForceClient();


                DateTime inicio = System.DateTime.Now.AddHours(2);
                //DateTime inicio = DateTime.Today.AddDays(diasInicio);
                //DateTime fim = DateTime.Today.AddDays(diasFinal);
                //string sInicio = $"2022-09-25";
                //string sInicio = $"{inicio.ToString("yyyy-MM-dd")}";
                string sInicio = $"{inicio.ToString("yyyy-MM-ddTHH:mm:ss.000+0000")}";
                //string sFim = $"2022-10-01";
                //string sFim = $"{fim.ToString("yyyy-MM-dd")}";
                //string sFim = $"{fim.ToString("yyyy-MM-ddTHH:mm:ss.000+0000")}";

                List<String> idsExternos = new List<String>();
                List<SfCustomIntegracao> integracoes = new List<SfCustomIntegracao>();
                
                try
                {
                    List<SfCustomHistoryOpportunity> historiesOpportunity = client.Query<SfCustomHistoryOpportunity>(SfCustomHistoryOpportunity.BuildSelect()  + $@" WHERE SystemModstamp >= {sInicio}", false).Result;
                    //List<SfCustomHistoryOpportunity> historiesOpportunity = client.Query<SfCustomHistoryOpportunity>(SfCustomHistoryOpportunity.BuildSelect()  + $@" WHERE SystemModstamp >= {sInicio} AND SystemModstamp < {sFim}", false).Result;

                    List<string> list_ids = new List<string>();
                    List<TB_Fases_Oportunidade> list_TB_Fases_Oportunidade = new List<TB_Fases_Oportunidade>();

                    foreach (var historic in historiesOpportunity){
                        list_ids.Add(historic.Id);
                    }

                    if(list_ids.Any())
                    {
                        list_TB_Fases_Oportunidade = innovaDbContext.TB_Fases_Oportunidade.Where(w => list_ids.Contains(w.ID_Fase)).ToList();
                    }

                    //if(!historiesOpportunity.Any()){
                        //break;
                    //}

                    foreach (var historic in historiesOpportunity)
                    {
                        try
                        {
                            
                            Boolean registroCadastrado = list_TB_Fases_Oportunidade.Any(a=> a.ID_Fase == historic.Id);
                            
                            TB_Fases_Oportunidade logFasesNew = registroCadastrado ? list_TB_Fases_Oportunidade.FirstOrDefault(c => c.ID_Fase == historic.Id) : new TB_Fases_Oportunidade();

                            logFasesNew.NM_Empresa = historic.CustomOpportunity.Empresa;
                            logFasesNew.N_Oportunidade = Convert.ToString(historic.CustomOpportunity.Codigo);
                            logFasesNew.Data_Inicio = historic.CreatedDate.Value.Date;
                            logFasesNew.Data_Termino = historic.SystemModstamp.Value.Date;
                            logFasesNew.Hora_Inicio = historic.CreatedDate.Value.TimeOfDay;
                            logFasesNew.Hora_Termino = historic.SystemModstamp.Value.TimeOfDay;
                            logFasesNew.Fase = historic.StageName;
                            logFasesNew.Forma_Pagamento = historic.CustomOpportunity.Condicao;

                            var dataCriacao = historic.CreatedDate.Value;
                            var dataModificacao = historic.SystemModstamp.Value;
                            var dt1 = new DateTime(dataCriacao.Year, dataCriacao.Month, dataCriacao.Day, dataCriacao.Hour, dataCriacao.Minute, dataCriacao.Second);
                            var dt2 = new DateTime(dataModificacao.Year, dataModificacao.Month, dataModificacao.Day, dataModificacao.Hour, dataModificacao.Minute, dataModificacao.Second);
                        
                            TimeSpan duracao = dt2 - dt1;
                            logFasesNew.Duracao_Dias = Math.Round(Convert.ToDecimal(duracao.TotalDays), 2);
                            logFasesNew.Duracao_Horas = Math.Round(Convert.ToDecimal(duracao.TotalHours), 2);
                            logFasesNew.Duracao_Minutos = Math.Round(Convert.ToDecimal(duracao.TotalMinutes), 2);

                            if(registroCadastrado)
                            {
                                innovaDbContext.Update(logFasesNew);
                            }else{
                                logFasesNew.ID_Fase = historic.Id;
                                innovaDbContext.Add(logFasesNew);
                            }  
                            //var result = innovaDbContext.SaveChanges();
                        }
                        catch(Exception ex)
                        {
                            string servico = "criarHistoricoFases // " + historic.Id;
                            if(!idsExternos.Contains(servico))
                            {
                                var message = ex.InnerException != null ? ex.InnerException.Message : null;
                                string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                                SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                                integracoes.Add(integracao);
                                idsExternos.Add(servico);
                            }
                        }
                    }

                    await innovaDbContext.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    string servico = "criarHistoricoFases";
                    var message = ex.InnerException != null ? ex.InnerException.Message : null;
                    string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                    SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                    integracoes.Add(integracao);
                }
                
                if(integracoes.Any()){
                    var integracaoJSON = JsonConvert.SerializeObject(integracoes, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                    var resultadoIntegracao = controller().UpsertDados(integracaoJSON, "Integracao__c", "Id_Externo__c", "criarHistoricoFases");
                }
                //diasFinal = diasInicio;
            //}
        }
    }
}