using System;
using System.Data;
using SfCustom.Models;
using NetCoreForce.Client;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using InnovaPharmaMiddleware.Models;
using System.Text.RegularExpressions;
using InnovaPharmaMiddleware.Helpers;
using Microsoft.Extensions.Caching.Memory;

using System.Text;
using System.Linq;
using System.Net.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using NetCoreForce.Client.Models;

namespace InnovaPharmaMiddleware.Controllers
{
    public class HandlerController : ControllerBase
    {
        public InnovaDbContext innovaDbContext;

        public ProdutoHelper produtoHelper;
        public IMemoryCache cache;
        public static int hours = 3;

        public HandlerController(InnovaDbContext sc, ProdutoHelper _produtoHelper, IMemoryCache _cache)
        {
            cache = _cache;
            innovaDbContext = sc;
            produtoHelper = _produtoHelper;
        }
        public ForceClient GetForceClient()
        {
            return cache.GetOrCreate("SFClient", entry =>
            {
                entry.AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(40);
                return SalesforceHelper.newClient();
            });
        }
        
        public List<SfCustomUser> GetVendedores(ForceClient client)
        {
            return cache.GetOrCreate("Vendedores", entry =>
            {
                entry.AbsoluteExpirationRelativeToNow = TimeSpan.FromHours(hours);
                return client.Query<SfCustomUser>("SELECT Id, Codigo_do_Vendedor__c, Codigo_Vendedor_Area_Medica__c FROM User WHERE IsActive = True AND (Codigo_do_Vendedor__c != null OR Codigo_Vendedor_Area_Medica__c != null)").Result;
            });
        }
        public List<SfEspecialidade> GetEspecialidades(ForceClient client)
        {
            return cache.GetOrCreate("Especialidades", entry =>
            {
                entry.AbsoluteExpirationRelativeToNow = TimeSpan.FromHours(hours);
                return client.Query<SfEspecialidade>("SELECT Id, Name, Codigo__c FROM Especialidade__c").Result;
            });
        }
        public List<SfSegmento> GetSegmentos(ForceClient client)
        {
            return cache.GetOrCreate("Segmentos", entry =>
            {
                entry.AbsoluteExpirationRelativeToNow = TimeSpan.FromHours(hours);
                return client.Query<SfSegmento>("SELECT Id, Codigo__c FROM Segmento__c").Result;
            });
        }
        public List<SfCustomRecordType> GetTipoDeRegistro(ForceClient client)
        {
            return cache.GetOrCreate("RecordTypeAccount", entry =>
            {
                entry.AbsoluteExpirationRelativeToNow = TimeSpan.FromHours(hours);
                return client.Query<SfCustomRecordType>("SELECT IsPersonType, Id FROM RecordType WHERE SobjectType = 'Account'").Result;
            });
        }
        public List<SfCidade> GetCidades(ForceClient client)
        {
            return cache.GetOrCreate("Cidades", entry =>
            {
                entry.AbsoluteExpirationRelativeToNow = TimeSpan.FromHours(hours);
                return client.Query<SfCidade>("SELECT Id, Codigo__c FROM Cidade__c").Result;
            });
        }
        public List<SfTransportadora> GetTransportadoras(ForceClient client)
        {
            return cache.GetOrCreate("Transportadoras", entry =>
            {
                entry.AbsoluteExpirationRelativeToNow = TimeSpan.FromHours(hours);
                return client.Query<SfTransportadora>("SELECT Id, CardCode__c FROM Transportadora__c").Result;
            });
        }
        public List<SfProduto> GetProdutos(ForceClient client)
        {
            return client.Query<SfProduto>("SELECT Id, Codigo_do_Produto__c, Name FROM Produto__c", false).Result;
        }

        public string getEmail(string email)
        {
            if (!String.IsNullOrWhiteSpace(email))
            {
                string novoEmail = email.Replace(" ", "").Replace(",", "").Replace(";", "");

                if (IsValid(novoEmail))
                {
                    return novoEmail;
                }
            }

            return String.Empty;
        }

        public bool IsValid(string email)
        {
            bool isEmail = Regex.IsMatch(email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);

            return isEmail;
        }

        public string RemoveCharSpecial(string texto)
        {
            string textoSemChar = null;

            textoSemChar = Regex.Replace(texto, "[^0-9]", "");

            return textoSemChar;
        }

        public bool validateEmail(string email)
        {
            bool isEmail = Regex.IsMatch(email, @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase);

            return isEmail;
        }

        public decimal getIpi(string cardCode, string codProd, int codVend, decimal? vlrVenda)
        {
            GetIPI ipi = new GetIPI();

            using (var connection = new SqlConnection("Server=192.168.1.74\\ERP;User Id=salesforce;Password=@55Bg30#;Database=Salesforce;connect timeout=580;"))
            {
                var command = new SqlCommand();
                command.Connection = connection;
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "GetIPIInnovaBR";
                //command.CommandText = "GetIPI";
                command.Parameters.AddWithValue("@USG", codVend.ToString());
                command.Parameters.AddWithValue("@CARDCODE", cardCode);
                command.Parameters.AddWithValue("@ITEMCODE", codProd);

               if(command.Connection.State == ConnectionState.Closed) command.Connection.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        ipi.ValIPI = Convert.ToDecimal(reader["ValIPI"].ToString().Replace(".", ","));
                        ipi.BaseCalculo = Convert.ToDecimal(reader["BaseCalculo"].ToString().Replace(".", ","));
                    }
                }

            }

            decimal aliqBase = ipi.BaseCalculo / 100000000;
            decimal aliqIPI = ipi.ValIPI / 100000000;
            
            decimal result = vlrVenda??0;
            decimal vlrVendaUnit = vlrVenda??0;
            if(aliqIPI > 0){
                aliqIPI = Decimal.Round(aliqIPI + 1, 4);
                decimal vlrBase = Decimal.Round(vlrVendaUnit * aliqBase, 2);
                decimal vlrIPI = Decimal.Round(vlrBase / aliqIPI, 6);
                
                //result = Decimal.Round(vlrVendaUnit - vlrIPI, 6);
                result = vlrIPI;
            }

            return result;
        }

        public Frete calculaFrete(SfCustomOpportunity opp, bool possuiNabota, bool possuiOutroProdutos, bool possuiDesoxicolato, string estado, string codCidade, String CodigoTipoFrete)
        {
            Frete frete = new Frete();
            
            decimal valorFrete = 0;

            if(!opp.Frete_gratis__c && opp.Type != "Bonificação Saída" && opp.Type != "Doação" && !opp.Oportunidade_em_evento__c){
                
                if(possuiNabota)
                {
                    List<String> ufsFreteGratis = new List<String>{"SP", "RJ", "DF", "GO", "MG", "PR", "SC", "RS"};
                    List<String> codCidFrete1 = new List<String>{"16","68","104","192","198","193","161","432","578","537","677","662","674","630","722","881","1313","1257","2313","2413","2217","2435","2426","2485","2654","2584","2599","2730","2914","2893","2869","2819","2938","3181","1331","3768","3776","3884","3902","4707","4710","4751","5513"};
                    
                    if(ufsFreteGratis.Contains(estado) || opp.TotalOpportunityQuantity >= 10)
                    {
                        CodigoTipoFrete = "1";
                    }else
                    {
                        CodigoTipoFrete = "2";
                    }

                    if (CodigoTipoFrete == "1")
                    {
                        valorFrete += 0;
                    } 
                    else if(codCidFrete1.Contains(codCidade))
                    {
                        valorFrete += Convert.ToDecimal(118.90);
                    }
                    else
                    {
                        valorFrete += Convert.ToDecimal(198.90);
                    }
                }
                
                if(possuiDesoxicolato)
                {
                    List<String> ufsFretePago = new List<String>{"AC", "AM", "AP", "PA", "RO", "RR", "TO", "AL", "BA", "CE", "MA", "PI", "PE", "PB", "RN", "SE"};
                    
                    if((ufsFretePago.Contains(estado) && opp.Valor__c < 1000) || opp.TotalOpportunityQuantity < 2)
                    {
                        CodigoTipoFrete = "2";
                    }else
                    {
                        CodigoTipoFrete = "1";
                    }

                    if (CodigoTipoFrete == "1")
                    {
                        valorFrete += 0;
                    }
                    else
                    {
                        valorFrete += Convert.ToDecimal(80);
                    }

                } 
                
                if(possuiOutroProdutos)
                {
                    if (((estado == "DF" || estado == "GO") && opp.Valor__c < 500)
                    || ((estado != "DF" && estado != "GO") && opp.Valor__c < 1000))
                    {
                        CodigoTipoFrete = "2";
                    }
                    else
                    {
                        CodigoTipoFrete = "1";
                    }

                    if (CodigoTipoFrete == "1")
                    {
                        valorFrete += 0;
                    }
                    else
                    {
                        valorFrete += 80;
                    }
                }
            }else{
                valorFrete = 0;
            }

            frete.ValorFrete = valorFrete;
            frete.CodigoTipoFrete = CodigoTipoFrete;

            return frete;
        }

        // #### METODOS PARA CHAMADA DE API ####
        public async Task<List<Result>> UpsertDados(string registros, string objectName, string idExterno, string metodo)
        {
            List<String> idsExternos = new List<String>();
            List<Result> deserialize = new List<Result>();
            List<SfCustomIntegracao> integracoes = new List<SfCustomIntegracao>();

            try
            {
                var client = GetForceClient();

                HttpClient httpClient = new HttpClient
                {
                    Timeout = TimeSpan.FromMilliseconds(120000) 
                };
                
                var body = String.Format(@"{{""allOrNone"" : false, ""records"" : {0}}}", registros);

                dynamic jObj = JsonConvert.DeserializeObject(body);
                foreach (var item in jObj.records)
                {
                    if(objectName == "Opportunity"){
                        item.Property("Frete_gratis__c").Remove();
                        item.Property("Oportunidade_em_evento__c").Remove();
                        item.Property("VIP__c").Remove();
                    }
                }
                
                body = jObj.ToString();

                if(objectName == "Pagamento__c"){
                    body = body.Replace(",\r\n        \"Sincronizado__c\": false\r\n      ", null);
                    body = body.Replace(",\r\n        \"Sincronizado__c\": false,\r\n        \"VIP__c\": false\r\n      ", null);
                }

                var url = client.InstanceUrl + "/services/data/v50.0/composite/sobjects/" + objectName + "/" + idExterno;
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", client.AccessToken);
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Patch, url);
                request.Content = new StringContent(body, Encoding.UTF8, "application/json");
                var response = await httpClient.SendAsync(request);
                var result = await response.Content.ReadAsStringAsync();
                deserialize = NetCoreForce.Client.JsonSerializer.Deserialize<List<Result>>(result);
                foreach (var item in deserialize)
                {
                    if(!item.success){
                        string servico = metodo +"RetornoSF";
                        if(!idsExternos.Contains(servico))
                        {
                            string mensagemErro = item.errors[0].message;
                            if(!mensagemErro.StartsWith("Foreign key external ID")){
                                SfCustomIntegracao integracao = CriarErroIntegracao(servico, mensagemErro);
                                integracoes.Add(integracao);
                                idsExternos.Add(servico);
                            }
                        }
                    }
                }

                if(integracoes.Any() && !metodo.StartsWith("ERRO") && objectName != "Integracao__c"){
                    var integracaoJSON = JsonConvert.SerializeObject(integracoes, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                    var resultadoIntegracao = UpsertDados(integracaoJSON, "Integracao__c", "Id_Externo__c", "ERRO INTEGRACOES - " + metodo);
                }
            }
            catch(Exception ex)
            {
                var err = ex.Message;
            }
            return deserialize;
        }

        public async Task<string> DeletartDados(string ids)
        {
            string result = "";
            try
            {
                var client = GetForceClient();

                HttpClient httpClient = new HttpClient{ Timeout = TimeSpan.FromMilliseconds(120000) };
            
                var url = client.InstanceUrl + "/services/data/v50.0/composite/sobjects?ids=" + ids;
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", client.AccessToken);
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Delete, url);
                var response = await httpClient.SendAsync(request);
                result = await response.Content.ReadAsStringAsync();
            }
            catch(Exception ex)
            {
                var err = ex.Message;
            }
            return result;
        }

        public SfCustomIntegracao CriarErroIntegracao(string servico, string mensagemErro){
            SObjectAttributes atributoIntegracao = new SObjectAttributes();
            atributoIntegracao.Type = "Integracao__c";

            SfCustomIntegracao integracao = new SfCustomIntegracao();
            integracao.Metodo = servico;
            integracao.IdExterno = servico;
            integracao.Mensagem = mensagemErro;
            integracao.Attributes = atributoIntegracao;

            return integracao;
        }
    }
}
