using System;
using System.Linq;
using System.Data;
using Newtonsoft.Json;
using SfCustom.Models;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using NetCoreForce.Client.Models;
using InnovaPharmaMiddleware.Models;
using InnovaPharmaMiddleware.Helpers;
using innovaPharma_meddleware.Controllers;
using Microsoft.Extensions.Caching.Memory;
using InnovaPharmaMiddleware.Controllers;
using NetCoreForce.Client;

namespace InnovaPharmaMiddleware.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SapController : ControllerBase
    {
        private InnovaDbContext innovaDbContext;
        private ProdutoHelper produtoHelper;
        private IMemoryCache cache;

        public SapController(InnovaDbContext sc, ProdutoHelper _produtoHelper, IMemoryCache _cache)
        {
            cache = _cache;
            innovaDbContext = sc;
            produtoHelper = _produtoHelper;
        }

        public HandlerController controller()
        {
            HandlerController controller = new HandlerController(innovaDbContext, produtoHelper, cache);
            return controller;
        }

        //Método para inserir Novo Cliente no SAP
        [HttpGet("cliente/criar/{accountId?}")]
        public async Task CreateClienteSAPAsync(string accountId)
        {
            List<SfCustomIntegracao> integracoes = new List<SfCustomIntegracao>();

            try{
                var client =  controller().GetForceClient();

                ClienteNew cNew = innovaDbContext.ClienteNew.FirstOrDefault(c => c.CliSf == accountId);
                SfCustomAccount acc = await client.QuerySingle<SfCustomAccount>(SfCustomAccount.BuildSelect() + " WHERE Id = '" + accountId + "'", false);
                SfEndereco endEntrega = await client.QuerySingle<SfEndereco>(SfEndereco.BuildSelect() + " WHERE Tipo__c = 'Endereço de Entrega' AND Conta__c = '" + accountId + "' limit 1", false);
                SfCustomContact contact = await client.QuerySingle<SfCustomContact>(SfCustomContact.BuildSelect() + " WHERE AccountId = '" + accountId + "'  order by LastModifiedDate desc limit 1", false);
                Grupo_VDM gVDM = innovaDbContext.Grupo_VDM.FirstOrDefault(c => c.GroupName == acc.GrupoCliente);
                
                string Idcliente = null;
                
                if (cNew == null){ cNew = new ClienteNew(); } else { Idcliente = cNew.CliSf;}

                int codigoSegmento = acc.SegmentoCod != null ? Int32.Parse(acc.SegmentoCod) : 0;
                int codigoEspecialidade = acc.EspecialidadeCode != null ? Int32.Parse(acc.EspecialidadeCode) : 0;

                cNew.CliEmpId = 1;
                cNew.CliSf = acc.Id;
                cNew.REDE = acc.Rede;
                cNew.CliObsImp = null;
                cNew.CliStatusCad = 1;
                cNew.CliId = acc.IdInt;
                cNew.CliNome = acc.Name;
                cNew.CLI_VIP = acc.VIP;
                cNew.CliSaldo = acc.Saldo;
                cNew.CliVendId = acc.SlpCode;
                cNew.CliWebsite = acc.Website != null ? acc.Website.Length > 100 ? acc.Website.Substring(100) : acc.Website : acc.Website;
                cNew.CliTipoIe = acc.TipoIEInt;
                cNew.CliEmpOrg = "SBOEQUILIBRIUM";
                cNew.CliGrpId = gVDM.GroupCode;
                if(codigoSegmento != 0){cNew.CliSegId = codigoSegmento;}
                cNew.CliUsrId = acc.CreatedById;
                cNew.CliLimCred = acc.LimiteCredito;
                if(codigoEspecialidade != 0){cNew.CliRatvId = codigoEspecialidade;}
                cNew.CliDtaCad = System.DateTime.Now;
                cNew.CapitalSocial = acc.CapitalSocial;
                cNew.CliObsAdm = acc.ObservacoesGestor;
                cNew.CLI_CAP_SOCIAL = acc.CapitalSocial;
                cNew.CliObsVend = acc.ObservacoesVendedor;
                cNew.CliRefCom = acc.ReferenciasComerciais;
                cNew.CliRefBanc = acc.ReferenciasBancarias;
                cNew.CliEspecialidade = acc.EspecialidadeNome;
                cNew.CLI_SIT_IE = Convert.ToInt16(acc.SituacaoIE);
                cNew.CliDtaInc = acc.CreatedDate.Value.UtcDateTime;
                cNew.CLI_PORTE_EMP = Convert.ToInt16(acc.PorteEmpresa);
                cNew.CLI_PONT_SCORE = Convert.ToInt16(acc.PontosScore);
                cNew.CLI_IND_IE = Convert.ToInt16(acc.Indicador_da_IE__c);
                cNew.CLI_DESC_NAT = Convert.ToInt16(acc.DescricaoNatureza);
                cNew.CLI_IND_SN = Convert.ToInt16(acc.Simples_Nacional__c);
                cNew.CLI_CAT_CLIENTE = Convert.ToInt16(acc.CategoriaClientes);
                cNew.CLI_SIT_CNPJ = Convert.ToInt16(acc.SituacaoCadastralCnpj);
                cNew.CliObs = acc.Description + " " + acc.ReferenciasComerciais;
                cNew.CLI_IND_NAT = Convert.ToInt16(acc.Indicador_de_Natureza__c);
                cNew.CLI_IND_OPC = Convert.ToInt16(acc.Indicador_de_Op_Consumidor__c);
                cNew.Consultor_Tecnico = acc.ConsultorTecnico;
                cNew.CLI_NR_DISTRIBUIDOR = acc.Distribuidor == "Navarro" ? 1 : 0;

                if (acc.StatusSerasa != null)
                {
                    if (acc.StatusSerasa.Contains("Positivo")) { cNew.CLI_SERASA = 0; }
                    if (acc.StatusSerasa.Contains("Negativo")) { cNew.CLI_SERASA = 1; }
                }

                if(acc.DataAbertura.HasValue){cNew.CLI_DT_ABERTURA = acc.DataAbertura.Value.UtcDateTime;}
                if(acc.DataSerasa.HasValue){cNew.CLI_DTA_SERASA =  acc.DataSerasa.Value.UtcDateTime;}

                if (acc.TipoDeRegistroInt.Equals(2))
                {
                    cNew.CliSuframa = acc.Suframa;
                    cNew.CliFantasia = acc.Fantasia;
                    cNew.CliIe = acc.InscricaoEstadual;
                    cNew.CliIm = acc.InscricaoMunicipal;
                    cNew.CliTipo = acc.TipoDeRegistroInt;
                    cNew.CliSubsTrib = acc.SubstitudoTributario;
                    cNew.CliCnpj = Convert.ToUInt64(acc.CNPJCPF).ToString(@"00\.000\.000\/0000\-00");
                    if (!String.IsNullOrEmpty(acc.Phone)) cNew.CliFoneCom = controller().RemoveCharSpecial(acc.Phone);
                    if (contact != null)
                    {
                        cNew.CliEmail = contact.Email;
                        cNew.CliContato = contact.Name;
                        cNew.CliEmailXml = contact.Email;
                        if (!String.IsNullOrEmpty(contact.MobilePhone))
                            cNew.CliFoneCel = controller().RemoveCharSpecial(contact.MobilePhone);
                        if (!String.IsNullOrEmpty(contact.Phone))
                            cNew.CliFoneCom = controller().RemoveCharSpecial(contact.Phone);
                    }
                }
                else
                {
                    cNew.CliRgNr = acc.RG;
                    cNew.CliRgUf = acc.UfRg;
                    cNew.CliRgOrgao = acc.OrgExp;
                    cNew.CliTipo = acc.TipoDeRegistroInt;
                    cNew.CliRegCons = acc.NRegistroConselho;
                    if(acc.DataNascimento.HasValue){cNew.CliDtaNasc = acc.DataNascimento.Value.UtcDateTime;}
                    cNew.CliCpf = Convert.ToInt64(acc.CNPJCPF).ToString(@"000\.000\.000\-00");
                    if (!String.IsNullOrEmpty(acc.CelularPessoal)) cNew.CliFoneCel = controller().RemoveCharSpecial(acc.CelularPessoal);
                    cNew.CliEmail = acc.Email;
                    if (!String.IsNullOrEmpty(acc.Phone)) cNew.CliFoneCom = controller().RemoveCharSpecial(acc.Phone);
                    cNew.CliEmailXml = acc.Email;
                }
                if (endEntrega != null)
                {
                    int codigoCidade = Int32.Parse(endEntrega.CodigoCidade);

                    cNew.CliMunId = codigoCidade;
                    cNew.CliMunIdC = codigoCidade;
                    cNew.CliUf = endEntrega.Estado;
                    cNew.CliNr = endEntrega.Numero;
                    cNew.CliUfC = endEntrega.Estado;
                    cNew.CliNrC = endEntrega.Numero;
                    cNew.CliBairro = endEntrega.Bairro;
                    cNew.CliCidade = endEntrega.Cidade;
                    cNew.CliBairroC = endEntrega.Bairro;
                    cNew.CliCidadeC = endEntrega.Cidade;
                    cNew.CliLogradouro = endEntrega.Rua;
                    cNew.CliLogradouroC = endEntrega.Rua;
                    cNew.CliCompl = endEntrega.Complemento;
                    cNew.CliComplC = endEntrega.Complemento;
                    cNew.CliCep = controller().RemoveCharSpecial(endEntrega.CEP);
                    cNew.CliCepC = controller().RemoveCharSpecial(endEntrega.CEP);
                    //Teste
                }
                try
                {
                    if (Idcliente == null)
                    {
                        await innovaDbContext.AddAsync(cNew);
                    }
                    else
                    {
                        if(cNew.CliIdOrg == null || cNew.CliIdOrg == "")
                        {
                            innovaDbContext.Update(cNew);
                        }
                    }
                }
                catch (Exception ex)
                {
                    string servico = "insertClienteSAP // " + acc.Id;
                    var message = ex.InnerException != null ? ex.InnerException.Message : null;
                    string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                    SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                    integracoes.Add(integracao);
                }

                await innovaDbContext.SaveChangesAsync();
            }catch(Exception ex)
            {
                string servico = "insertClienteSAP";
                var message = ex.InnerException != null ? ex.InnerException.Message : null;
                string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                integracoes.Add(integracao);
            }
            
            if(integracoes.Any()){
                var integracaoJSON = JsonConvert.SerializeObject(integracoes, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultadoIntegracao = controller().UpsertDados(integracaoJSON, "Integracao__c", "Id_Externo__c", "insertClienteSAP");
            }
        }

        //Método para atualizar Cliente no SAP
        [HttpGet("cliente/atualizar/{accountId?}")]
        public async Task UpdateClienteSAPAsync(string accountId)
        {
            List<SfCustomIntegracao> integracoes = new List<SfCustomIntegracao>();

            try{
                var client = controller().GetForceClient();

                var select = SfCustomContact.BuildSelect();
                
                SfCustomAccount acc = await client.QuerySingle<SfCustomAccount>(SfCustomAccount.BuildSelect() + " WHERE Id = '" + accountId + "'", false);
                SfEndereco endEntrega = await client.QuerySingle<SfEndereco>(SfEndereco.BuildSelect() + " WHERE Tipo__c = 'Endereço de Entrega' AND Conta__c = '" + accountId + "' limit 1", false);
                SfCustomContact contact = await client.QuerySingle<SfCustomContact>(SfCustomContact.BuildSelect() + " WHERE AccountId = '" + accountId + "'  order by LastModifiedDate desc limit 1", false);
                Grupo_VDM gVDM = innovaDbContext.Grupo_VDM.FirstOrDefault(c => c.GroupName == acc.GrupoCliente);

                Credit credit = innovaDbContext.Credit.FirstOrDefault(c => c.CardCode == acc.CardCode && c.Data >= System.DateTime.Now.AddSeconds(-10));

                if (credit != null){return;}

                try
                {
                    int codigoSegmento = acc.SegmentoCod != null ? Int32.Parse(acc.SegmentoCod) : 0;
                    
                    Credit cNew = new Credit();

                    cNew.StatusCad = 1;
                    cNew.REDE = acc.Rede;
                    cNew.RazaoSocial = acc.Name;
                    cNew.TipoIE = acc.TipoIEInt;
                    cNew.Empresa = "SBOEquilibrium";
                    cNew.CardCode = acc.CardCode;
                    cNew.CLI_VIP = acc.VIP;
                    cNew.CodVendedor = acc.SlpCode;
                    cNew.Data = System.DateTime.Now;
                    cNew.FreeText = acc.Description;
                    cNew.IE = acc.InscricaoEstadual;
                    cNew.DebtLine = acc.LimiteCredito;
                    cNew.GrupoCliente = gVDM.GroupCode;
                    cNew.CreditLine = acc.LimiteCredito;
                    cNew.ObsSerasa = acc.ObservacoesSerasa != null && acc.ObservacoesSerasa.Length > 50 ? acc.ObservacoesSerasa.Substring(0, 50) : acc.ObservacoesSerasa;
                    if(codigoSegmento != 0){cNew.Segmento = codigoSegmento;}
                    cNew.CLI_CAP_SOCIAL = acc.CapitalSocial;
                    cNew.CLI_SIT_IE = Convert.ToInt16(acc.SituacaoIE);
                    cNew.CLI_PORTE_EMP = Convert.ToInt16(acc.PorteEmpresa);
                    cNew.CLI_PONT_SCORE = Convert.ToInt32(acc.PontosScore);
                    cNew.CLI_IND_IE = Convert.ToInt16(acc.Indicador_da_IE__c);
                    cNew.CLI_IND_SN = Convert.ToInt16(acc.Simples_Nacional__c);
                    cNew.CLI_DESC_NAT = Convert.ToInt16(acc.DescricaoNatureza);
                    cNew.CLI_CAT_CLIENTE = Convert.ToInt16(acc.CategoriaClientes);
                    cNew.CLI_SIT_CNPJ = Convert.ToInt16(acc.SituacaoCadastralCnpj);
                    cNew.CLI_IND_NAT = Convert.ToInt16(acc.Indicador_de_Natureza__c);
                    cNew.CLI_IND_OPC = Convert.ToInt16(acc.Indicador_de_Op_Consumidor__c);
                    if(acc.DataAbertura.HasValue){cNew.CLI_DT_ABERTURA = acc.DataAbertura.Value.UtcDateTime;}
                    if (acc.LimiteCredito > 0) { cNew.AjustarLimiteCredito = true; } else { cNew.AjustarLimiteCredito = false; }
                    if (acc.Description != null) { cNew.AjustarObservacaoCliente = true; } else { cNew.AjustarObservacaoCliente = false; }        
                    cNew.CLI_NR_DISTRIBUIDOR = acc.Distribuidor == "Navarro" ? 1 : 0;

                    if (acc.DataSerasa.HasValue) { cNew.DataSerasa = acc.DataSerasa.Value.UtcDateTime; }

                    if (acc.StatusSerasa != null)
                    {
                        if (acc.StatusSerasa.Contains("Positivo")) { cNew.TipoSerasa = 0; }
                        if (acc.StatusSerasa.Contains("Negativo")) { cNew.TipoSerasa = 1; }
                    }

                    if (acc.TipoDeRegistroInt.Equals(2))
                    {
                        cNew.NomeFantasia = acc.Fantasia;
                        if (contact != null)
                        {
                            cNew.Email = contact.Email;
                            cNew.Celular = contact.MobilePhone != null ? controller().RemoveCharSpecial(contact.MobilePhone) : contact.MobilePhone;
                            cNew.Telefone = contact.Phone != null ? controller().RemoveCharSpecial(contact.Phone) : contact.Phone;
                        }
                    }
                    else
                    {
                        cNew.Email = acc.Email;
                        cNew.NomeFantasia = acc.Name;
                        cNew.Telefone = acc.Phone != null ? controller().RemoveCharSpecial(acc.Phone) : acc.Phone;
                        cNew.Celular = acc.CelularPessoal != null ? controller().RemoveCharSpecial(acc.CelularPessoal) : acc.CelularPessoal;
                    }

                    if (endEntrega != null)
                    {
                        int codigoCidade = Int32.Parse(endEntrega.CodigoCidade);

                        cNew.IdCidade = codigoCidade;
                        cNew.Endereco = endEntrega.Rua;
                        cNew.Numero = endEntrega.Numero;
                        cNew.Cidade = endEntrega.Cidade;
                        cNew.Bairro = endEntrega.Bairro;
                        cNew.Estado = endEntrega.Estado;
                        cNew.Complemento = endEntrega.Complemento;
                        cNew.CEP = controller().RemoveCharSpecial(endEntrega.CEP);
                    }

                    cNew.Consultor_Tecnico = acc.ConsultorTecnico;

                    await innovaDbContext.AddAsync(cNew);

                    await innovaDbContext.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    string servico = "atualizaClienteSAP // " + acc.Id;
                    var message = ex.InnerException != null ? ex.InnerException.Message : null;
                    string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                    SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                    integracoes.Add(integracao);
                }
            }catch(Exception ex)
            {
                string servico = "atualizaClienteSAP";
                var message = ex.InnerException != null ? ex.InnerException.Message : null;
                string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                integracoes.Add(integracao);
            }
            
            if(integracoes.Any()){
                var integracaoJSON = JsonConvert.SerializeObject(integracoes, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultadoIntegracao = controller().UpsertDados(integracaoJSON, "Integracao__c", "Id_Externo__c", "atualizaClienteSAP");
            }
        }

        //Método para criar Pedido no SAP
        [HttpGet("pedido/criar/{opportunityId?}")]
        public async Task CreatePedidoAsync(string opportunityId)
        {
            List<SfCustomIntegracao> integracoes = new List<SfCustomIntegracao>();

            try{
                var client = controller().GetForceClient();
                //List<string> idDocumentos = new List<string>();
                SfCustomOpportunity opp = await client.QuerySingle<SfCustomOpportunity>(SfCustomOpportunity.BuildSelect() + " WHERE Id = '" + opportunityId + "'", false);
                
                SfEndereco endEntrega = await client.QuerySingle<SfEndereco>(SfEndereco.BuildSelect() + " WHERE Tipo__c = 'Endereço de Entrega' AND Conta__c = '" + opp.AccountId + "' limit 1", false);
                List<SfCustomOpportunityLineItem> produtosOpp = await client.Query<SfCustomOpportunityLineItem>(SfCustomOpportunityLineItem.BuildSelect() + " WHERE OpportunityId = '" + opportunityId + "'");
                //List<SfContentDocumentLink> documentosdaOpp = await client.Query<SfContentDocumentLink>("SELECT Id, ContentDocumentId, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId = '" + opportunityId + "'");
                CondicoesPagamento_VDM condPag;

                //foreach (var documento in documentosdaOpp) { idDocumentos.Add(documento.ContentDocumentId); }

                //List<SfContentNote> notasOpp = await client.Query<SfContentNote>("SELECT Id, Title, TextPreview FROM ContentNote WHERE Id in ('" + String.Join("','", idDocumentos) + "')");

                var pedidoSAP = innovaDbContext.Pedido.Where(pedido => pedido.IdPedido == opp.Codigo).FirstOrDefault();
                var itensSAP = innovaDbContext.PedidoItens.Where(item => item.PedidoId == opp.Codigo).FirstOrDefault();

                Boolean inserirItens = itensSAP == null;

                if (opp.FormaDePagamento != null) 
                { condPag = innovaDbContext.CondicoesPagamento_VDM.Where(cPag => cPag.Descricao == opp.FormaDePagamento).FirstOrDefault(); }
                else { condPag = innovaDbContext.CondicoesPagamento_VDM.Where(cPag => cPag.Descricao == "00").FirstOrDefault(); }

                var nabota = produtosOpp.FirstOrDefault(p => p.ProductCode == "1162957");
                var desoxicolato = produtosOpp.FirstOrDefault(p => p.ProductCode == "0328910");
                var outroProduto = produtosOpp.FirstOrDefault(p => p.ProductCode != "1162957");

                bool possuiDesoxicolato = desoxicolato != null;
                bool possuiOutrosProdutos = outroProduto != null;
                bool possuiNabota = !possuiOutrosProdutos && nabota != null;
                
                string empresa = opp.Empresa;

                try{
                    if(pedidoSAP == null){

                        Pedido pNew = new Pedido();

                        pNew.Origem = "4";
                        pNew.Status = null;
                        pNew.PcDesconto = 0;
                        pNew.IdPedidoSap = "F";
                        pNew.PcDescontoFin = 0;
                        pNew.IdPedido = opp.Codigo;
                        pNew.CodigoPlanoNegocio = "1";
                        pNew.CodigoTransportadora = "0";
                        pNew.Indicador = opp.VIP ? "VIP" : empresa == "Navarro" ? "N" : null;
                        pNew.DataEnvio = System.DateTime.Now;
                        pNew.CodigoCliente = opp.ContaCardCode;
                        pNew.ObservacaoNf = opp.ObservacesDaNF;
                        pNew.CodigoEmitente = opp.CodigoEmitente;
                        var CodigoVendedor = opp.CodVendedor.Split(".");
                        pNew.CodigoVendedor = CodigoVendedor[0];
                        pNew.CodAutorizacao = opp.Codigo_de_Autorizacao__c;
                        pNew.DataPedido = opp.CreatedDate.Value.UtcDateTime;
                        pNew.CodigoTipoPagamento = condPag.Codigo.ToString();
                        if(opp.CodigoTransportadora != null) pNew.CodigoTransportadora = opp.CodigoTransportadora;
                        pNew.Empresa = empresa;
                        pNew.Id_Salesforce = opp.Id;
                        pNew.Codigo_TipoEntrega = opp.Oportunidade_em_evento__c ? "2" : "1";
                        pNew.Bandeira_Cartao1 = opp.Bandeira__c;
                        pNew.Bandeira_Cartao2 = opp.Bandeira_2__c;
                        if(opp.Numero_de_Parcelas__c != null){
                            pNew.N_Parcela_Cartao1 = Convert.ToInt32(opp.Numero_de_Parcelas__c);
                        }
                        if(opp.Numero_de_Parcelas_2__c != null){
                            pNew.N_Parcela_Cartao2 = Convert.ToInt32(opp.Numero_de_Parcelas_2__c);
                        }
                        pNew.CodAutorizacao_Cartao1 = opp.NSU_1__c;
                        pNew.CodAutorizacao_Cartao2 = opp.NSU_2__c;
                        pNew.Valor_Cartao1 = opp.Valor_cartao_1__c;
                        pNew.Valor_Cartao2 = opp.Valor_cartao_2__c;
                        pNew.Cred_ou_Deb = opp.Condicao == "Cartão" ? "Crédito" : opp.Condicao == "Cartão de débito" ? "Débito" : "";

                        /*string ObservacesDoPedido = "";
                        Inativado por falta de tamanho no banco 
                        int contador = 1;
                        foreach (var nota in notasOpp){ObservacesDoPedido += contador + "ª Nota: " + nota.Title + " \r\nDescrição: " + nota.TextPreview + " \r\n// ";contador++;}*/
                        //1ª Nota: NÃO SEPARAR Descrição: NÃO DEVE SER FEITA A SEPARAÇÃO DO PRODUTO! 

                        pNew.ObservacaoPedido = opp.Description;

                        Frete frete = controller().calculaFrete(opp, possuiNabota, possuiOutrosProdutos, possuiDesoxicolato, endEntrega.Estado ,endEntrega.CodigoCidade, "1");

                        pNew.ValorFrete = frete.ValorFrete;
                        pNew.CodigoTipoFrete = frete.CodigoTipoFrete;

                        if (opp.Condicao.Contains("Boleto"))
                        {
                            pNew.CodigoFormaPagamento = "BOLETO";
                        }
                        else if (opp.Condicao.Contains("Cartão"))
                        {
                            pNew.CodigoFormaPagamento = "CARTAO";
                        }
                        else if (opp.Condicao.Contains("Depósito Bancário"))
                        {
                            pNew.CodigoFormaPagamento = "DEPOSITO";
                        }
                        else if (opp.Condicao.Contains("PIX"))
                        {
                            pNew.CodigoFormaPagamento = "PIX";
                        }
                        else if (opp.Condicao.Contains("Carteira") || condPag.Codigo.ToString() == "53")
                        {
                            pNew.CodigoFormaPagamento = "CARTEIRA";
                        }

                        pNew.FormaPagamento = opp.Condicao;

                        await innovaDbContext.AddAsync(pNew);
                        await innovaDbContext.SaveChangesAsync();
                    }
                }
                catch (Exception ex)
                {
                    string servico = "criarPedidoSAP // " + opp.Id;
                    var message = ex.InnerException != null ? ex.InnerException.Message : null;
                    string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                    if(message == null || !message.StartsWith("Violation of PRIMARY KEY constraint")){
                        SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                        integracoes.Add(integracao);
                    }
                    inserirItens = false;
                }

                if(inserirItens){
                    foreach (var prodOpp in produtosOpp)
                    {
                        if(possuiOutrosProdutos && prodOpp.CodigoProduto == "1162957"){
                            continue;
                        }

                        try{
                            decimal valorVenda = prodOpp.UnitPrice??0;

                            //if(prodOpp.Empresa == "SBOInnovaBR")
                            //if(prodOpp.CodigoProduto == "0064030" || prodOpp.CodigoProduto == "0064014")
                            //{
                                int tipoNegociacao = 1;

                                if (opp.Type == "Bonificação Saída") { tipoNegociacao = 9;}
                                else if (opp.Type == "Doação"){tipoNegociacao = 13;}
                                else if (opp.Type == "Produtos promocionais"){tipoNegociacao = 42;}
                                else if (opp.Type == "Feiras, Cong e Eventos"){tipoNegociacao = 59;}
                                else if (opp.Type == "Consumo Interno"){tipoNegociacao = 65;}
                                else if (opp.Type == "Venda (Desc. Incond.)"){tipoNegociacao = 68;}

                                valorVenda = controller().getIpi(opp.ContaCardCode, prodOpp.ProductCode, tipoNegociacao, prodOpp.UnitPrice);
                            //}

                            PedidoItens produtoNew = new PedidoItens();

                            produtoNew.Desconto = 0;
                            produtoNew.PcDesconto = 0;
                            produtoNew.ItemSequencia = 1;
                            produtoNew.PedidoId = opp.Codigo;
                            produtoNew.IdSalesforce = prodOpp.Id;
                            produtoNew.Quantidade = Int32.Parse(prodOpp.Quantity.ToString());
                            produtoNew.ItemCodigo = prodOpp.CodigoProduto;
                            produtoNew.QuantidadeVenda = Int32.Parse(prodOpp.Quantity.ToString());
                            produtoNew.ValorVenda = valorVenda > 0 ? valorVenda : prodOpp.UnitPrice;
                            produtoNew.Empresa = empresa != null ? empresa : "SBOEquilibrium";
                            produtoNew.TotalTabela = prodOpp.ValorCombo;

                            if (opp.Type == "Feiras, Cong e Eventos")
                            {
                                produtoNew.TipoUtilizacao = "59";
                            }
                            else if (opp.Type == "Bonificação Saída")
                            {
                                produtoNew.TipoUtilizacao = "9";
                            }
                            else if (opp.Type == "Doação")
                            {
                                produtoNew.TipoUtilizacao = "13";
                            }
                            else if (opp.Type == "Produtos promocionais")
                            {
                                produtoNew.TipoUtilizacao ="31";
                            }
                            else if (opp.Type == "Consumo Interno")
                            {
                                produtoNew.TipoUtilizacao ="65";
                            }
                            else if (opp.Type == "Venda (Desc. Incond.)")
                            {
                                produtoNew.TipoUtilizacao ="68";
                            }
                            else
                            {
                                produtoNew.TipoUtilizacao = "1";
                            }

                            await innovaDbContext.AddAsync(produtoNew);
                        }
                        catch (Exception ex)
                        {
                            string servico = "criarItemPedidoSAP // " + prodOpp.Id;
                            var message = ex.InnerException != null ? ex.InnerException.Message : null;
                            string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                            SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                            integracoes.Add(integracao);
                        }
                            
                    }
                    await innovaDbContext.SaveChangesAsync();
                }
            }
            catch(Exception ex)
            {
                string servico = "criarPedidoSAP";
                var message = ex.InnerException != null ? ex.InnerException.Message : null;
                string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                integracoes.Add(integracao);
            }
            
            if(integracoes.Any()){
                var integracaoJSON = JsonConvert.SerializeObject(integracoes, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultadoIntegracao = controller().UpsertDados(integracaoJSON, "Integracao__c", "Id_Externo__c", "criarPedidoSAP");
            }
        }

        [HttpGet("pedidomundipagg/criar/{opportunityId?}")]
        public async Task CreatePedidoMundiPaggAsync(string opportunityId)
        {
            List<SfCustomIntegracao> integracoes = new List<SfCustomIntegracao>();

            try{
                var client = controller().GetForceClient();
                SfCustomOpportunity opp = await client.QuerySingle<SfCustomOpportunity>(SfCustomOpportunity.BuildSelect() + " WHERE Id = '" + opportunityId + "'", false);
                PedidoMundiPagg pedido = innovaDbContext.PedidoMundiPagg.FirstOrDefault(c => c.IdSalesForce == opportunityId);
                if(pedido != null){return;}
                SfEndereco endEntrega = await client.QuerySingle<SfEndereco>(SfEndereco.BuildSelect() + " WHERE Tipo__c = 'Endereço de Entrega' AND Conta__c = '" + opp.AccountId + "' limit 1", false);
                List<SfCustomOpportunityLineItem> produtosOpp = await client.Query<SfCustomOpportunityLineItem>(SfCustomOpportunityLineItem.BuildSelect() + " WHERE OpportunityId = '" + opportunityId + "'");

                try{
                    PedidoMundiPagg pNew = new PedidoMundiPagg();

                    pNew.Empresa = produtosOpp.FirstOrDefault().Empresa != null ? produtosOpp.FirstOrDefault().Empresa : "SBOEquilibrium";
                    pNew.IdPedido = opp.Codigo;
                    pNew.Cancelado = 0;
                    pNew.StatusPedido = 1;
                    pNew.IdSalesForce = opp.Id;
                    pNew.ValorTotal = opp.Valor__c;
                    pNew.Email = opp.Email_do_Contato__c;
                    pNew.DataEnvio = System.DateTime.Now;
                    pNew.Codigo_Cliente = opp.ContaCardCode;
                    pNew.Parcelas = Convert.ToInt16(opp.Numero_de_Parcelas__c);
                    pNew.QuantidadeCartoes = Convert.ToInt16(opp.Quantidade_de_cartoes__c) == 0 ? 1 : Convert.ToInt16(opp.Quantidade_de_cartoes__c);
                    
                    var nabota = produtosOpp.FirstOrDefault(p => p.ProductCode == "1162957");
                    var desoxicolato = produtosOpp.FirstOrDefault(p => p.ProductCode == "0328910");
                    var outroProduto = produtosOpp.FirstOrDefault(p => p.ProductCode != "1162957");

                    bool possuiNabota = nabota != null;;
                    bool possuiDesoxicolato = desoxicolato != null;
                    bool possuiOutrosProdutos = outroProduto != null;

                    Frete frete = controller().calculaFrete(opp, possuiNabota, possuiOutrosProdutos, possuiDesoxicolato, endEntrega.Estado ,endEntrega.CodigoCidade, "1");

                    pNew.ValorFrete = frete.ValorFrete;

                    await innovaDbContext.AddAsync(pNew);

                    foreach (var prodOpp in produtosOpp)
                    {
                        try{
                            PedidoMundiPaggItens produtoNew = new PedidoMundiPaggItens();

                            int tipoUtilizacao = 1;

                            if (opp.Type == "Bonificação Saída") { tipoUtilizacao = 9;}
                            else if (opp.Type == "Doação"){tipoUtilizacao = 13;}
                            else if (opp.Type == "Produtos promocionais"){tipoUtilizacao = 42;}
                            else if (opp.Type == "Feiras, Cong e Eventos"){tipoUtilizacao = 59;}
                            else if (opp.Type == "Consumo Interno"){tipoUtilizacao = 65;}
                            else if (opp.Type == "Venda (Desc. Incond.)"){tipoUtilizacao = 68;}
                            
                            Decimal valorVenda = controller().getIpi(opp.ContaCardCode, prodOpp.ProductCode, tipoUtilizacao, prodOpp.UnitPrice);

                            produtoNew.IdPedido = opp.Codigo;
                            produtoNew.IdSalesForce = prodOpp.Id;
                            produtoNew.Preco = valorVenda > 0 ? valorVenda : prodOpp.UnitPrice;
                            produtoNew.Quantidade = prodOpp.Quantity;
                            produtoNew.ItemCodigo = prodOpp.CodigoProduto;
                            produtoNew.Empresa = prodOpp.Empresa != null ? prodOpp.Empresa : "SBOEquilibrium";
                            produtoNew.TotalTabela = prodOpp.ValorCombo;

                            await innovaDbContext.AddAsync(produtoNew);
                        }
                        catch (Exception ex)
                        {
                            string servico = "criarItemPedidoMundiPaggSAP // " + prodOpp.Id;
                            var message = ex.InnerException != null ? ex.InnerException.Message : null;
                            string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                            SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                            integracoes.Add(integracao);
                        }
                    }

                    await innovaDbContext.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    string servico = "criarPedidoMundiPaggSAP // " + opp.Id;
                    var message = ex.InnerException != null ? ex.InnerException.Message : null;
                    if(!message.StartsWith("Violation of PRIMARY KEY constraint")){
                        string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                        SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                        integracoes.Add(integracao);
                    }
                }
            }
            catch(Exception ex)
            {
                string servico = "criarPedidoMundiPaggSAP";
                var message = ex.InnerException != null ? ex.InnerException.Message : null;
                if(!message.StartsWith("Violation of PRIMARY KEY constraint")){
                    string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                    SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                    integracoes.Add(integracao);
                }
            }
            
            if(integracoes.Any()){
                var integracaoJSON = JsonConvert.SerializeObject(integracoes, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultadoIntegracao = controller().UpsertDados(integracaoJSON, "Integracao__c", "Id_Externo__c", "criarItemPedidoMundiPaggSAP");
            }
        }

        [HttpGet("pedidopix/criar/{opportunityId?}")]
        public async Task CreatePedidoPIXAsync(string opportunityId)
        {
            List<SfCustomIntegracao> integracoes = new List<SfCustomIntegracao>();

            try{
                var client = controller().GetForceClient();
                SfCustomOpportunity opp = await client.QuerySingle<SfCustomOpportunity>(SfCustomOpportunity.BuildSelect() + " WHERE Id = '" + opportunityId + "'", false);
                PedidoBankPlus pedido = innovaDbContext.PedidoBankPlus.FirstOrDefault(c => c.NumeroPedido == opp.Codigo);
                if(pedido != null){return;}
                SfEndereco endEntrega = await client.QuerySingle<SfEndereco>(SfEndereco.BuildSelect() + " WHERE Tipo__c = 'Endereço de Entrega' AND Conta__c = '" + opp.AccountId + "' limit 1", false);
                List<SfCustomOpportunityLineItem> produtosOpp = await client.Query<SfCustomOpportunityLineItem>(SfCustomOpportunityLineItem.BuildSelect() + " WHERE OpportunityId = '" + opportunityId + "'");
                
                try{
                    PedidoBankPlus pNew = new PedidoBankPlus();

                    pNew.DBEmpresa = produtosOpp.FirstOrDefault().Empresa != null ? produtosOpp.FirstOrDefault().Empresa : "SBOEquilibrium";
                    pNew.NumeroPedido = opp.Codigo;
                    pNew.IDStatus = 1;
                    pNew.CodigoCliente = opp.ContaCardCode;
                    pNew.Email = opp.Email_do_Contato__c;
                    pNew.CodigoTitular = Int32.Parse(opp.CodigoEmitente);
                    pNew.CodigoVendedor = Int32.Parse(opp.CodVendedor.Split(".")[0]);
                    pNew.DataEnvio = System.DateTime.Now;
                    pNew.DataPedido = System.DateTime.Now;
                    pNew.Observacao = opp.ObservacesDoPedido;
                    pNew.Descricao = opp.Description;
                    pNew.PercentualDesconto = 0;

                    Decimal valorTotal = 0;

                    foreach (var prodOpp in produtosOpp)
                    {
                        PedidoItemBankPlus produtoNew = new PedidoItemBankPlus();

                        int tipoUtilizacao = 1;

                        if (opp.Type == "Bonificação Saída") { tipoUtilizacao = 9;}
                        else if (opp.Type == "Doação"){tipoUtilizacao = 13;}
                        else if (opp.Type == "Produtos promocionais"){tipoUtilizacao = 42;}
                        else if (opp.Type == "Feiras, Cong e Eventos"){tipoUtilizacao = 59;}
                        else if (opp.Type == "Consumo Interno"){tipoUtilizacao = 65;}
                        else if (opp.Type == "Venda (Desc. Incond.)"){tipoUtilizacao = 68;}
                        
                        decimal valorVenda = controller().getIpi(opp.ContaCardCode, prodOpp.ProductCode, tipoUtilizacao, prodOpp.UnitPrice);
                        valorVenda = valorVenda * Convert.ToInt32(prodOpp.Quantity);

                        valorTotal += valorVenda;
                    }
                    
                    var nabota = produtosOpp.FirstOrDefault(p => p.ProductCode == "1162957");
                    var desoxicolato = produtosOpp.FirstOrDefault(p => p.ProductCode == "0328910");
                    var outroProduto = produtosOpp.FirstOrDefault(p => p.ProductCode != "1162957");

                    bool possuiNabota = nabota != null;;
                    bool possuiDesoxicolato = desoxicolato != null;
                    bool possuiOutrosProdutos = outroProduto != null;

                    Frete frete = controller().calculaFrete(opp, possuiNabota, possuiOutrosProdutos, possuiDesoxicolato, endEntrega.Estado ,endEntrega.CodigoCidade, "1");

                    pNew.ValorTotalFrete = frete.ValorFrete;
                    pNew.ValorTotal = valorTotal + frete.ValorFrete;
                    
                    await innovaDbContext.AddAsync(pNew);

                    foreach (var prodOpp in produtosOpp)
                    {
                        try{
                            PedidoItemBankPlus produtoNew = new PedidoItemBankPlus();

                             int tipoUtilizacao = 1;

                            if (opp.Type == "Bonificação Saída") { tipoUtilizacao = 9;}
                            else if (opp.Type == "Doação"){tipoUtilizacao = 13;}
                            else if (opp.Type == "Produtos promocionais"){tipoUtilizacao = 42;}
                            else if (opp.Type == "Feiras, Cong e Eventos"){tipoUtilizacao = 59;}
                            else if (opp.Type == "Consumo Interno"){tipoUtilizacao = 65;}
                            else if (opp.Type == "Venda (Desc. Incond.)"){tipoUtilizacao = 68;}
                            
                            Decimal valorVenda = controller().getIpi(opp.ContaCardCode, prodOpp.ProductCode, tipoUtilizacao, prodOpp.UnitPrice);

                            produtoNew.IdSalesForce = prodOpp.Id;
                            produtoNew.NumeroPedido = opp.Codigo;
                            produtoNew.CodigoItem = prodOpp.CodigoProduto;
                            produtoNew.PrecoUnitario = valorVenda > 0 ? valorVenda : prodOpp.UnitPrice;
                            produtoNew.Quantidade = prodOpp.Quantity;
                            produtoNew.DBEmpresa = prodOpp.Empresa != null ? prodOpp.Empresa : "SBOEquilibrium";
                            produtoNew.PercentualDesconto = 0;
                            produtoNew.CodigoUtilizacao = tipoUtilizacao;
                            produtoNew.TotalTabela = prodOpp.ValorCombo;

                            await innovaDbContext.AddAsync(produtoNew);
                        }
                        catch (Exception ex)
                        {
                            string servico = "criarItemPedidoPIX // " + prodOpp.Id;
                            var message = ex.InnerException != null ? ex.InnerException.Message : null;
                            string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                            SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                            integracoes.Add(integracao);
                        }
                    }

                    await innovaDbContext.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    string servico = "criarPedidoPIX // " + opp.Id;
                    var message = ex.InnerException != null ? ex.InnerException.Message : null;
                    if(!message.StartsWith("Violation of PRIMARY KEY constraint")){
                        string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                        SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                        integracoes.Add(integracao);
                    }
                }
            }
            catch(Exception ex)
            {
                string servico = "criarPedidoPIX";
                var message = ex.InnerException != null ? ex.InnerException.Message : null;
                if(!message.StartsWith("Violation of PRIMARY KEY constraint")){
                    string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                    SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                    integracoes.Add(integracao);
                }
            }
            
            if(integracoes.Any()){
                var integracaoJSON = JsonConvert.SerializeObject(integracoes, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultadoIntegracao = controller().UpsertDados(integracaoJSON, "Integracao__c", "Id_Externo__c", "criarItemPedidoMundiPaggSAP");
            }
        }

        [HttpGet("pedidopix/renovarqrcode/{opportunityId?}")]
        public async Task renovarQrCode(string opportunityId)
        {
            List<SfCustomIntegracao> integracoes = new List<SfCustomIntegracao>();

            try{
                var client = controller().GetForceClient();
                SfCustomOpportunity opp = await client.QuerySingle<SfCustomOpportunity>(SfCustomOpportunity.BuildSelect() + " WHERE Id = '" + opportunityId + "'", false);
                
                try{
                    PedidoBankPlus pNew = new PedidoBankPlus();

                    pNew.NumeroPedido = opp.Codigo;
                    pNew.IDStatus = 5;
                    
                    innovaDbContext.Update(pNew);

                }
                catch (Exception ex)
                {
                    string servico = "renovarQrCode // " + opp.Id;
                    var message = ex.InnerException != null ? ex.InnerException.Message : null;
                    if(!message.StartsWith("Violation of PRIMARY KEY constraint")){
                        string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                        SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                        integracoes.Add(integracao);
                    }
                }
            }
            catch(Exception ex)
            {
                string servico = "renovarQrCode";
                var message = ex.InnerException != null ? ex.InnerException.Message : null;
                if(!message.StartsWith("Violation of PRIMARY KEY constraint")){
                    string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                    SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                    integracoes.Add(integracao);
                }
            }
            
            if(integracoes.Any()){
                var integracaoJSON = JsonConvert.SerializeObject(integracoes, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultadoIntegracao = controller().UpsertDados(integracaoJSON, "Integracao__c", "Id_Externo__c", "renovarQrCodeSAP");
            }
        }

        [HttpGet("pedidomundipagg/cancelar/{opportunityId?}")]
        public async Task CancelPedidoMundiPaggAsync(string opportunityId)
        {
            List<SfCustomIntegracao> integracoes = new List<SfCustomIntegracao>();
            try{
                var client = controller().GetForceClient();
                
                SfCustomOpportunity opp = await client.QuerySingle<SfCustomOpportunity>(SfCustomOpportunity.BuildSelect() + " WHERE Id = '" + opportunityId + "'", false);
                PedidoMundiPagg pedido = innovaDbContext.PedidoMundiPagg.FirstOrDefault(c => c.IdPedido == opp.Codigo);

                try{
                    if(pedido != null){
                        pedido.Cancelado = 1;

                        innovaDbContext.Update(pedido);

                        innovaDbContext.SaveChanges();
                    }
                }
                catch(Exception ex)
                {
                    string servico = "cancelarPedidoMundiPaggSAP // " + opp.Id;
                    var message = ex.InnerException != null ? ex.InnerException.Message : null;
                string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                    SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                    integracoes.Add(integracao);
                }
            }
            catch(Exception ex)
            {
                string servico = "cancelarPedidoMundiPaggSAP";
                var message = ex.InnerException != null ? ex.InnerException.Message : null;
                string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                integracoes.Add(integracao);
            }
            
            if(integracoes.Any()){
                var integracaoJSON = JsonConvert.SerializeObject(integracoes, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultadoIntegracao = controller().UpsertDados(integracaoJSON, "Integracao__c", "Id_Externo__c", "cancelarPedidoMundiPaggSAP");
            }
        }

        [HttpGet("pedidopix/cancelar/{opportunityId?}")]
        public async Task CancelPedidoPIXAsync(string opportunityId)
        {
            List<SfCustomIntegracao> integracoes = new List<SfCustomIntegracao>();
            try{
                var client = controller().GetForceClient();
                
                SfCustomOpportunity opp = await client.QuerySingle<SfCustomOpportunity>(SfCustomOpportunity.BuildSelect() + " WHERE Id = '" + opportunityId + "'", false);
                PedidoBankPlus pedido = innovaDbContext.PedidoBankPlus.FirstOrDefault(c => c.NumeroPedido == opp.Codigo);

                try{
                    pedido.IDStatus = 4;

                    innovaDbContext.Update(pedido);

                    innovaDbContext.SaveChanges();
                }
                catch(Exception ex)
                {
                    string servico = "cancelarPedidoPIX // " + opp.Id;
                    var message = ex.InnerException != null ? ex.InnerException.Message : null;
                string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                    SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                    integracoes.Add(integracao);
                }
            }
            catch(Exception ex)
            {
                string servico = "cancelarPedidoPIX";
                var message = ex.InnerException != null ? ex.InnerException.Message : null;
                string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                integracoes.Add(integracao);
            }
            
            if(integracoes.Any()){
                var integracaoJSON = JsonConvert.SerializeObject(integracoes, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultadoIntegracao = controller().UpsertDados(integracaoJSON, "Integracao__c", "Id_Externo__c", "cancelarPedidoMundiPaggSAP");
            }
        }

        [HttpGet("calcularprodutos")]
        public async Task CalcularProdutos()
        {
            List<SfCustomIntegracao> integracoes = new List<SfCustomIntegracao>();
            try{
                var client = controller().GetForceClient();

                List<ProdutoQuantidade> produtos = new List<ProdutoQuantidade>();
                Dictionary<string, ProdutoQuantidade> mapProduto = new Dictionary<string, ProdutoQuantidade>();
                List<SfCustomProduct2> produtosSf = await client.Query<SfCustomProduct2>("SELECT Id, ItemCodeVida__c FROM Product2");

                foreach (var item in produtosSf)
                {
                    ProdutoQuantidade produto = new ProdutoQuantidade();
                    produto.Quantidade = 0;
                    produto.IdProduto = item.Id;
                    produto.CodigoProduto = item.ItemCodeVida;

                    produtos.Add(produto);
                    if(!mapProduto.ContainsKey(item.Id))
                    {
                        mapProduto.Add(item.Id, produto);
                    }
                }

                List<ProdutoOportunidade> produtosOpp = await client.Query<ProdutoOportunidade>("SELECT Product2Id, SUM(Quantity) Quantidade FROM OpportunityLineItem WHERE Opportunity.StageName = 'Finalizado' GROUP BY Product2Id");

                foreach (var item in produtosOpp)
                {
                    if(mapProduto.ContainsKey(item.Product2Id))
                    {
                        ProdutoQuantidade produto = mapProduto.GetValueOrDefault(item.Product2Id);

                        produto.Quantidade = Convert.ToInt32(item.Quantidade.Split(".")[0]);
                    }
                }

                List<string> codigosProdutos = innovaDbContext.EstoquePendente.Select(c=> c.Codigo).ToList();

                foreach (var item in produtos)
                {
                    try{
                        EstoquePendente estoquePendente = new EstoquePendente();
                        estoquePendente.Codigo = item.CodigoProduto;
                        estoquePendente.Quantidade = item.Quantidade;

                        if(codigosProdutos.Contains(item.CodigoProduto))
                        {
                            innovaDbContext.Update(estoquePendente);
                        }else{
                            await innovaDbContext.AddAsync(estoquePendente);
                        }  
                    }
                    catch(Exception ex)
                    {
                        string servico = "caulculaProdutosSAP // " + item.IdProduto;
                        var message = ex.InnerException != null ? ex.InnerException.Message : null;
                string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                        SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                        integracoes.Add(integracao);
                    }
                }

                await innovaDbContext.SaveChangesAsync();
            }
            catch(Exception ex)
            {
                string servico = "caulculaProdutosSAP";
                var message = ex.InnerException != null ? ex.InnerException.Message : null;
                string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                integracoes.Add(integracao);
            }
            
            if(integracoes.Any()){
                var integracaoJSON = JsonConvert.SerializeObject(integracoes, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultadoIntegracao = controller().UpsertDados(integracaoJSON, "Integracao__c", "Id_Externo__c", "caulculaProdutosSAP");
            }
        }

        [HttpGet("pagamento/{idCliente}")]
        public async Task<ActionResult<IEnumerable<GetContasReceberEQ>>> SincronizarPagamentos(string idCliente)
        {
            List<SfCustomIntegracao> integracoes = new List<SfCustomIntegracao>();
            DateTime? dateTime = new DateTime(1900, 01, 01, 0, 0, 0);
            
            var client = controller().GetForceClient();

            SfCustomAccount acc = await client.QuerySingle<SfCustomAccount>("SELECT Id, Name, Data_Serasa__c, Status_Serasa__c, Observacoes_Serasa__c, CardCode__c, PersonMobilePhone, Phone, PersonEmail, CreatedDate, Tipo_IE_INT__c, Description, Limite_de_Credito__c, Observacoes_Gestor__c, Observacoes_Vendedor__c, LastModifiedDate, Tipo_de_Registro__c FROM Account WHERE Id = '" + idCliente + "'", false);
            List<SfCustomUser> vendedores = await client.Query<SfCustomUser>("SELECT Id, Codigo_do_Vendedor__c, Codigo_Vendedor_Area_Medica__c, Codigo_SAP__c FROM User WHERE isActive = true");
            List<GetContasReceberEQ> dadoSAP = innovaDbContext.GetContasReceberEQ.Where(c => c.CodigoCliente == acc.CardCode).ToList();
            
            foreach (var pagamento in dadoSAP)
            {
                try{
                    SfPagamento pag = new SfPagamento();

                    pag.Conta = idCliente;
                    string idVendedor = null;
                    var vendedor = vendedores.FirstOrDefault(c => c.CodigoVendedor == pagamento.CodigoVendedor || c.CodigoVendedorMed == pagamento.CodigoVendedor);
                    idVendedor = vendedor == null ? vendedores.FirstOrDefault(c => c.CodigoVendedor == 916 || c.CodigoVendedorMed == 916).Id : vendedores.FirstOrDefault(c => c.CodigoVendedor == pagamento.CodigoVendedor || c.CodigoVendedorMed == pagamento.CodigoVendedor).Id;
                    pag.OwnerId = idVendedor;
                    pag.Status = pagamento.Status;
                    pag.Name = pagamento.Documento;
                    pag.Emissao = pagamento.Emissao;
                    pag.NF = pagamento.NF.ToString();
                    pag.Vencimento = pagamento.Vencimento;
                    pag.ValorTitulo = pagamento.ValorTitulo;
                    pag.Empresa = pagamento.Filial.ToUpper();
                    string idExterno = pagamento.Filial + pagamento.Documento;
                    pag.Codigo_do_Vendedor = pagamento.CodigoVendedor.ToString();
                    pag.Baixa = pagamento.Baixa != dateTime ? pagamento.Baixa : null;

                    var paga = client.InsertOrUpdateRecord(SfPagamento.SObjectTypeName, "Documento__c", idExterno, pag).Result;
                }
                catch(Exception ex)
                {
                    string servico = "pagamentoClienteSAP // " + idCliente + " // " + pagamento.Documento;
                    var message = ex.InnerException != null ? ex.InnerException.Message : null;
                string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                    SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                    integracoes.Add(integracao);
                }
            }
        
            if(integracoes.Any()){
                var integracaoJSON = JsonConvert.SerializeObject(integracoes, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultadoIntegracao = controller().UpsertDados(integracaoJSON, "Integracao__c", "Id_Externo__c", "pagamentoClienteSAP");
            }

            return dadoSAP;
        }

        [HttpGet("carteira/vendedor/{idVendedor}")]
        public async Task<ActionResult<IEnumerable<CLIENTE_EQUILIBRIUM>>> GetClientesVendedor(string idVendedor)
        {
            List<String> idsExternos = new List<String>();
            List<SfCustomAccount> clientesSF = new List<SfCustomAccount>();
            List<SfCustomIntegracao> integracoes = new List<SfCustomIntegracao>();

            SObjectAttributes atributo = new SObjectAttributes();
            atributo.Type = "Account";
            
            SObjectAttributes atributoEnd = new SObjectAttributes();
            atributoEnd.Type = "Endereco__c";

            SObjectAttributes atributoContact = new SObjectAttributes();
            atributoContact.Type = "Contact";
            
            List<String> cpfsAdicionados = new List<String>();

            var client = controller().GetForceClient();
            List<SfCidade> cidades = controller().GetCidades(client);
            List<SfSegmento> segmentos = controller().GetSegmentos(client);
            List<Grupo_VDM> grupo = innovaDbContext.Grupo_VDM.ToList();
            List<SfEspecialidade> especialidades = controller().GetEspecialidades(client);
            List<SfTransportadora> transportadoras = controller().GetTransportadoras(client);
            List<SfCustomRecordType> tipoDeRegistro = controller().GetTipoDeRegistro(client);
            SfCustomUser vendedor = await client.QuerySingle<SfCustomUser>("SELECT Id, Codigo_do_Vendedor__c, Codigo_Vendedor_String__c, Codigo_Vendedor_Area_Medica__c FROM User WHERE IsActive = True AND Id = '" + idVendedor + "' limit 1");
            List<CLIENTE_EQUILIBRIUM> dadosCliente = new List<CLIENTE_EQUILIBRIUM>();

            if(vendedor == null){
                return dadosCliente;
            }

            try{
                dadosCliente = innovaDbContext.CLIENTE_EQUILIBRIUM.Where(cliente => (cliente.SlpCode == vendedor.CodigoVendedorInt || cliente.SlpCode == vendedor.CodigoVendedorMadInt) && cliente.CPF_CNPJ != null && cliente.CPF_CNPJ != "").ToList();
                
                int qtdContas = 0;

                foreach (var dadoSAP in dadosCliente)
                {
                    try
                    {
                        string idUser = vendedor.Id;

                        var cpfCnpjReplace = controller().RemoveCharSpecial(dadoSAP.CPF_CNPJ);

                        SfCustomAccount acc = new SfCustomAccount();
                        
                        acc.Attributes = atributo;
                        if (cpfCnpjReplace.Length > 11)
                        {   // Conta PJ
                            acc.Name = dadoSAP.CardName;
                            acc.Celular = dadoSAP.Fone_Cel;
                            acc.TipoDeRegistro = tipoDeRegistro.FirstOrDefault(t => t.IsPersonType == false).Id;
                        }
                        else
                        {   // Conta PF
                            acc.Email = controller().getEmail(dadoSAP.Email);
                            acc.CelularPessoal = dadoSAP.Fone_Cel;
                            int quantNome = dadoSAP.CardName.Split(" ").Length;
                            string[] nomeCompleto = dadoSAP.CardName.Split(" ");
                            acc.TipoDeRegistro = tipoDeRegistro.FirstOrDefault(t => t.IsPersonType == true).Id;
                            if(quantNome == 1)
                            {
                                acc.Sobrenome = dadoSAP.CardName;
                            }else if (quantNome >= 2)
                            {
                                acc.PrimeiroNome = nomeCompleto[0];
                                acc.MeioNome = "";
                                string restoNome = null;
                                for(int i = 1; i < quantNome; i++)
                                {
                                    restoNome = restoNome + " " + nomeCompleto[i];  
                                }
                                acc.Sobrenome = restoNome;
                            }
                        }

                        //Dados Básicos
                        acc.Rede = dadoSAP.Rede;
                        acc.CNPJCPF = cpfCnpjReplace;
                        acc.Suframa = dadoSAP.SUFRAMA;
                        acc.Fantasia = dadoSAP.CardFName;
                        acc.InscricaoMunicipal = dadoSAP.IM;
                        acc.Medicamento__c = dadoSAP.Medicamento == "S" ? "Sim" : "Não";
                        acc.Produto_Saude__c = dadoSAP.ProdSaude == "S" ? "Sim" : "Não";
                        acc.GrupoCliente = grupo.FirstOrDefault(g => g.GroupCode == dadoSAP.Grupo_Cliente).GroupName;
                        // Dados de Contato
                        acc.Fax = dadoSAP.Fone_2;
                        acc.Phone = dadoSAP.Fone_1;
                        acc.Website = dadoSAP.Site;
                        acc.EmailXML = controller().getEmail(dadoSAP.Email);
                        // Dados Financeiros
                        //acc.Saldo = dadoSAP.Saldo;
                        acc.SaldoSAP = dadoSAP.Saldo;
                        acc.LimiteCredito = dadoSAP.CreditLine;
                        acc.SubstitudoTributario = dadoSAP.SUBS_TRIB;
                        // Informações adicionais
                        acc.OwnerId = idUser;
                        acc.Sincronizado = true;
                        acc.CardCode = dadoSAP.CardCode;
                        acc.NrAlvara = dadoSAP.NR_ALVARA;
                        acc.DataAlvara = dadoSAP.DATA_ALVARA;
                        acc.PontosScore = dadoSAP.PontosScore;
                        acc.DataAbertura = dadoSAP.DataAbertura;
                        acc.CapitalSocial = dadoSAP.CapitalSocial;
                        acc.DataUltimaCompra = dadoSAP.DtUltCompra;
                        acc.Indicador_da_IE__c = dadoSAP.IndicadorDaIE;
                        acc.Simples_Nacional__c = dadoSAP.SimplesNacional;
                        acc.ObservacoesVendedor = dadoSAP.Observacao_venda;
                        acc.Indicador_de_Natureza__c = dadoSAP.IndicadorNatureza;
                        acc.Indicador_de_Op_Consumidor__c = dadoSAP.IndicadorDeOpConsumidor;
                        acc.Description = dadoSAP.Free_Text != null? dadoSAP.Free_Text.Length < 32000 ? dadoSAP.Free_Text : dadoSAP.Free_Text.Substring(0, 32000) : null;
                        if(!String.IsNullOrEmpty(dadoSAP.SituacaoIE.ToString())){acc.SituacaoIE = Convert.ToString(dadoSAP.SituacaoIE);}
                        if(!String.IsNullOrEmpty(dadoSAP.PorteEmpresa.ToString())){acc.PorteEmpresa = Convert.ToString(dadoSAP.PorteEmpresa);}
                        if(!String.IsNullOrEmpty(dadoSAP.DescricaoNatureza.ToString())){acc.DescricaoNatureza = Convert.ToString(dadoSAP.DescricaoNatureza);}
                        if(!String.IsNullOrEmpty(dadoSAP.SituacaoCadastralCnpj.ToString())){acc.SituacaoCadastralCnpj = Convert.ToString(dadoSAP.SituacaoCadastralCnpj);}
                        if(!String.IsNullOrEmpty(dadoSAP.CategoriaClientes.ToString())){acc.CategoriaClientes = Convert.ToString(dadoSAP.CategoriaClientes);}

                        acc.AceitaEmailMarketing = dadoSAP.U_Recebe_Email_Marketing == "S" ? "Sim" : "Não";
                        acc.ExclusaoLgpd = dadoSAP.U_Exclusao_Lgpd == "S" ? "Sim" : "Não";

                        acc.ObservacoesSerasa = dadoSAP.ObservacaoSerasa;
                        acc.DataSerasa = dadoSAP.DataSerasa;
                        if(dadoSAP.StatusSerasa.HasValue)
                            acc.StatusSerasa = dadoSAP.StatusSerasa.Value == 0 ? "Positivo" : "Negativo";

                        if(dadoSAP.CodigoTransportadora != null && transportadoras.FirstOrDefault(t => t.CardCode == dadoSAP.CodigoTransportadora) != null){
                            acc.Transportadora = transportadoras.FirstOrDefault(t => t.CardCode == dadoSAP.CodigoTransportadora).Id;
                        }
                        if (segmentos.Any(s => s.Codigo == dadoSAP.CodSegmento))
                        {
                            acc.Segmento = segmentos.FirstOrDefault(s => s.Codigo == dadoSAP.CodSegmento).Id;
                        }

                        if (especialidades.Any(e => e.Codigo == dadoSAP.CodEspecialidade))
                        {
                            acc.Especialidade = especialidades.FirstOrDefault(e => e.Codigo == dadoSAP.CodEspecialidade).Id;
                        }
                        if (dadoSAP.IE == null)
                        {
                            acc.TipoIE = "Isento";
                        }
                        else
                        {
                            if (dadoSAP.IE.Contains("Isento"))
                            {
                                acc.TipoIE = dadoSAP.IE;
                            }
                            else
                            {
                                acc.TipoIE = "Contribuinte";
                                acc.InscricaoEstadual = dadoSAP.IE;
                            }
                        }

                        if(!cpfsAdicionados.Contains(cpfCnpjReplace)){
                            clientesSF.Add(acc);
                            cpfsAdicionados.Add(cpfCnpjReplace);
                            qtdContas = qtdContas + 1;
                        }

                        if(qtdContas == 200){
                            var accountJSON = JsonConvert.SerializeObject(clientesSF, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                            var resultContas = controller().UpsertDados(accountJSON, "Account", "CNPJ_CPF__c", "upsertClientes");
                            //var resultJSON = JsonConvert.DeserializeObject(resultContas.ToString());
                            qtdContas = 0;
                            clientesSF = new List<SfCustomAccount>();
                        }
                    }
                    catch(Exception ex)
                    {
                        string servico = "atualizaCarteiraVendedorSAP / upsertClientes / "+dadoSAP.CardCode;
                        if(!idsExternos.Contains(servico))
                        {
                            var message = ex.InnerException != null ? ex.InnerException.Message : null;
                            string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                            SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                            integracoes.Add(integracao);
                            idsExternos.Add(servico);
                        }
                        continue;
                    }
                }

                var accountsJSON = JsonConvert.SerializeObject(clientesSF, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultado = controller().UpsertDados(accountsJSON, "Account", "CNPJ_CPF__c", "upsertClientes");

                /*List<SfEndereco> enderecosSF = new List<SfEndereco>();

                int qtdEndereco = 0;

                cpfsAdicionados = new List<string>();

                foreach (var dadoSAP in dadosCliente)
                {
                    try
                    {
                        var cpfCnpjReplace = controller().RemoveCharSpecial(dadoSAP.CPF_CNPJ);

                        var idConta = ""; 
                        var Conta = innovaDbContext.ClienteNew.FirstOrDefault(c => c.CliCnpj == dadoSAP.CPF_CNPJ || c.CliCpf == dadoSAP.CPF_CNPJ);
                        var contaLog = innovaDbContext.LogCadastro.FirstOrDefault(c => c.CardCode_Cliente == dadoSAP.CardCode);

                        if(Conta != null){
                            idConta = Conta.CliSf;
                        }else if(contaLog != null){
                            idConta = contaLog.Id_SF_Cliente;
                        } else {
                            continue;
                        }
                        
                        SfEndereco endEntrega = new SfEndereco();
                        endEntrega.Conta = idConta;
                        endEntrega.Bairro = dadoSAP.Setor;
                        endEntrega.Rua = dadoSAP.Endereco;
                        endEntrega.Attributes = atributoEnd;
                        endEntrega.CardCode = cpfCnpjReplace;
                        endEntrega.Numero = dadoSAP.Endereco_Nr;
                        endEntrega.Tipo = "Endereço de Entrega";
                        endEntrega.Name = "Endereço de Entrega";
                        endEntrega.Complemento = dadoSAP.Endereco_Comp.Length > 100 ? dadoSAP.Endereco_Comp.Substring(0, 100) : dadoSAP.Endereco_Comp;

                        if (dadoSAP.CEP != null){
                            var cep = dadoSAP.CEP.Replace(".", "").Replace(" ", "");
                            endEntrega.CEP = cep.Length > 9 ? cep.Substring(0, 9) : endEntrega.CEP = cep;
                        }

                        if (dadoSAP.CIDADE_CODIGO != "" && dadoSAP.CIDADE_CODIGO != "0" && dadoSAP.CIDADE_CODIGO != null){
                            endEntrega.CidadePesquisa = cidades.FirstOrDefault(c => c.Codigo == dadoSAP.CIDADE_CODIGO).Id;
                        }
                        
                        if(!cpfsAdicionados.Contains(cpfCnpjReplace)){
                            enderecosSF.Add(endEntrega);
                            cpfsAdicionados.Add(cpfCnpjReplace);
                            qtdEndereco = qtdEndereco + 1;
                        }

                        if(qtdEndereco == 200){
                            var enderecosEntrega = JsonConvert.SerializeObject(enderecosSF, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                            var resultEndereco = controller().UpsertDados(enderecosEntrega, "Endereco__c", "CardCode__c", "upsertEndereco");
                            qtdEndereco = 0;
                            enderecosSF = new List<SfEndereco>();
                        }
                    }
                    catch(Exception ex)
                    {
                        string servico = "atualizaCarteiraVendedorSAP / upsertEndereco / "+dadoSAP.CardCode;
                        if(!idsExternos.Contains(servico))
                        {
                            var message = ex.InnerException != null ? ex.InnerException.Message : null;
                            string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                            SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                            integracoes.Add(integracao);
                            idsExternos.Add(servico);
                        }
                        continue;
                    }
                }
                
                var enderecosJSON = JsonConvert.SerializeObject(enderecosSF, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultados = controller().UpsertDados(enderecosJSON, "Endereco__c", "CardCode__c", "upsertEndereco");

                List<SfCustomContact> contatosSF = new List<SfCustomContact>();

                int qtdContatos = 0;
                
                cpfsAdicionados = new List<string>();

                foreach (var dadoSAP in dadosCliente)
                {
                    try
                    {
                        string idUser = vendedor.Id;

                        var cpfCnpjReplace = controller().RemoveCharSpecial(dadoSAP.CPF_CNPJ);

                        var idConta = ""; 
                        var Conta = innovaDbContext.ClienteNew.FirstOrDefault(c => c.CliCnpj == dadoSAP.CPF_CNPJ || c.CliCpf == dadoSAP.CPF_CNPJ);
                        var contaLog = innovaDbContext.LogCadastro.FirstOrDefault(c => c.CardCode_Cliente == dadoSAP.CardCode);

                        if(Conta != null){
                            idConta = Conta.CliSf;
                        }else if(contaLog != null){
                            idConta = contaLog.Id_SF_Cliente;
                        } else {
                            continue;
                        }
                        
                        if (cpfCnpjReplace.Length > 11 && dadoSAP.Contato != "Sem Contato")
                        {
                            SfCustomContact cont = new SfCustomContact();

                            if(dadoSAP.Contato.Length > 50) {continue;}
                            cont.AccountId = idConta;
                            cont.OwnerId = idUser;
                            cont.Fax = dadoSAP.Fone_2;
                            cont.Phone = dadoSAP.Fone_1;
                            cont.CardCode = dadoSAP.CardCode;
                            cont.Attributes = atributoContact;
                            cont.MobilePhone = dadoSAP.Fone_Cel;
                            cont.Email = controller().getEmail(dadoSAP.Email);
                            int quantNome = dadoSAP.Contato.Split(" ").Length;
                            string[] nomeCompleto = dadoSAP.Contato.Split(" ");
                            if(quantNome == 1)
                            {
                                cont.LastName = dadoSAP.CardName;
                            }else if (quantNome >= 2)
                            {
                                cont.FirstName = nomeCompleto[0];
                                string restoNome = null;
                                for(int i = 1; i < quantNome; i++)
                                {
                                    restoNome = restoNome + " " + nomeCompleto[i];  
                                }
                                cont.LastName = restoNome;
                            }

                            if(!cpfsAdicionados.Contains(dadoSAP.CardCode)){
                                contatosSF.Add(cont);
                                cpfsAdicionados.Add(dadoSAP.CardCode);
                                qtdContatos = qtdContatos + 1;
                            }

                            if(qtdContatos == 200){
                                var contatos = JsonConvert.SerializeObject(contatosSF, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                                var resultContato = controller().UpsertDados(contatos, "Contact", "CardCode__c", "upsertContatos");
                                qtdContatos = 0;
                                contatosSF = new List<SfCustomContact>();
                            }
                        }
                    }
                    catch(Exception ex)
                    {
                        string servico = "atualizaCarteiraVendedorSAP / upsertContatos / "+dadoSAP.CardCode;
                        if(!idsExternos.Contains(servico))
                        {
                            var message = ex.InnerException != null ? ex.InnerException.Message : null;
                            string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                            SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                            integracoes.Add(integracao);
                            idsExternos.Add(servico);
                        }
                        continue;
                    }
                }

                if(contatosSF.Count() > 0){
                    var contatosJSON = JsonConvert.SerializeObject(contatosSF, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                    var resultadoContatos = controller().UpsertDados(contatosJSON, "Contact", "CardCode__c", "upsertContatos");
                }*/
            }
            catch(Exception ex)
            {
                string servico = "atualizaCarteiraVendedorSAP";
                var message = ex.InnerException != null ? ex.InnerException.Message : null;
                string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                integracoes.Add(integracao);
            }
        
            if(integracoes.Any()){
                var integracaoJSON = JsonConvert.SerializeObject(integracoes, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultadoIntegracao = controller().UpsertDados(integracaoJSON, "Integracao__c", "Id_Externo__c", "atualizaCarteiraVendedorSAP");
            }

            return dadosCliente;
        }

        // ******************* VERIFICADO ******************* 
        [HttpGet("cidades/{busca?}")]
        public async Task<ActionResult<IEnumerable<Cidade_VDM>>> GetCidades(int busca)
        {
            List<Cidade_VDM> dados = new List<Cidade_VDM>();
            List<SfCustomIntegracao> integracoes = new List<SfCustomIntegracao>();

            try
            {
                if (busca == 0)
                {
                    dados = innovaDbContext.Cidade_VDM.ToList();
                }
                else
                {
                    dados = innovaDbContext.Cidade_VDM.Where(cidade => cidade.Codigo == busca).ToList();
                }

                var client = controller().GetForceClient();

                foreach (var dadoSAP in dados)
                {
                    try
                    {
                        SfCidade cidade = new SfCidade();

                        cidade.Name = dadoSAP.Nome;
                        cidade.UF = dadoSAP.State;
                        cidade.Pais = dadoSAP.Pais;
                        cidade.CodigoIBGE = dadoSAP.CodigoIbge;

                        var result = await client.InsertOrUpdateRecord(SfCidade.SObjectTypeName, "Codigo__c", dadoSAP.Codigo.ToString(), cidade);
                    }
                    catch (Exception ex)
                    {
                        string servico = "atualizaCidadesSalesforce // " + dadoSAP.CodigoIbge + " // " + dadoSAP.Nome;
                        var message = ex.InnerException != null ? ex.InnerException.Message : null;
                string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                        SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                        integracoes.Add(integracao);
                    }
                }
            }
            catch(Exception ex)
            {
                string servico = "atualizaCidadesSalesforce";
                var message = ex.InnerException != null ? ex.InnerException.Message : null;
                string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                integracoes.Add(integracao);
            }
        
            if(integracoes.Any()){
                var integracaoJSON = JsonConvert.SerializeObject(integracoes, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultadoIntegracao = controller().UpsertDados(integracaoJSON, "Integracao__c", "Id_Externo__c", "atualizaCidadesSalesforce");
            }

            return dados;
        }

        [HttpGet("transportadoras/{busca?}")]
        public ActionResult<IEnumerable<Transportadora_VDM>> GetTransportadoras(string busca)
        {
            List<Transportadora_VDM> dados = new List<Transportadora_VDM>();
            List<SfCustomIntegracao> integracoes = new List<SfCustomIntegracao>();
            try
            {
                if (busca == null)
                {
                    dados = innovaDbContext.Transportadora_VDM.ToList();
                }
                else
                {
                    dados = innovaDbContext.Transportadora_VDM.Where(transportadora => transportadora.CardName.Contains(busca)).ToList();
                }

                var client = controller().GetForceClient();

                foreach (var dadoSAP in dados)
                {
                    try
                    {
                        SfTransportadora transp = new SfTransportadora();

                        transp.Name = dadoSAP.CardName.Length > 80 ? dadoSAP.CardName.Substring(0, 80) : dadoSAP.CardName;

                        var result = client.InsertOrUpdateRecord(SfTransportadora.SObjectTypeName, "CardCode__c", dadoSAP.CardCode, transp);
                    }
                    catch (Exception ex)
                    {
                        string servico = "atualizaTransportadoraSalesforce // " + dadoSAP.CardCode + " // " + dadoSAP.CardName;
                        var message = ex.InnerException != null ? ex.InnerException.Message : null;
                string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                        SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                        integracoes.Add(integracao);
                    }
                }
            }
            catch(Exception ex)
            {
                string servico = "atualizaTransportadoraSalesforce";
                var message = ex.InnerException != null ? ex.InnerException.Message : null;
                string mensagemErro = ex.Message + " // " + message + " // "+ ex.StackTrace;
                SfCustomIntegracao integracao = controller().CriarErroIntegracao(servico, mensagemErro);
                integracoes.Add(integracao);
            }
        
            if(integracoes.Any()){
                var integracaoJSON = JsonConvert.SerializeObject(integracoes, Formatting.None, new JsonSerializerSettings{ NullValueHandling = NullValueHandling.Ignore});
                var resultadoIntegracao = controller().UpsertDados(integracaoJSON, "Integracao__c", "Id_Externo__c", "atualizaTransportadoraSalesforce");
            }

            return dados;
        }

    }
}
