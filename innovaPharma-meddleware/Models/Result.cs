using System.Collections.Generic;

namespace SfCustom.Models
{
    public class Result
    {
        public string id { get; set; }
        public bool success { get; set; }
        public bool created { get; set; }
        public List<Error> errors { get; set; }
    }
}