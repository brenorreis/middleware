using System.Collections.Generic;

namespace SfCustom.Models
{
    public class Error
    {
        public string message { get; set; }
        public string statusCode { get; set; }
        public List<object> fields { get; set; }
    }
}
