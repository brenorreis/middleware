﻿using System.ComponentModel.DataAnnotations;

namespace InnovaPharmaMiddleware.Models
{
    public partial class PedidoItemBankPlus
    {
        [Key]
        public string IdSalesForce { get; set; }
        public string DBEmpresa { get; set; }
        public int NumeroPedido { get; set; }
        public string CodigoItem { get; set; }
        public double? Quantidade { get; set; }
        public decimal? PrecoUnitario { get; set; }
        public decimal? PercentualDesconto { get; set; }
        public decimal? CodigoUtilizacao { get; set; }
        public decimal? TotalTabela { get; set; }
    }
}
