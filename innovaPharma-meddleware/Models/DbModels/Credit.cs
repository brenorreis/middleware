﻿using System;

namespace InnovaPharmaMiddleware.Models
{
    public partial class Credit
    {
        public int IdNum { get; set; }
        public string CardCode { get; set; }
        public decimal? CreditLine { get; set; }
        public decimal? DebtLine { get; set; }
        public string FreeText { get; set; }
        public string Empresa { get; set; }
        public bool? AjustarLimiteCredito { get; set; }
        public bool? AjustarObservacaoCliente { get; set; }
        public DateTime? Data { get; set; }
        public int? StatusCad { get; set; }
        public string CodigoFormaPagamento { get; set; }
        public string RazaoSocial { get; set; }
        public Int16? GrupoCliente { get; set; }
        public int? TipoIE { get; set; }
        public string IE { get; set; }
        public string Telefone { get; set; }
        public string Celular { get; set; }
        public string Email { get; set; }
        public string Cidade { get; set; }
        public int? IdCidade { get; set; }
        public string Bairro { get; set; }
        public string Estado { get; set; }
        public string Endereco { get; set; }
        public string CEP { get; set; }
        public string Numero { get; set; }
        public string Complemento { get; set; }
        public int? TipoSerasa { get; set; }
        public string ObsSerasa { get; set; }
        public DateTime? DataSerasa { get; set; }
        public int? CodVendedor { get; set; }
        public string NomeFantasia { get; set; }
        public int? CLI_IND_NAT { get; set; }
        public int? CLI_IND_IE { get; set; }
        public int? CLI_IND_OPC { get; set; }
        public int? CLI_IND_SN { get; set; }
        public Int16? CLI_SIT_IE { get; set; }
        public Int16? CLI_SIT_CNPJ { get; set; }
        public Int16? CLI_DESC_NAT { get; set; }
        public decimal? CLI_CAP_SOCIAL { get; set; }
        public DateTime? CLI_DT_ABERTURA { get; set; }
        public Int16? CLI_PORTE_EMP { get; set; }
        public int? CLI_PONT_SCORE { get; set; }
        public int? CLI_CAT_CLIENTE { get; set; }
        public string REDE { get; set; }
        public string Consultor_Tecnico { get; set; }
        public int? Segmento { get; set; }
        public int? CLI_NR_DISTRIBUIDOR { get; set; }
        public Boolean CLI_VIP { get; set; }
    }
}
