﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InnovaPharmaMiddleware.Models
{
    //[Table("Associacao_VDM")]
    [Table("Associacao_VDM_INNOVABR")]
    public class Associacao_VDM
    {
        [Key]
        public string Codigo { get; set; }
        public string Nome { get; set; }
    }
}