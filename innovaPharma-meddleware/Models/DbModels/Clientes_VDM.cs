﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InnovaPharmaMiddleware.Models
{
    //[Table("Clientes_VDM")]
    //[Table("CLIENTE_EQUILIBRIUM")]
    [Table("CLIENTE_INNOVABR")]
    public class CLIENTE_EQUILIBRIUM
    {
        //Clientes_VDM
        public string Empresa { get; set; }
        [Key]
        public string CardCode { get; set; }
        public string CardName { get; set; }
        public decimal? CreditLine { get; set; }
        public string FatherCard { get; set; }
        public string FatherType { get; set; }
        public decimal? discount { get; set; }
        public string Comissao { get; set; }
        public decimal? DebtLine { get; set; }
        public decimal? Balance { get; set; }
        public string Free_Text { get; set; }
        public string Especialidade { get; set; }
        public string CIDADE_CODIGO { get; set; }
        public string Email { get; set; }
        public string Observacao_venda { get; set; }
        public string Endereco { get; set; }
        public string Endereco_Nr { get; set; }
        public string Endereco_Comp { get; set; }
        public string Setor { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
        public string CEP { get; set; }
        public string Fone_1 { get; set; }
        public string Fone_2 { get; set; }
        public string Fone_Cel { get; set; }
        public string Contato { get; set; }
        public string CPF_CNPJ { get; set; }
        public int? SlpCode { get; set; }
        public string CardFName { get; set; }
        public string SUFRAMA { get; set; }
        public string SUBS_TRIB { get; set; }
        public string IE { get; set; }
        public string IM { get; set; }
        public string CodSegmento { get; set; }
        public string Segmento { get; set; }
        public Int16 Grupo_Cliente { get; set; }
        public string CodEspecialidade { get; set; }
        public string NR_ALVARA { get; set; }
        public DateTime? DATA_ALVARA { get; set; }
        public string Site { get; set; }
        public decimal? Saldo {get; set;}
        public DateTime? DtUltCompra { get; set; }
        public int? IdUltPedido { get; set; }
        public DateTime? DtAtualizaSaldo { get; set; }
        public DateTime? Cadastro { get; set; }
        public DateTime? Alteracao { get; set; }
        public DateTime? DataAtualizacao { get; set; }
        public int? HoraAtualizacao { get; set; }
        public string IndicadorNatureza { get; set; }
        public string IndicadorDaIE { get; set; }
        public string IndicadorDeOpConsumidor { get; set; }
        public string SimplesNacional { get; set; }
        public string Medicamento { get; set; }
        public string ProdSaude { get; set; }
        public Int16? SituacaoIE { get; set; }
        public Int16? SituacaoCadastralCnpj { get; set; }
        public Int16? DescricaoNatureza { get; set; }
        public decimal? CapitalSocial { get; set; }
        public DateTime? DataAbertura { get; set; }
        public int? CategoriaClientes { get; set; }
        public int? PontosScore { get; set; }
        public Int16? PorteEmpresa { get; set; }
        public string CodigoTransportadora { get; set; }
        public string Rede { get; set; }
        public string U_Exclusao_Lgpd { get; set; }
        public string U_Recebe_Email_Marketing { get; set; }
        public Int16? StatusSerasa { get; set; }
        public string ObservacaoSerasa { get; set; }
        public DateTime? DataSerasa { get; set; }
        public string Distribuidor { get; set; }
        public string VIP { get; set; }
    }
}