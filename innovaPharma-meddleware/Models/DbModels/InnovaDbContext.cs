﻿using Microsoft.EntityFrameworkCore;

namespace InnovaPharmaMiddleware.Models
{
    public partial class InnovaDbContext : DbContext
    {
        public InnovaDbContext()
        {
        }

        public InnovaDbContext(DbContextOptions<InnovaDbContext> options)
            : base(options)
        {
        }
        public virtual DbSet<Pedido> Pedido { get; set; }
        public virtual DbSet<Credit> Credit { get; set; }
        public virtual DbSet<Empresas> Empresas { get; set; }
        public virtual DbSet<Itens_VDM> Itens_VDM { get; set; }
        public virtual DbSet<Grupo_VDM> Grupo_VDM { get; set; }
        public virtual DbSet<ClienteNew> ClienteNew { get; set; }
        public virtual DbSet<Cidade_VDM> Cidade_VDM { get; set; }
        public virtual DbSet<LogCadastro> LogCadastro { get; set; }
        public virtual DbSet<Familia_VDM> Familia_VDM { get; set; }
        public virtual DbSet<Margem_NOVO> Margem_NOVO { get; set; }
        public virtual DbSet<PedidoItens> PedidoItens { get; set; }
        public virtual DbSet<GetDevDocsEQ> GetDevDocsEQ { get; set; }
        public virtual DbSet<Vendedores_EQ> Vendedores_EQ { get; set; }
        public virtual DbSet<Segmentos_VDM> Segmentos_VDM { get; set; }
        public virtual DbSet<Associacao_VDM> Associacao_VDM { get; set; }
        public virtual DbSet<Cliente_Solicitacao> Cliente_Solicitacao { get; set; }
        public virtual DbSet<PedidoMundiPagg> PedidoMundiPagg { get; set; }
        public virtual DbSet<PedidoMundiPaggItens> PedidoMundiPaggItens { get; set; }
        public virtual DbSet<EstoquePendente> EstoquePendente { get; set; }
        public virtual DbSet<StatusPedidoSap> StatusPedidoSap { get; set; }
        public virtual DbSet<CLIENTE_EQUILIBRIUM> CLIENTE_EQUILIBRIUM { get; set; }
        public virtual DbSet<Transportadora_VDM> Transportadora_VDM { get; set; }
        public virtual DbSet<Margem_InnovaLabSF> Margem_InnovaLabSF { get; set; }
        public virtual DbSet<GetContasReceberEQ> GetContasReceberEQ { get; set; }
        public virtual DbSet<EnderecoEntrega_VDM> EnderecoEntrega_VDM { get; set; }
        public virtual DbSet<PedidoBankPlus> PedidoBankPlus { get; set; }
        public virtual DbSet<PedidoItemBankPlus> PedidoItemBankPlus { get; set; }
        public virtual DbSet<CondicoesPagamento_VDM> CondicoesPagamento_VDM { get; set; }
        public virtual DbSet<TB_Atividade_SAP> TB_Atividade_SAP { get; set; }
        public virtual DbSet<TB_Fases_Oportunidade> TB_Fases_Oportunidade { get; set; }
        public virtual DbSet<VW_GrupoEconomico_InnovaBrasil> VW_GrupoEconomico_InnovaBrasil { get; set; }
        public virtual DbSet<VW_VendedoresAdicional_InnovaBrasil> VW_VendedoresAdicional_InnovaBrasil { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.3-servicing-35854");

            modelBuilder.Entity<ClienteNew>(entity =>
            {
                entity.HasKey(e => new { e.CliEmpId, e.CliId })
                    .HasName("PK__Cliente___46E8191864A9C70F");

                entity.ToTable("Cliente_New");

                entity.Property(e => e.CliEmpId).HasColumnName("CLI_EMP_ID");

                entity.Property(e => e.CliId).HasColumnName("CLI_ID");

                entity.Property(e => e.CliSf).HasColumnName("CLI_SF");

                entity.Property(e => e.CliBairro)
                    .HasColumnName("CLI_BAIRRO")
                    .HasMaxLength(50);

                entity.Property(e => e.CliBairroC)
                    .HasColumnName("CLI_BAIRRO_C")
                    .HasMaxLength(50);

                entity.Property(e => e.CliCep)
                    .HasColumnName("CLI_CEP")
                    .HasMaxLength(20);

                entity.Property(e => e.CliCepC)
                    .HasColumnName("CLI_CEP_C")
                    .HasMaxLength(20);

                entity.Property(e => e.CliCidade)
                    .HasColumnName("CLI_CIDADE")
                    .HasMaxLength(50);

                entity.Property(e => e.CliCidadeC)
                    .HasColumnName("CLI_CIDADE_C")
                    .HasMaxLength(50);

                entity.Property(e => e.CliCnpj)
                    .HasColumnName("CLI_CNPJ")
                    .HasMaxLength(20);

                entity.Property(e => e.CliCompl)
                    .HasColumnName("CLI_COMPL")
                    .HasMaxLength(50);

                entity.Property(e => e.CliComplC)
                    .HasColumnName("CLI_COMPL_C")
                    .HasMaxLength(50);

                entity.Property(e => e.CliContato)
                    .HasColumnName("CLI_CONTATO")
                    .HasMaxLength(50);

                entity.Property(e => e.CliCpf)
                    .HasColumnName("CLI_CPF")
                    .HasMaxLength(20);

                entity.Property(e => e.CliCvtoId)
                    .HasColumnName("CLI_CVTO_ID")
                    .HasDefaultValueSql("('1')");

                entity.Property(e => e.CliDescFin)
                    .HasColumnName("CLI_DESC_FIN")
                    .HasColumnType("numeric(19, 6)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.CliDtaCad)
                    .HasColumnName("CLI_DTA_CAD")
                    .HasColumnType("datetime");

                entity.Property(e => e.CliDtaInc)
                    .HasColumnName("CLI_DTA_INC")
                    .HasColumnType("datetime");

                entity.Property(e => e.CliDtaNasc)
                    .HasColumnName("CLI_DTA_NASC")
                    .HasColumnType("date");

                entity.Property(e => e.CliEmail)
                    .HasColumnName("CLI_EMAIL")
                    .HasMaxLength(100);

                entity.Property(e => e.CliEmailXml)
                    .HasColumnName("CLI_EMAIL_XML")
                    .HasMaxLength(100);

                entity.Property(e => e.CliEmpOrg)
                    .HasColumnName("CLI_EMP_ORG")
                    .HasMaxLength(20);

                entity.Property(e => e.CliEspecialidade)
                    .HasColumnName("CLI_ESPECIALIDADE")
                    .HasMaxLength(50);

                entity.Property(e => e.CliFantasia)
                    .HasColumnName("CLI_FANTASIA")
                    .HasMaxLength(100);

                entity.Property(e => e.CliFlgRevenda)
                    .HasColumnName("CLI_FLG_REVENDA")
                    .HasMaxLength(1)
                    .HasDefaultValueSql("('N')");

                entity.Property(e => e.CliFoneCel)
                    .HasColumnName("CLI_FONE_CEL")
                    .HasMaxLength(100);

                entity.Property(e => e.CliFoneCom)
                    .HasColumnName("CLI_FONE_COM")
                    .HasMaxLength(100);

                entity.Property(e => e.CliFoneFax)
                    .HasColumnName("CLI_FONE_FAX")
                    .HasMaxLength(20);

                entity.Property(e => e.CliForma).HasColumnName("CLI_FORMA");

                entity.Property(e => e.CliGrpId).HasColumnName("CLI_GRP_ID");

                entity.Property(e => e.CliIdOrg)
                    .HasColumnName("CLI_ID_ORG")
                    .HasMaxLength(20);

                entity.Property(e => e.CliIe)
                    .HasColumnName("CLI_IE")
                    .HasMaxLength(20);

                entity.Property(e => e.CliIm)
                    .HasColumnName("CLI_IM")
                    .HasMaxLength(20);

                entity.Property(e => e.CliLat)
                    .HasColumnName("CLI_LAT")
                    .HasMaxLength(20);

                entity.Property(e => e.CliLimCred)
                    .HasColumnName("CLI_LIM_CRED")
                    .HasColumnType("numeric(19, 6)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.CliLng)
                    .HasColumnName("CLI_LNG")
                    .HasMaxLength(20);

                entity.Property(e => e.CliLogradouro)
                    .HasColumnName("CLI_LOGRADOURO")
                    .HasMaxLength(100);

                entity.Property(e => e.CliLogradouroC)
                    .HasColumnName("CLI_LOGRADOURO_C")
                    .HasMaxLength(100);

                entity.Property(e => e.CliMunId).HasColumnName("CLI_MUN_ID");

                entity.Property(e => e.CliMunIdC).HasColumnName("CLI_MUN_ID_C");

                entity.Property(e => e.CliNome)
                    .HasColumnName("CLI_NOME")
                    .HasMaxLength(100);

                entity.Property(e => e.CliNr)
                    .HasColumnName("CLI_NR")
                    .HasMaxLength(50);

                entity.Property(e => e.CliNrC)
                    .HasColumnName("CLI_NR_C")
                    .HasMaxLength(50);

                entity.Property(e => e.CliObs)
                    .HasColumnName("CLI_OBS")
                    .HasColumnType("text");

                entity.Property(e => e.CliObsAdm)
                    .HasColumnName("CLI_OBS_ADM")
                    .HasColumnType("text");

                entity.Property(e => e.CliObsImp)
                    .HasColumnName("CLI_OBS_IMP")
                    .HasColumnType("text");

                entity.Property(e => e.CliObsVend)
                    .HasColumnName("CLI_OBS_VEND")
                    .HasColumnType("text");

                entity.Property(e => e.CliRatvId).HasColumnName("CLI_RATV_ID");

                entity.Property(e => e.CliRefBanc)
                    .HasColumnName("CLI_REF_BANC")
                    .HasColumnType("text");

                entity.Property(e => e.CliRefCom)
                    .HasColumnName("CLI_REF_COM")
                    .HasColumnType("text");

                entity.Property(e => e.CliRegCons)
                    .HasColumnName("CLI_REG_CONS")
                    .HasMaxLength(20);

                entity.Property(e => e.CliRgNr)
                    .HasColumnName("CLI_RG_NR")
                    .HasMaxLength(20);

                entity.Property(e => e.CliRgOrgao)
                    .HasColumnName("CLI_RG_ORGAO")
                    .HasMaxLength(20);

                entity.Property(e => e.CliRgUf)
                    .HasColumnName("CLI_RG_UF")
                    .HasMaxLength(20);

                entity.Property(e => e.CliSaldo)
                    .HasColumnName("CLI_SALDO")
                    .HasColumnType("numeric(19, 6)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.CliSegId)
                    .HasColumnName("CLI_SEG_ID")
                    .HasDefaultValueSql("('1')");

                entity.Property(e => e.CliSituacao).HasColumnName("CLI_SITUACAO");

                entity.Property(e => e.CliStatusCad)
                    .HasColumnName("CLI_STATUS_CAD")
                    .HasDefaultValueSql("('1')");

                entity.Property(e => e.CliSubsTrib)
                    .HasColumnName("CLI_SUBS_TRIB")
                    .HasMaxLength(50);

                entity.Property(e => e.CliSuframa)
                    .HasColumnName("CLI_SUFRAMA")
                    .HasMaxLength(20);

                entity.Property(e => e.CliTipo).HasColumnName("CLI_TIPO");

                entity.Property(e => e.CliTipoIe)
                    .HasColumnName("CLI_TIPO_IE")
                    .HasDefaultValueSql("('1')");

                entity.Property(e => e.CliUf)
                    .HasColumnName("CLI_UF")
                    .HasMaxLength(2);

                entity.Property(e => e.CliUfC)
                    .HasColumnName("CLI_UF_C")
                    .HasMaxLength(2);

                entity.Property(e => e.CliUsrId).HasColumnName("CLI_USR_ID");

                entity.Property(e => e.CliVendId).HasColumnName("CLI_VEND_ID");

                entity.Property(e => e.CliVlrPedf)
                    .HasColumnName("CLI_VLR_PEDF")
                    .HasColumnType("numeric(19, 6)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.CliWebsite)
                    .HasColumnName("CLI_WEBSITE")
                    .HasMaxLength(100);
                    
                entity.Property(e => e.CliStatusDta)
                    .HasColumnName("CLI_STATUS_DTA")
                    .HasColumnType("datetime");

                entity.Property(e => e.REDE)
                    .HasColumnName("REDE")
                    .HasMaxLength(30);
            });

            modelBuilder.Entity<Credit>(entity =>
            {
                entity.HasKey(e => e.IdNum);

                entity.Property(e => e.IdNum).HasColumnName("id_num");

                entity.Property(e => e.CardCode)
                    .IsRequired()
                    .HasMaxLength(15);

                entity.Property(e => e.CodigoFormaPagamento)
                    .HasColumnName("Codigo_FormaPagamento")
                    .HasMaxLength(50);

                entity.Property(e => e.CreditLine).HasColumnType("numeric(19, 6)");

                entity.Property(e => e.Data).HasColumnType("datetime");

                entity.Property(e => e.DebtLine).HasColumnType("numeric(19, 6)");

                entity.Property(e => e.Empresa)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.FreeText)
                    .HasColumnName("Free_Text")
                    .HasColumnType("ntext");

                entity.Property(e => e.NomeFantasia)
                    .HasColumnName("NomeFantasia")
                    .HasColumnType("ntext");
                
                entity.Property(e => e.RazaoSocial)
                    .HasColumnName("RazaoSocial")
                    .HasMaxLength(100);

                entity.Property(e => e.GrupoCliente)
                    .HasColumnName("GrupoCliente");
                
                entity.Property(e => e.TipoIE)
                    .HasColumnName("TipoIE");
                    
                entity.Property(e => e.Telefone)
                    .HasColumnName("Telefone")
                    .HasMaxLength(20);
                    
                entity.Property(e => e.Celular)
                    .HasColumnName("Celular")
                    .HasMaxLength(20);
                    
                entity.Property(e => e.Email)
                    .HasColumnName("Email")
                    .HasMaxLength(100);
                    
                entity.Property(e => e.IdCidade)
                    .HasColumnName("IdCidade");
                    
                entity.Property(e => e.Bairro)
                    .HasColumnName("Bairro")
                    .HasMaxLength(100);
                    
                entity.Property(e => e.Estado)
                    .HasColumnName("Estado")
                    .HasMaxLength(2);
                    
                entity.Property(e => e.Endereco)
                    .HasColumnName("Endereco")
                    .HasMaxLength(100);
                    
                entity.Property(e => e.CEP)
                    .HasColumnName("Cep")
                    .HasMaxLength(20);
                    
                entity.Property(e => e.Numero)
                    .HasColumnName("Numero")
                    .HasMaxLength(100);
                
                entity.Property(e => e.Complemento)
                    .HasColumnName("Complemento")
                    .HasColumnType("ntext");
                    
                entity.Property(e => e.ObsSerasa)
                    .HasColumnName("ObsSerasa")
                    .HasMaxLength(50);
                    
                entity.Property(e => e.TipoSerasa)
                    .HasColumnName("TipoSerasa");
                    
                entity.Property(e => e.CodVendedor)
                    .HasColumnName("CodVendedor");

                entity.Property(e => e.DataSerasa)
                    .HasColumnName("DataSerasa")
                    .HasColumnType("datetime");
                    
                entity.Property(e => e.REDE)
                    .HasColumnName("REDE")
                    .HasMaxLength(30);
            });

            modelBuilder.Entity<Empresas>(entity =>
            {
                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Empresa)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<EstoquePendente>(entity =>
            {
                entity.Property(e => e.Codigo).ValueGeneratedNever();

                entity.Property(e => e.Quantidade);
            });

            modelBuilder.Entity<Pedido>(entity =>
            {
                entity.HasKey(e => new { e.Empresa, e.IdPedido });

                entity.Property(e => e.Empresa).HasMaxLength(20);

                entity.Property(e => e.IdPedido).HasColumnName("ID_Pedido");

                entity.Property(e => e.CodigoCliente)
                    .IsRequired()
                    .HasColumnName("Codigo_Cliente")
                    .HasMaxLength(50);

                entity.Property(e => e.CodigoEmitente)
                    .IsRequired()
                    .HasColumnName("Codigo_Emitente")
                    .HasMaxLength(50);

                entity.Property(e => e.CodigoFormaPagamento)
                    .HasColumnName("Codigo_FormaPagamento")
                    .HasMaxLength(50);

                entity.Property(e => e.CodigoPlanoNegocio)
                    .IsRequired()
                    .HasColumnName("Codigo_PlanoNegocio")
                    .HasMaxLength(50);

                entity.Property(e => e.CodigoTipoFrete)
                    .IsRequired()
                    .HasColumnName("Codigo_TipoFrete")
                    .HasMaxLength(50);

                entity.Property(e => e.CodigoTipoPagamento)
                    .IsRequired()
                    .HasColumnName("Codigo_TipoPagamento")
                    .HasMaxLength(50);

                entity.Property(e => e.CodigoTransportadora)
                    .IsRequired()
                    .HasColumnName("Codigo_Transportadora")
                    .HasMaxLength(50);

                entity.Property(e => e.CodigoVendedor)
                    .IsRequired()
                    .HasColumnName("Codigo_Vendedor")
                    .HasMaxLength(50);

                entity.Property(e => e.DataEnvio).HasColumnType("datetime");

                entity.Property(e => e.DataPedido).HasColumnType("datetime");
                entity.Property(e => e.DataStatus).HasColumnType("datetime");

                entity.Property(e => e.Empenho)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Fonte)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IdOrdemCompra)
                    .HasColumnName("ID_OrdemCompra")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IdPedidoSap)
                    .HasColumnName("ID_PedidoSAP")
                    .HasMaxLength(50);

                entity.Property(e => e.Indicador).HasMaxLength(2);

                entity.Property(e => e.IndicadorEstoque)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('N')");

                entity.Property(e => e.ObservacaoNf).HasColumnName("Observacao_NF");

                entity.Property(e => e.ObservacaoPedido).HasColumnName("Observacao_Pedido");

                entity.Property(e => e.PcDesconto)
                    .HasColumnName("PC_Desconto")
                    .HasColumnType("numeric(19, 6)");

                entity.Property(e => e.PcDescontoFin)
                    .HasColumnName("PC_DESCONTO_FIN")
                    .HasColumnType("numeric(13, 2)")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.Pregao)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Processo)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ReferenciaCliente)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ValorFrete).HasColumnType("numeric(14, 2)");
            });

            modelBuilder.Entity<PedidoItens>(entity =>
            {
                entity.HasKey(e => new { e.Id });

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Empresa).HasMaxLength(20);

                entity.Property(e => e.PedidoId).HasColumnName("PedidoID");

                entity.Property(e => e.ItemCodigo).HasMaxLength(20);

                entity.Property(e => e.ItemSequencia).HasDefaultValueSql("((1))");

                entity.Property(e => e.Desconto).HasColumnType("numeric(19, 6)");

                entity.Property(e => e.NumeroControle).HasMaxLength(6);

                entity.Property(e => e.NumeroControleId).HasMaxLength(50);

                entity.Property(e => e.PcDesconto)
                    .HasColumnName("Pc_Desconto")
                    .HasColumnType("numeric(19, 6)");

                entity.Property(e => e.PromocaoCodigo).HasMaxLength(50);

                entity.Property(e => e.TipoUtilizacao)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.ValorTabela).HasColumnType("numeric(18, 3)");

                entity.Property(e => e.ValorVenda).HasColumnType("numeric(19, 6)");
            });
            
            modelBuilder.Entity<PedidoMundiPagg>(entity =>
            {
                entity.HasKey(e => new { e.Empresa, e.IdPedido });

                entity.Property(e => e.Empresa).HasMaxLength(20);

                entity.Property(e => e.IdPedido).HasColumnName("IdPedido");

                entity.Property(e => e.Codigo_Cliente)
                    .IsRequired()
                    .HasColumnName("Codigo_Cliente")
                    .HasMaxLength(50);

                entity.Property(e => e.IdMundiPagg)
                    .HasColumnName("IdMundiPagg")
                    .HasMaxLength(50);

                entity.Property(e => e.Parcelas)
                    .HasColumnName("Parcelas")
                    .HasMaxLength(50);

                entity.Property(e => e.ValorTotal)
                    .HasColumnName("ValorTotal")
                    .HasColumnType("numeric(19, 6)");

                entity.Property(e => e.DataEnvio).HasColumnType("datetime");
                entity.Property(e => e.DataStatus).HasColumnType("datetime");

                entity.Property(e => e.StatusPedido)
                    .HasColumnName("StatusPedido")
                    .HasMaxLength(50);

                entity.Property(e => e.StatusMundiPagg)
                    .HasColumnName("StatusMundiPagg")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<PedidoMundiPaggItens>(entity =>
            {
                entity.HasKey(e => new { e.IdSalesForce });

                entity.Property(e => e.Empresa).HasMaxLength(20);

                entity.Property(e => e.IdPedido).HasColumnName("IdPedido");

                entity.Property(e => e.IdSalesForce).HasColumnName("IdSalesForce");

                entity.Property(e => e.ItemCodigo).HasMaxLength(20);

                entity.Property(e => e.Preco).HasColumnType("numeric(19, 2)");
            });

            modelBuilder.Entity<PedidoBankPlus>(entity =>
            {
                entity.HasKey(e => new { e.DBEmpresa, e.NumeroPedido });

                entity.Property(e => e.DBEmpresa).HasMaxLength(50);

                entity.Property(e => e.NumeroPedido).HasColumnName("NumeroPedido");

                entity.Property(e => e.CodigoCliente)
                    .IsRequired()
                    .HasColumnName("CodigoCliente")
                    .HasMaxLength(50);

                entity.Property(e => e.IDBanKPlus)
                    .HasColumnName("IDBanKPlus");

                entity.Property(e => e.DataEnvio).HasColumnType("datetime");
                entity.Property(e => e.DataStatus).HasColumnType("datetime");
                entity.Property(e => e.DataPedido).HasColumnType("datetime");

                entity.Property(e => e.IDStatus)
                    .HasColumnName("IDStatus")
                    .HasMaxLength(50);

                entity.Property(e => e.IDStatusPagamento)
                    .HasColumnName("IDStatusPagamento")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<PedidoItemBankPlus>(entity =>
            {
                entity.HasKey(e => new {e.IdSalesForce, e.DBEmpresa, e.NumeroPedido });

                entity.Property(e => e.DBEmpresa).HasMaxLength(50);

                entity.Property(e => e.NumeroPedido).HasColumnName("NumeroPedido");

                entity.Property(e => e.CodigoItem).HasColumnName("CodigoItem");

                entity.Property(e => e.PrecoUnitario).HasColumnType("numeric(19, 2)");
            });
        }
    }
}
