﻿using System.ComponentModel.DataAnnotations;
using System;

namespace InnovaPharmaMiddleware.Models
{
    public partial class PedidoItens
    {
        public int? Id { get; set; }
        public string Empresa { get; set; }
        public int PedidoId { get; set; }
        public string ItemCodigo { get; set; }
        public Int32? Quantidade { get; set; }
        public decimal? ValorVenda { get; set; }
        public decimal Desconto { get; set; }
        public string PromocaoCodigo { get; set; }
        public string TipoUtilizacao { get; set; }
        public Int32? QuantidadeVenda { get; set; }
        public int? QuantidadeCorte { get; set; }
        public string NumeroControle { get; set; }
        public string NumeroControleId { get; set; }
        [Key]
        public string IdSalesforce { get; set; }
        public int ItemSequencia { get; set; }
        public decimal? ValorTabela { get; set; }
        public decimal? PcDesconto { get; set; }
        public decimal? TotalTabela { get; set; }
    }
}
