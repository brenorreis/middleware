﻿namespace InnovaPharmaMiddleware.Models
{
    public partial class Frete
    {
        public decimal? ValorFrete { get; set; }
        public string CodigoTipoFrete { get; set; }
    }
}
