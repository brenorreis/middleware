﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InnovaPharmaMiddleware.Models
{
    [Table("VW_VendedoresAdicional_InnovaBrasil")]
    public class VW_VendedoresAdicional_InnovaBrasil
    {
        [Key]
        public Int64 Id { get; set; }
        public DateTime? Data { get; set; }
        public string CodigoCliente { get; set; }
        public string NomeCliente { get; set; }
        public int? CodigoVendedor { get; set; }
        public string NomeVendedor { get; set; }
        public string Tecnico { get; set; }
        public string Omnichannel { get; set; }
    }
}