﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InnovaPharmaMiddleware.Models
{
    //[Table("Margem_InnovaLabSF")]
    [Table("Margem_InnovaLabSF_INNOVABR")]
    public class Margem_InnovaLabSF
    {
        [Key]
        public string Id {get; set;}
        public string LinhaItemSf {get; set;}
        public decimal CMV { get; set; }
        public string CodigoNuvemSaas { get; set; }
        public decimal Contribuicao { get; set; }
        public DateTime Data { get; set; }
        public int? Devolucao { get; set; }
        public decimal Imposto { get; set; }
        public int? NF { get; set; }
        public int? Quantidade { get; set; }
        public decimal ReceitaLiquida { get; set; }
        public string Item_Cod { get; set; }
        public int? PedidoEmpDestino { get; set; }
        public decimal ValorLiquido { get; set; }
        public int? Vendedor_Cod { get; set; }
    }
}