﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InnovaPharmaMiddleware.Models
{
    //[Table("Familia_VDM")]
    [Table("Familia_VDM_INNOVABR")]
    public class Familia_VDM
    {
        [Key]
        public string Code { get; set; }
        public string Name { get; set; }
    }
}