﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InnovaPharmaMiddleware.Models
{
    //[Table("CondicoesPagamento_VDM")]
    [Table("CondicoesPagamento_VDM_INNOVABR")]
    public class CondicoesPagamento_VDM
    {
        public Int16 Codigo { get; set; }
        [Key]

        public string Descricao {get; set;}
    }
}