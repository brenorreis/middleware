﻿using System;
using System.ComponentModel.DataAnnotations;

namespace InnovaPharmaMiddleware.Models
{
    public partial class PedidoBankPlus
    {
        public DateTime? DataEnvio { get; set; }
        public DateTime? DataPedido { get; set; }
        public DateTime? DataStatus { get; set; }
        public int? IDStatus { get; set; }
        public int? IDStatusPagamento { get; set; }
        public int? IDBanKPlus { get; set; }
        public string DBEmpresa { get; set; }
        [Key]
        public int? NumeroPedido { get; set; }
        public string CodigoCliente { get; set; }
        public int? CodigoTitular { get; set; }
        public int? CodigoVendedor { get; set; }
        public string Email { get; set; }
        public decimal? PercentualDesconto { get; set; }
        public string Observacao { get; set; }
        public string LinkPagamento { get; set; }
        public DateTime? DataPagamento { get; set; }
        public string Descricao { get; set; }
        public string QrCodePix { get; set; }
        public decimal? ValorTotal { get; set; }
        public decimal? ValorTotalFrete { get; set; }
    }
}
