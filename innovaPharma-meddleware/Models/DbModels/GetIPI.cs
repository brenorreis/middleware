﻿namespace InnovaPharmaMiddleware.Models
{
    public class GetIPI
    {
        public decimal ValIPI { get; set; }
        public decimal BaseCalculo { get; set; }
    }
}