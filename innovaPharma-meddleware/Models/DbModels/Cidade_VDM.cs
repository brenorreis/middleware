﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InnovaPharmaMiddleware.Models
{
    //[Table("Cidade_VDM")]
    [Table("Cidade_VDM_INNOVABR")]
    public class Cidade_VDM
    {
        [Key]
        public int Codigo { get; set; }
        public string Nome { get; set; }
        public string State { get; set; }
        public string Pais { get; set; }
        public string CodigoIbge { get; set; }

    }
}