﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InnovaPharmaMiddleware.Models
{
    //[Table("EnderecoEntrega_VDM")]
    [Table("EnderecoEntrega_VDM_INNOVABR")]
    public class EnderecoEntrega_VDM
    {
        [Key]
        public string Codigo { get; set; }
        public string Empresa { get; set; }
        public string Tipo { get; set; }
        public string ENDERECO { get; set; }
        public string Numero { get; set; }
        public string Complemento { get; set; }
        public string Setor { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
        public string CEP { get; set; }
    }
}