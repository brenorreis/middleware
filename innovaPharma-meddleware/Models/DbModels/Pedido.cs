﻿using System;
using System.ComponentModel.DataAnnotations;

namespace InnovaPharmaMiddleware.Models
{
    public partial class Pedido
    {
        public string Empresa { get; set; }
        [Key]
        public int IdPedido { get; set; }
        public string IdPedidoSap { get; set; }
        public string CodigoCliente { get; set; }
        public string CodigoEmitente { get; set; }
        public string CodigoVendedor { get; set; }
        public string CodigoTransportadora { get; set; }
        public string CodigoTipoFrete { get; set; }
        public DateTime? DataEnvio { get; set; }
        public DateTime? DataPedido { get; set; }
        public string CodigoPlanoNegocio { get; set; }
        public string CodigoTipoPagamento { get; set; }
        public string ObservacaoPedido { get; set; }
        public string ObservacaoNf { get; set; }
        public string Status { get; set; }
        public string Origem { get; set; }
        public decimal? PcDesconto { get; set; }
        public string Indicador { get; set; }
        public string IndicadorEstoque { get; set; }
        public decimal? PcDescontoFin { get; set; }
        public string CodigoFormaPagamento { get; set; }
        public string IdOrdemCompra { get; set; }
        public string Empenho { get; set; }
        public string Pregao { get; set; }
        public string Processo { get; set; }
        public string Fonte { get; set; }
        public string ReferenciaCliente { get; set; }
        public decimal? ValorFrete { get; set; }
        public DateTime? DataStatus { get; set; }
        public string CodAutorizacao { get; set; }
        public string Id_Salesforce { get; set; }
        public string Codigo_TipoEntrega { get; set; }
        public string Bandeira_Cartao1 { get; set; }
        public string Bandeira_Cartao2 { get; set; }
        public int? N_Parcela_Cartao1 { get; set; }
        public int? N_Parcela_Cartao2 { get; set; }
        public string CodAutorizacao_Cartao1 { get; set; }
        public string CodAutorizacao_Cartao2 { get; set; }
        public decimal? Valor_Cartao1 { get; set; }
        public decimal? Valor_Cartao2 { get; set; }
        public string Cred_ou_Deb { get; set; }
        public string FormaPagamento { get; set; }
        
    }
}
