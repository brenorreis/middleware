﻿using System;
using System.ComponentModel.DataAnnotations;

namespace InnovaPharmaMiddleware.Models
{
    public partial class LogCadastro
    {
      [Key]
      public string Id { get; set; }
      public string Id_SF_Cliente { get; set; }
      public string Nome_Cliente { get; set; }
      public string CardCode_Cliente { get; set; }
      public string Id_SF_Usuário { get; set; }
      public string Nome_Usuario { get; set; }
      public DateTime? Data_de_Alteração { get; set; }
      public DateTime? Data_Cadastro { get; set; }
      public string Campo_Alterado { get; set; }
      public string Valor_Anterior { get; set; }
      public string Valor_Novo { get; set; }
    }
}
