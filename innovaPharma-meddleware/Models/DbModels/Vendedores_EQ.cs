﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InnovaPharmaMiddleware.Models
{
    [Table("Vendedores_EQ")]
    public class Vendedores_EQ
    {
        [Key]
        public string CodigoNovo { get; set; }
        public string CodigoAnterior { get; set; }
    }
}