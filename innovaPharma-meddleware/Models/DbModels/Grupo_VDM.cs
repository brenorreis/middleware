﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InnovaPharmaMiddleware.Models
{
    //[Table("Grupo_VDM")]
    [Table("Grupo_VDM_INNOVABR")]
    public class Grupo_VDM
    {
        [Key]
        public Int16 GroupCode { get; set; }
        public string GroupName { get; set; }
    }
}
