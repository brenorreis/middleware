﻿using System.ComponentModel.DataAnnotations;

namespace InnovaPharmaMiddleware.Models
{
    public class EstoquePendente
    {
        [Key]
        public string Codigo { get; set; }
        public int? Quantidade { get; set; }
    }
}
