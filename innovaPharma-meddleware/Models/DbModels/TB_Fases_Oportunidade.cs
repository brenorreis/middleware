﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InnovaPharmaMiddleware.Models
{
    [Table("TB_Fases_Oportunidade")]
    public class TB_Fases_Oportunidade
    {
        public string NM_Empresa { get; set; }
        public string N_Oportunidade { get; set; }
        [Key]
        public string ID_Fase { get; set; }
        public string Fase { get; set; }
        public DateTime Data_Inicio { get; set; }
        public DateTime Data_Termino { get; set; }
        public TimeSpan Hora_Inicio { get; set; }
        public TimeSpan Hora_Termino { get; set; }
        public decimal Duracao_Horas { get; set; }
        public decimal Duracao_Minutos { get; set; }
        public decimal Duracao_Dias { get; set; }
        public string Forma_Pagamento { get; set; }
    }
}