﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InnovaPharmaMiddleware.Models
{
    //[Table("GetDevDocsEQ")]
    [Table("GetDevDocsEQ_INNOVABR")]
    public class GetDevDocsEQ
    {
        [Key]
        public int? NF { get; set; }
        public string BASE { get; set; }
        public string CLIENTE { get; set; }
        public DateTime DtDev { get; set; }
    }
}