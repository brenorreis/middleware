﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InnovaPharmaMiddleware.Models
{
    //[Table("GetContasReceberEQ")]
    [Table("GetContasReceberEQ_INNOVABR")]
    public class GetContasReceberEQ
    {
        [Key]
        public string Documento { get; set; }
        public int? CodigoVendedor { get; set; }
        public decimal ValorTitulo { get; set; }
        public DateTime? Emissao { get; set; }
        public DateTime? Vencimento { get; set; }
        public DateTime? Baixa { get; set; }
        public DateTime? CriacaoBaixa { get; set; }
        public DateTime? DtModificacao { get; set; }
        public int? Atraso { get; set; }
        public string CodigoCliente { get; set; }
        public string Status { get; set; }
        public string Filial { get; set; }
        public int? NF { get; set; }
    }
}