﻿namespace InnovaPharmaMiddleware.Models
{
    public partial class PedidoMundiPaggItens
    {
        public string Empresa { get; set; }
        public int IdPedido { get; set; }
        public string IdSalesForce { get; set; }
        public string ItemCodigo { get; set; }
        public double? Quantidade { get; set; }
        public decimal? Preco { get; set; }
        public decimal? TotalTabela { get; set; }
    }
}
