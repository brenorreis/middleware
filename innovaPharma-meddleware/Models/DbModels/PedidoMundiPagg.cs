﻿using System;
using System.ComponentModel.DataAnnotations;

namespace InnovaPharmaMiddleware.Models
{
    public partial class PedidoMundiPagg
    {
        public string Empresa { get; set; }
        [Key]
        public string IdSalesForce { get; set; }
        public int? IdPedido { get; set; }
        public string IdMundiPagg { get; set; }
        public string Codigo_Cliente { get; set; }
        public decimal? ValorTotal { get; set; }
        public int? Parcelas { get; set; }
        public DateTime? DataEnvio { get; set; }
        public int? StatusPedido { get; set; }
        public int? StatusMundiPagg { get; set; }
        //1 - Pendente
        //2 - Pago
        //3 - Recusado
        //4 - Cancelado
        public DateTime? DataStatus { get; set; }
        public string Observacao { get; set; }
        public string Email { get; set; }
        public string LinkPagamento { get; set; }
        public int? Cancelado { get; set; }
        public int? QuantidadeCartoes { get; set; }
        public string CodAutorizacao { get; set; }
        public decimal? ValorFrete { get; set; }
    }
}
