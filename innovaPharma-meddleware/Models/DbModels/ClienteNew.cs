﻿using System;
using System.ComponentModel.DataAnnotations;

namespace InnovaPharmaMiddleware.Models
{
    public partial class ClienteNew
    {
        public int CliEmpId { get; set; }
        public int CliId { get; set; }
        [Key]
        public string CliSf { get; set; }
        public string CliEmpOrg { get; set; }
        public string CliIdOrg { get; set; }
        public int? CliSituacao { get; set; }
        public string CliNome { get; set; }
        public string CliFantasia { get; set; }
        public int? CliTipo { get; set; }
        public int? CliGrpId { get; set; }
        public string CliCnpj { get; set; }
        public int? CliTipoIe { get; set; }
        public string CliIe { get; set; }
        public string CliIm { get; set; }
        public string CliSuframa { get; set; }
        public string CliSubsTrib { get; set; }
        public string CliCpf { get; set; }
        public DateTime? CliDtaNasc { get; set; }
        public string CliRgNr { get; set; }
        public string CliRgOrgao { get; set; }
        public string CliRgUf { get; set; }
        public string CliEspecialidade { get; set; }
        public string CliRegCons { get; set; }
        public string CliCep { get; set; }
        public string CliLogradouro { get; set; }
        public string CliNr { get; set; }
        public string CliCompl { get; set; }
        public string CliBairro { get; set; }
        public string CliCidade { get; set; }
        public string CliUf { get; set; }
        public int? CliMunId { get; set; }
        public string CliLat { get; set; }
        public string CliLng { get; set; }
        public string CliCepC { get; set; }
        public string CliLogradouroC { get; set; }
        public string CliNrC { get; set; }
        public string CliComplC { get; set; }
        public string CliBairroC { get; set; }
        public string CliCidadeC { get; set; }
        public string CliUfC { get; set; }
        public int? CliMunIdC { get; set; }
        public string CliContato { get; set; }
        public string CliFoneCom { get; set; }
        public string CliFoneFax { get; set; }
        public string CliFoneCel { get; set; }
        public string CliEmail { get; set; }
        public string CliEmailXml { get; set; }
        public string CliWebsite { get; set; }
        public int? CliVendId { get; set; }
        public int? CliCvtoId { get; set; }
        public int? CliForma { get; set; }
        public int? CliSegId { get; set; }
        public decimal? CliLimCred { get; set; }
        public string CliFlgRevenda { get; set; }
        public decimal? CliVlrPedf { get; set; }
        public decimal? CliSaldo { get; set; }
        public decimal? CliDescFin { get; set; }
        public string CliObs { get; set; }
        public string CliObsVend { get; set; }
        public string CliObsAdm { get; set; }
        public string CliRefCom { get; set; }
        public string CliRefBanc { get; set; }
        public string CliUsrId { get; set; }
        public DateTime? CliDtaInc { get; set; }
        public int? CliStatusCad { get; set; }
        public string CliObsImp { get; set; }
        public DateTime? CliDtaCad { get; set; }
        public int? CliRatvId { get; set; }
        
        public DateTime? CliStatusDta { get; set; }
        public int? CLI_IND_NAT { get; set; }
        public int? CLI_IND_IE { get; set; }
        public int? CLI_IND_OPC { get; set; }
        public int? CLI_IND_SN { get; set; }
        public DateTime? DataAbertura { get; set; }
        public int? PorteEmpresa { get; set; }
        public decimal? CapitalSocial { get; set; }
        public Int16? CLI_SIT_IE { get; set; }
        public Int16? CLI_SIT_CNPJ { get; set; }
        public Int16? CLI_DESC_NAT { get; set; }
        public decimal? CLI_CAP_SOCIAL { get; set; }
        public DateTime? CLI_DT_ABERTURA { get; set; }
        public Int16? CLI_PORTE_EMP { get; set; }
        public int? CLI_PONT_SCORE { get; set; }
        public int? CLI_CAT_CLIENTE { get; set; }
        public string REDE { get; set; }
        public string Consultor_Tecnico { get; set; }
        public DateTime? CLI_DTA_SERASA { get; set; }
        public int? CLI_NR_DISTRIBUIDOR { get; set; }
        public Boolean CLI_VIP { get; set; }
        public int? CLI_SERASA { get; set; }
    }
}
