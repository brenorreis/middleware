﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InnovaPharmaMiddleware.Models
{
    [Table("TB_Atividade_SAP")]
    public class TB_Atividade_SAP
    {
        public string Doc_Relacionado { get; set; } //20
        public string Cod_Cliente { get; set; } //20
        public DateTime Data_Inicio { get; set; }
        public DateTime DT_Criacao { get; set; }
        public DateTime Data_Termino { get; set; }
        public TimeSpan Hora_Inicio { get; set; }
        public TimeSpan Hora_Termino { get; set; }
        public string Status { get; set; } //20
        public string Tipo { get; set; } //20
        public string Duracao { get; set; } //20
        public string Atividade { get; set; } //20
        public string Observacoes { get; set; } //NO banco esta como nvarchar 1
        public string Conteudo { get; set; } //NO banco esta como nvarchar 1
        public string Assunto { get; set; } //NO banco esta como nvarchar 1
        public string DOC_relacionado_SAP { get; set; } //20
        [Key]
        public string ID_registro_CRM { get; set; } //20
        public decimal Duracao_Horas { get; set; } //No banco esta com acentuação
        public decimal duracao_Minutos { get; set; }  //No banco esta com acentuação
        public decimal duracao_Dias { get; set; } //No banco esta com acentuação
        public string COD_Solicitante { get; set; } //20
        public string Nome_Conta { get; set; } //40
        public string Segmento { get; set; } //20
        public string Aprovador { get; set; } //50
        public string NM_Empresa { get; set; }
        public string Forma_Pagamento { get; set; }
    }
}