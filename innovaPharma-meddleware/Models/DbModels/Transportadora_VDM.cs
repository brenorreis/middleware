﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InnovaPharmaMiddleware.Models
{
    //[Table("Transportadora_VDM")]
    [Table("Transportadora_VDM_INNOVABR")]
    public class Transportadora_VDM
    {
        [Key]
        public string CardCode { get; set; }
        public string CardName { get; set; }
    }
}