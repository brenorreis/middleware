﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InnovaPharmaMiddleware.Models
{
    //[Table("Segmentos_VDM")]
    [Table("Segmentos_VDM_INNOVABR")]
    public class Segmentos_VDM
    {
        [Key]
        public string Codigo { get; set; }
        public string Descricao { get; set; }
    }
}