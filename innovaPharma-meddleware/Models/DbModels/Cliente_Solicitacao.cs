﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InnovaPharmaMiddleware.Models
{
    [Table("Cliente_Solicitacao")]
    public class Cliente_Solicitacao
    {
        [Key]
        public string Id_Solicitacao { get; set; }
        public string Nome_Cliente { get; set; }
        public string ID_SF_Cliente { get; set; }
        public string CardCode { get; set; }
        public DateTime? Data_Solicitacao { get; set; }
        public DateTime? Data_Resposta { get; set; }
        public string Nome_Solicitante { get; set; }
        public string ID_SF_Solicitante { get; set; }
        public string Nome_Aprovador { get; set; }
        public string ID_SF_Aprovador { get; set; }
        public string Etapa_Aprovacao { get; set; }
        public string Status { get; set; }
        public string Motivo_Recusa { get; set; }
    }
}