﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InnovaPharmaMiddleware.Models
{
    //[Table("StatusPedidoSap")]
    [Table("StatusPedidoSap_INNOVABR")]
    public partial class StatusPedidoSap
    {
        [Key]
        public string PedidoSales { get; set; }
        public int? PedidoSap { get; set; }
        public string StatusPedido { get; set; }
        public string StatusPedidoSap2 { get; set; }
        public string Chave { get; set; }
        public int? Danfe { get; set; }
        public string StatusTabelaPEdido { get; set; }
        public DateTime? DataAtualizacao { get; set; }
        public DateTime? DataAtualizacaoNota { get; set; }
    }
}
