﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InnovaPharmaMiddleware.Models
{
    [Table("VW_GrupoEconomico_InnovaBrasil")]
    public class VW_GrupoEconomico_InnovaBrasil
    {
        [Key]
        public int? ID { get; set; }
        public DateTime? Data { get; set; }
        public string CodigoGrupo { get; set; }
        public string NomeGrupo { get; set; }
        public DateTime? DataEntrada { get; set; }
        public string Codigo { get; set; }
        public string Nome { get; set; }
        public string Documento { get; set; }
        public decimal? CapitalSocial { get; set; }
        public decimal? Capital { get; set; }
        public string Observacao { get; set; }
    }
}