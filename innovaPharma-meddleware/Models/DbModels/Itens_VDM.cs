﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace InnovaPharmaMiddleware.Models
{
    //[Table("Itens_VDM")]
    [Table("Itens_VDM_INNOVABR")]
    public class Itens_VDM
    {
        [Key]
        public string ItemCodeVida { get; set; }
        public string ItemCodeMil { get; set; }
        public string ItemName { get; set; }
        public string U_Linha { get; set; }
        public string Linha { get; set; }
        public decimal? Price { get; set; }
        public decimal? PriceM { get; set; }
        public decimal? PriceB { get; set; }
        public int? EstoqueCD { get; set; }
        public decimal? Expr1 { get; set; }
        public int? EstoqueRevenda { get; set; }
        public int? EstoqueRecebimento { get; set; }
        public int? EstoqueLJ { get; set; }
        public string NCM { get; set; }
        public int? EmbPadrao { get; set; }
        public string CodigoBarras { get; set; }
        public string U_RegMS { get; set; }
        public string U_AlvaraClasse { get; set; }
        public string U_Departamento { get; set; }
        public string U_Categoria { get; set; }
        public string Categoria { get; set; }
        public string U_Familia { get; set; }
        public string U_SubFamilia { get; set; }
        public string CEST { get; set; }
        public string DUN14 { get; set; }
        public string CodFci { get; set; }
        public string Empresas { get; set; }
        public string Secao { get; set; }
        public decimal? Comprimento { get; set; }
        public decimal? Largura { get; set; }
        public decimal? Altura { get; set; }
        public decimal? Volume { get; set; }
        public decimal? Peso { get; set; }
        public short? GrupoComissao { get; set; }
        public decimal? PcntComissao { get; set; }
        public decimal? Custo { get; set; }
        public int? Estoque_Evento { get; set; }
        public string CodigoDepositoRevenda { get; set; }
        public string CodigoDepositoEvento { get; set; }
        public int? Estoque_Navarro { get; set; }
        public string CodigoDepositoNavarro { get; set; }
    }
}