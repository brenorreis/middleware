﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using NetCoreForce.Client.Models;
using NetCoreForce.Models;
using Newtonsoft.Json;

namespace SfCustom.Models
{
    public partial class SfAccountTeamMember : SfCustomObject
    {
        public SfAccountTeamMember()
        {
        }

        [JsonIgnore]
        public static string SObjectTypeName 
        {
            get 
            {
                return "AccountTeamMember";
            } 
        }
        
        [JsonProperty(PropertyName = "Id_Externo__c")]
        public string Id_Externo__c { get; set; }

        [JsonProperty(PropertyName = "AccountId")]
        public string AccountId { get; set; }

        [JsonProperty(PropertyName = "TeamMemberRole")]
        public string TeamMemberRole { get; set; }

        [JsonProperty(PropertyName = "AccountAccessLevel")]
        public string AccountAccessLevel { get; set; }

        [JsonProperty(PropertyName = "OpportunityAccessLevel")]
        public string OpportunityAccessLevel { get; set; }

        [JsonProperty(PropertyName = "CaseAccessLevel")]
        public string CaseAccessLevel { get; set; }

        [JsonProperty(PropertyName = "ContactAccessLevel")]
        public string ContactAccessLevel { get; set; }

        [JsonProperty(PropertyName = "UserId")]
        public string UserId { get; set; }

        public static string BuildSelect()
        {
            List<String> retorno = new List<string>();

            List<String> removerCampos = new List<string>{"lastAmountChangedHistory", "lastCloseDateChangedHistory", "masterRecord", "masterRecordId", "parentId", "parent", "owner", "createdBy", "lastModifiedBy", "campaign", "pricebook2", "cleanStatus", "dunsNumber", "tradestyle", "naicsCode", "naicsDesc", "yearStarted", "dandbCompanyId", "dandbCompany", "attributes", "account", "probability", "Codigo"};

            PropertyInfo[] props = typeof(SfAccountTeamMember).GetProperties();

            foreach (PropertyInfo prop in props)
            {
                object[] attrs = prop.GetCustomAttributes(true);
                foreach (object attr in attrs)
                {
                    JsonPropertyAttribute jSonAttr = attr as JsonPropertyAttribute;
                    if (jSonAttr != null)
                    {
                        retorno.Add(jSonAttr.PropertyName);
                    }
                }
            }

            foreach (String campo in removerCampos)
            {            
                retorno.Remove(campo);
            }

            return $"Select {String.Join(", ", retorno)} From AccountTeamMember";
        }

    }
}