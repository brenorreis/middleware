﻿using Newtonsoft.Json;
using NetCoreForce.Models;

namespace SfCustom.Models
{
    public partial class SfCustomPricebookEntry : SfPricebookEntry
    {
        public SfCustomPricebookEntry()
        {
        }
        
        [JsonProperty(PropertyName = "Cogido_Externo__c")]
        public string CodigoExterno { get; set; }

    }
}