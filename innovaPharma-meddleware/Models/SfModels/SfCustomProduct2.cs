﻿using Newtonsoft.Json;
using NetCoreForce.Models;

namespace SfCustom.Models
{
    public partial class SfCustomProduct2 : SfProduct2
    {
        public SfCustomProduct2()
        {
        }
        
        [JsonProperty(PropertyName = "ItemCodeVida__c")]
        public string ItemCodeVida { get; set; }
        
        [JsonProperty(PropertyName = "Estoque_SAP__c")]
        public int? Estoque { get; set; }
        
        [JsonProperty(PropertyName = "Estoque_SAP_Evento__c")]
        public int? EstoqueEvento { get; set; }
        
        [JsonProperty(PropertyName = "Estoque_SAP_Navarro__c")]
        public int? EstoqueNavarro { get; set; }
        
        [JsonProperty(PropertyName = "Alvara_Classe__c")]
        public string AlvaraClasse { get; set; }

        [JsonProperty(PropertyName = "Empresa__c")]
        public string Empresa { get; set; }

        [JsonProperty(PropertyName = "Segmento__c")]
        public string Segmento__c { get; set; }

        [JsonProperty(PropertyName = "Deposito_Revenda__c")]
        public string Deposito_Revenda__c { get; set; }

        [JsonProperty(PropertyName = "Deposito_Evento__c")]
        public string Deposito_Evento__c { get; set; }

        [JsonProperty(PropertyName = "Deposito_Navarro__c")]
        public string Deposito_Navarro__c { get; set; }

        [JsonProperty(PropertyName = "Categoria__c")]
        public string Categoria__c { get; set; }
    }
}
