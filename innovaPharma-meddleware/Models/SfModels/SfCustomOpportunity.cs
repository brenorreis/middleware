﻿using System;
using System.Linq;
using Newtonsoft.Json;
using System.Reflection;
using NetCoreForce.Models;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SfCustom.Models
{
    public partial class SfCustomOpportunity : SfOpportunity    
    {
        public SfCustomOpportunity()
        {
        }

        [JsonProperty(PropertyName = "CardCode__c")]
        public string ContaCardCode { get; set; }

        [JsonProperty(PropertyName = "Forma_de_Pagamento__c")]
        public string FormaDePagamento { get; set; }
        
        [JsonProperty(PropertyName = "Condicao__c")]
        public string Condicao { get; set; }   
        
        [JsonProperty(PropertyName = "Status_SAP__c")]
        public string StatusSAP { get; set; }    
        
        [JsonProperty(PropertyName = "Codigo_SAP__c")]
        public string Codigo_SAP__c { get; set; }    

        [JsonProperty(PropertyName = "Observaces_do_Pedido__c")]

        public string ObservacesDoPedido { get; set; }
        
        [JsonProperty(PropertyName = "Observacoes_NF__c")]
        public string ObservacesDaNF { get; set; } 
        
        [JsonProperty(PropertyName = "Data_emissao_NF__c")]
        public DateTime? DataEmissaoNF { get; set; }
        
        [JsonProperty(PropertyName = "Data_Qr_Code__c")]
        public DateTime? DataQRCode { get; set; }

        [JsonProperty(PropertyName = "Cod_Vendedor__c")]
        public string CodVendedor { get; set; }

        [JsonProperty(PropertyName = "n_NF__c")]
        public string nNF { get; set; }

        [JsonProperty(PropertyName = "Sincronizado__c")]
        public bool Sincronizado { get; set; }
        
        [JsonProperty(PropertyName = "Codigo__c")]
        public decimal? CodigoSf { get; set; }
        [JsonIgnore]
        public int Codigo => (int) (CodigoSf??0);
        
        [JsonProperty(PropertyName = "Codigo_Emitente__c")]
        public string CodigoEmitente { get; set; }
        
        [JsonProperty(PropertyName = "Chave_NF__c")]
        public string ChaveNF { get; set; }
        
        [JsonProperty(PropertyName = "Status_pedido__c")]
        public string StatusPedido { get; set; }
        
        [JsonProperty(PropertyName = "Status_pedido_SAP__c")]
        public string StatusPedidoSAP { get; set; }
        
        [JsonProperty(PropertyName = "Codigo_Transportadora__c")]
        public string CodigoTransportadora { get; set; }
        
        [JsonProperty(PropertyName = "Status_MundiPagg__c")]
        public string Status_MundiPagg__c { get; set; }
        
        [JsonProperty(PropertyName = "Enviado_MundiPagg__c")]
        public bool Enviado_MundiPagg__c { get; set; }
        
        [JsonProperty(PropertyName = "bypass__c")]
        public bool bypass__c { get; set; }
        
        [JsonProperty(PropertyName = "VIP__c")]
        public bool VIP { get; set; }
        
        [JsonProperty(PropertyName = "Oportunidade_em_evento__c")]
        public bool Oportunidade_em_evento__c { get; set; }

        [JsonProperty(PropertyName = "Frete_gratis__c")]
        public bool Frete_gratis__c { get; set; }
        
        [JsonProperty(PropertyName = "Financeiro__c")]
        public string Financeiro__c { get; set; }
        
        [JsonProperty(PropertyName = "Dep_Desconto__c")]
        public string Dep_Desconto__c { get; set; }
        
        [JsonProperty(PropertyName = "Dep_Estoque__c")]
        public string Dep_Estoque__c { get; set; }
        
        [JsonProperty(PropertyName = "Codigo_de_Autorizacao__c")]
        public string Codigo_de_Autorizacao__c { get; set; }

        [JsonProperty(PropertyName = "Email_do_Contato__c")]
        public string Email_do_Contato__c { get; set; }

        [JsonProperty(PropertyName = "Link_de_Pagamento__c")]
        public string Link_de_Pagamento__c { get; set; }

        [JsonProperty(PropertyName = "Quantidade_de_cartoes__c")]
        public string Quantidade_de_cartoes__c { get; set; }

        [JsonProperty(PropertyName = "Status_Integracao__c")]
        public string Status_Integracao__c { get; set; }

        [JsonProperty(PropertyName = "Bandeira__c")]
        public string Bandeira__c { get; set; }

        [JsonProperty(PropertyName = "Bandeira_2__c")]
        public string Bandeira_2__c { get; set; }

        [JsonProperty(PropertyName = "NSU_1__c")]
        public string NSU_1__c { get; set; }

        [JsonProperty(PropertyName = "NSU_2__c")]
        public string NSU_2__c { get; set; }

        [JsonProperty(PropertyName = "Numero_de_Parcelas__c")]
        public string Numero_de_Parcelas__c { get; set; }

        [JsonProperty(PropertyName = "Numero_de_Parcelas_2__c")]
        public string Numero_de_Parcelas_2__c { get; set; }

        [JsonProperty(PropertyName = "Valor_cartao_1__c")]
        public decimal? Valor_cartao_1__c { get; set; }

        [JsonProperty(PropertyName = "Valor_cartao_2__c")]
        public decimal? Valor_cartao_2__c { get; set; }

        [JsonProperty(PropertyName = "Valor__c")]
        public decimal? Valor__c { get; set; }

        [JsonProperty(PropertyName = "Empresa__c")]
        public string Empresa { get; set; }

        [JsonProperty(PropertyName = "QR_Code_PIX__c")]
        public string QR_Code_PIX__c { get; set; }
        

        public static String BuildSelect()
        {
            List<String> retorno = new List<string>();

            List<String> removerCampos = new List<string>{"lastAmountChangedHistory", "lastCloseDateChangedHistory", "masterRecord", "masterRecordId", "parentId", "parent", "owner", "createdBy", "lastModifiedBy", "campaign", "pricebook2",
            "cleanStatus", "dunsNumber", "tradestyle", "naicsCode", "naicsDesc", "yearStarted", "dandbCompanyId", "dandbCompany", "attributes", "account", "probability", "Codigo"};

            PropertyInfo[] props = typeof(SfCustomOpportunity).GetProperties();

            foreach (PropertyInfo prop in props)
            {
                object[] attrs = prop.GetCustomAttributes(true);
                foreach (object attr in attrs)
                {
                    JsonPropertyAttribute jSonAttr = attr as JsonPropertyAttribute;
                    if (jSonAttr != null)
                    {
                        retorno.Add(jSonAttr.PropertyName);
                    }
                }
            }

            foreach (String campo in removerCampos)
            {            
                retorno.Remove(campo);
            }

            return $"Select {String.Join(", ", retorno)} From Opportunity";
        }
    }
}