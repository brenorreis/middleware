﻿using System;
using Newtonsoft.Json;
using System.Reflection;
using NetCoreForce.Models;
using System.Collections.Generic;
using NetCoreForce.Client.Models;

namespace SfCustom.Models
{
    public partial class SfCustomHistoryAccount : SfAccountHistory
    {
        public SfCustomHistoryAccount()
        {
        }

        [JsonProperty(PropertyName = "account")]
        public SfCustomAccount CustomAccount { get; set; }
        
        [JsonProperty(PropertyName = "Account.Name")]
        public string NomeCliente { get; set; }

        [JsonProperty(PropertyName = "Account.CreatedDate")]
        public DateTime? DataCriacao { get; set; }

        [JsonProperty(PropertyName = "CreatedBy.Name")]
        public string NomeUsuario { get; set; }

        [JsonProperty(PropertyName = "Account.CardCode__c")]
        public String CardCode { get; set; }
        

        public static String BuildSelect()
        {
            List<String> retorno = new List<string>();

            List<String> removerCampos = new List<string>{"account", "account","masterRecord", "masterRecordId", "parentId", "parent", "owner", "createdBy", "lastModifiedBy", "cleanStatus", "dunsNumber", "tradestyle", "naicsCode", "naicsDesc", "yearStarted", "dandbCompanyId", "dandbCompany", "attributes", "billingStreet","billingCity", "billingState", "billingPostalCode", "billingCountry", "billingLatitude", "billingLongitude", "billingGeocodeAccuracy", "billingAddress", "shippingStreet", "shippingCity", "shippingState", "shippingPostalCode", "shippingCountry", "shippingLatitude", "shippingLongitude", "shippingGeocodeAccuracy", "shippingAddress"};

            PropertyInfo[] props = typeof(SfCustomHistoryAccount).GetProperties();

            foreach (PropertyInfo prop in props)
            {
                object[] attrs = prop.GetCustomAttributes(true);
                foreach (object attr in attrs)
                {
                    JsonPropertyAttribute jSonAttr = attr as JsonPropertyAttribute;
                    if (jSonAttr != null)
                    {
                        retorno.Add(jSonAttr.PropertyName);
                    }
                }
            }

            foreach (String campo in removerCampos)
            {            
                retorno.Remove(campo);
            }

            return $"Select {String.Join(", ", retorno)} From AccountHistory ";
        }

        public static implicit operator CreateResponse(SfCustomHistoryAccount v)
        {
            throw new NotImplementedException();
        }
    }
}