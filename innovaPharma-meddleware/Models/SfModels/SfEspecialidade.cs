﻿using Newtonsoft.Json;

namespace SfCustom.Models
{
    public partial class SfEspecialidade : SfCustomObject
    {
        public SfEspecialidade()
        {
        }

        [JsonIgnore]
        public static string SObjectTypeName { 
            get 
            {
                return "Especialidade__c";
            } 
        }

        [JsonProperty(PropertyName = "Codigo__c")]
        public string Codigo { get; set; }
    }
}
