﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Newtonsoft.Json;

namespace SfCustom.Models
{
    public partial class SfPagamento : SfCustomObject
    {
        public SfPagamento()
        {
        }

        [JsonIgnore]
        public static string SObjectTypeName 
        {
            get 
            {
                return "Pagamento__c";
            } 
        }

        [JsonProperty(PropertyName = "Documento__c")]
        public string Documento { get; set; }

        [JsonProperty(PropertyName = "Baixa__c")]
        public DateTime? Baixa { get; set; }
        
        [JsonProperty(PropertyName = "Conta__c")]
        public string Conta { get; set; }

        [JsonProperty(PropertyName = "Codigo_do_Vendedor__c")]
        public string Codigo_do_Vendedor { get; set; }
        
        [JsonProperty(PropertyName = "Emissao__c")]
        public DateTime? Emissao { get; set; }

        [JsonProperty(PropertyName = "Valor_Titulo__c")]
        public decimal ValorTitulo { get; set; }

        [JsonProperty(PropertyName = "OwnerId")]
        public string OwnerId { get; set; }

        [JsonProperty(PropertyName = "Status__c")]
        public string Status { get; set; }

        [JsonProperty(PropertyName = "Empresa__c")]
        public string Empresa { get; set; }

        [JsonProperty(PropertyName = "NF__c")]
        public string NF { get; set; }

        [JsonProperty(PropertyName = "Vencimento__c")]
        public DateTime? Vencimento { get; set; }

        [JsonProperty(PropertyName = "Conta__r")]
        public SfCustomAccount ContaRelacionamento { get; set; }

        public static String BuildSelect()
        {
            List<String> retorno = new List<string>();

            List<String> removerCampos = new List<string>{"lastAmountChangedHistory", "lastCloseDateChangedHistory", "masterRecord", "masterRecordId", "parentId", "parent", "owner", "createdBy", "lastModifiedBy", "campaign", "pricebook2",
            "cleanStatus", "dunsNumber", "tradestyle", "naicsCode", "naicsDesc", "yearStarted", "dandbCompanyId", "dandbCompany", "attributes", "account", "probability", "Codigo", "Conta__r"};

            PropertyInfo[] props = typeof(SfPagamento).GetProperties();

            foreach (PropertyInfo prop in props)
            {
                object[] attrs = prop.GetCustomAttributes(true);
                foreach (object attr in attrs)
                {
                    JsonPropertyAttribute jSonAttr = attr as JsonPropertyAttribute;
                    if (jSonAttr != null)
                    {
                        retorno.Add(jSonAttr.PropertyName);
                    }
                }
            }

            foreach (String campo in removerCampos)
            {            
                retorno.Remove(campo);
            }

            return $"Select {String.Join(", ", retorno)} From Pagamento__c";
        }

    }
}
