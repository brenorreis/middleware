﻿using Newtonsoft.Json;

namespace SfCustom.Models
{
    public partial class SfCidade : SfCustomObject
    {
        public SfCidade()
        {
        }

        [JsonIgnore]
        public static string SObjectTypeName 
        {
            get 
            {
                return "Cidade__c";
            } 
        }

        [JsonProperty(PropertyName = "Codigo__c")]
        public string Codigo { get; set; }

        [JsonProperty(PropertyName = "UF__c")]
        public string UF { get; set; }

        [JsonProperty(PropertyName = "Codigo_IBGE__c")]
        public string CodigoIBGE { get; set; }
        
        [JsonProperty(PropertyName = "Pais__c")]
        public string Pais { get; set; }
    }
}
