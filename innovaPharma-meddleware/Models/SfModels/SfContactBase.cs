﻿#region assembly NetCoreForce.Models, Version=3.0.0.0, Culture=neutral, PublicKeyToken=null
// NetCoreForce.Models.dll
#endregion

using System;
using Newtonsoft.Json;
using SfCustom.Models;
using NetCoreForce.Client.Models;
using NetCoreForce.Client.Attributes;

namespace NetCoreForce.Models
{
    public class SfContactBase : SObject
    {
        [JsonIgnore]
        public static string SObjectTypeName { get; }
        [Createable(false)]
        [JsonProperty(PropertyName = "createdById")]
        [Updateable(false)]
        public string CreatedById { get; set; }
        
        [Createable(false)]
        [JsonProperty(PropertyName = "createdDate")]
        [Updateable(false)]
        public DateTimeOffset? CreatedDate { get; set; }
        
        [Createable(false)]
        [JsonProperty(PropertyName = "owner")]
        [Updateable(false)]
        public SfUser Owner { get; set; }
        
        [JsonProperty(PropertyName = "ownerId")]
        public string OwnerId { get; set; }
        
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }
        
        [JsonProperty(PropertyName = "birthdate")]
        public DateTime? Birthdate { get; set; }
        
        [JsonProperty(PropertyName = "leadSource")]
        public string LeadSource { get; set; }
        
        [JsonProperty(PropertyName = "assistantName")]
        public string AssistantName { get; set; }
        
        [JsonProperty(PropertyName = "department")]
        public string Department { get; set; }
        
        [JsonProperty(PropertyName = "title")]
        public string Title { get; set; }
        
        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }
        
        [Createable(false)]
        [JsonProperty(PropertyName = "reportsTo")]
        [Updateable(false)]
        public SfContact ReportsTo { get; set; }
        
        [JsonProperty(PropertyName = "reportsToId")]
        public string ReportsToId { get; set; }
        
        [Createable(false)]
        [JsonProperty(PropertyName = "createdBy")]
        [Updateable(false)]
        public SfUser CreatedBy { get; set; }
        
        [Createable(false)]
        [JsonProperty(PropertyName = "lastModifiedDate")]
        [Updateable(false)]
        public DateTimeOffset? LastModifiedDate { get; set; }
        
        [Createable(false)]
        [JsonProperty(PropertyName = "lastModifiedById")]
        [Updateable(false)]
        public string LastModifiedById { get; set; }
        
        [Createable(false)]
        [JsonProperty(PropertyName = "lastModifiedBy")]
        [Updateable(false)]
        public SfUser LastModifiedBy { get; set; }
        
        [Createable(false)]
        [JsonProperty(PropertyName = "systemModstamp")]
        [Updateable(false)]
        public DateTimeOffset? SystemModstamp { get; set; }
        
        [Createable(false)]
        [JsonProperty(PropertyName = "lastActivityDate")]
        [Updateable(false)]
        public DateTime? LastActivityDate { get; set; }
        
        [Createable(false)]
        [JsonProperty(PropertyName = "lastCURequestDate")]
        [Updateable(false)]
        public DateTimeOffset? LastCURequestDate { get; set; }
        
        [Createable(false)]
        [JsonProperty(PropertyName = "lastCUUpdateDate")]
        [Updateable(false)]
        public DateTimeOffset? LastCUUpdateDate { get; set; }
        
        [Createable(false)]
        [JsonProperty(PropertyName = "lastViewedDate")]
        [Updateable(false)]
        public DateTimeOffset? LastViewedDate { get; set; }
        
        [Createable(false)]
        [JsonProperty(PropertyName = "lastReferencedDate")]
        [Updateable(false)]
        public DateTimeOffset? LastReferencedDate { get; set; }
        
        [JsonProperty(PropertyName = "emailBouncedReason")]
        public string EmailBouncedReason { get; set; }
        
        [JsonProperty(PropertyName = "emailBouncedDate")]
        public DateTimeOffset? EmailBouncedDate { get; set; }
        
        [Createable(false)]
        [JsonProperty(PropertyName = "isEmailBounced")]
        [Updateable(false)]
        public bool? IsEmailBounced { get; set; }
        
        [Createable(false)]
        [JsonProperty(PropertyName = "photoUrl")]
        [Updateable(false)]
        public string PhotoUrl { get; set; }
        //
        // Resumo:
        //     Data.com Key
        //     Name: Jigsaw
        //     SF Type: string
        //     Nillable: True
        [JsonProperty(PropertyName = "jigsaw")]
        public string Jigsaw { get; set; }
        //
        // Resumo:
        //     Jigsaw Contact ID
        //     Name: JigsawContactId
        //     SF Type: string
        //     Nillable: True
        [Createable(false)]
        [JsonProperty(PropertyName = "jigsawContactId")]
        [Updateable(false)]
        public string JigsawContactId { get; set; }
        //
        // Resumo:
        //     Clean Status
        //     Name: CleanStatus
        //     SF Type: picklist
        //     Nillable: True
        [JsonProperty(PropertyName = "cleanStatus")]
        public string CleanStatus { get; set; }
        //
        // Resumo:
        //     Asst. Phone
        //     Name: AssistantPhone
        //     SF Type: phone
        //     Nillable: True
        [JsonProperty(PropertyName = "assistantPhone")]
        public string AssistantPhone { get; set; }
        //
        // Resumo:
        //     Other Phone
        //     Name: OtherPhone
        //     SF Type: phone
        //     Nillable: True
        [JsonProperty(PropertyName = "otherPhone")]
        public string OtherPhone { get; set; }
        //
        // Resumo:
        //     Home Phone
        //     Name: HomePhone
        //     SF Type: phone
        //     Nillable: True
        [JsonProperty(PropertyName = "homePhone")]
        public string HomePhone { get; set; }
        //
        // Resumo:
        //     Mobile Phone
        //     Name: MobilePhone
        //     SF Type: phone
        //     Nillable: True
        [JsonProperty(PropertyName = "mobilePhone")]
        public string MobilePhone { get; set; }
        //
        // Resumo:
        //     Contact ID
        //     Name: Id
        //     SF Type: id
        //     Nillable: False
        [Createable(false)]
        [JsonProperty(PropertyName = "id")]
        [Updateable(false)]
        public string Id { get; set; }
        //
        // Resumo:
        //     Deleted
        //     Name: IsDeleted
        //     SF Type: boolean
        //     Nillable: False
        [Createable(false)]
        [JsonProperty(PropertyName = "isDeleted")]
        [Updateable(false)]
        public bool? IsDeleted { get; set; }
        //
        // Resumo:
        //     Master Record ID
        //     Name: MasterRecordId
        //     SF Type: reference
        //     Nillable: True
        [Createable(false)]
        [JsonProperty(PropertyName = "masterRecordId")]
        [Updateable(false)]
        public string MasterRecordId { get; set; }
        //
        // Resumo:
        //     ReferenceTo: Contact
        //     RelationshipName: MasterRecord
        [Createable(false)]
        [JsonProperty(PropertyName = "masterRecord")]
        [Updateable(false)]
        public SfContact MasterRecord { get; set; }
        //
        // Resumo:
        //     Account ID
        //     Name: AccountId
        //     SF Type: reference
        //     Nillable: True
        [JsonProperty(PropertyName = "accountId")]
        public string AccountId { get; set; }
        //
        // Resumo:
        //     ReferenceTo: Account
        //     RelationshipName: Account
        [Createable(false)]
        [JsonProperty(PropertyName = "account")]
        [Updateable(false)]
        public SfCustomAccount Account { get; set; }
        //
        // Resumo:
        //     Last Name
        //     Name: LastName
        //     SF Type: string
        //     Nillable: False
        [JsonProperty(PropertyName = "lastName")]
        public string LastName { get; set; }
        //
        // Resumo:
        //     First Name
        //     Name: FirstName
        //     SF Type: string
        //     Nillable: True
        [JsonProperty(PropertyName = "firstName")]
        public string FirstName { get; set; }
        //
        // Resumo:
        //     Salutation
        //     Name: Salutation
        //     SF Type: picklist
        //     Nillable: True
        [JsonProperty(PropertyName = "salutation")]
        public string Salutation { get; set; }
        //
        // Resumo:
        //     Full Name
        //     Name: Name
        //     SF Type: string
        //     Nillable: False
        [Createable(false)]
        [JsonProperty(PropertyName = "name")]
        [Updateable(false)]
        public string Name { get; set; }
        //
        // Resumo:
        //     Other Street
        //     Name: OtherStreet
        //     SF Type: textarea
        //     Nillable: True
        [JsonProperty(PropertyName = "otherStreet")]
        public string OtherStreet { get; set; }
        //
        // Resumo:
        //     Other City
        //     Name: OtherCity
        //     SF Type: string
        //     Nillable: True
        [JsonProperty(PropertyName = "otherCity")]
        public string OtherCity { get; set; }
        //
        // Resumo:
        //     Other State/Province
        //     Name: OtherState
        //     SF Type: string
        //     Nillable: True
        [JsonProperty(PropertyName = "otherState")]
        public string OtherState { get; set; }
        //
        // Resumo:
        //     Other Zip/Postal Code
        //     Name: OtherPostalCode
        //     SF Type: string
        //     Nillable: True
        [JsonProperty(PropertyName = "otherPostalCode")]
        public string OtherPostalCode { get; set; }
        //
        // Resumo:
        //     Individual ID
        //     Name: IndividualId
        //     SF Type: reference
        //     Nillable: True
        [JsonProperty(PropertyName = "individualId")]
        public string IndividualId { get; set; }
        //
        // Resumo:
        //     Other Country
        //     Name: OtherCountry
        //     SF Type: string
        //     Nillable: True
        [JsonProperty(PropertyName = "otherCountry")]
        public string OtherCountry { get; set; }
        //
        // Resumo:
        //     Other Longitude
        //     Name: OtherLongitude
        //     SF Type: double
        //     Nillable: True
        [JsonProperty(PropertyName = "otherLongitude")]
        public double? OtherLongitude { get; set; }
        //
        // Resumo:
        //     Other Geocode Accuracy
        //     Name: OtherGeocodeAccuracy
        //     SF Type: picklist
        //     Nillable: True
        [JsonProperty(PropertyName = "otherGeocodeAccuracy")]
        public string OtherGeocodeAccuracy { get; set; }
        //
        // Resumo:
        //     Other Address
        //     Name: OtherAddress
        //     SF Type: address
        //     Nillable: True
        [Createable(false)]
        [JsonProperty(PropertyName = "otherAddress")]
        [Updateable(false)]
        public Address OtherAddress { get; set; }
        //
        // Resumo:
        //     Mailing Street
        //     Name: MailingStreet
        //     SF Type: textarea
        //     Nillable: True
        [JsonProperty(PropertyName = "mailingStreet")]
        public string MailingStreet { get; set; }
        //
        // Resumo:
        //     Mailing City
        //     Name: MailingCity
        //     SF Type: string
        //     Nillable: True
        [JsonProperty(PropertyName = "mailingCity")]
        public string MailingCity { get; set; }
        //
        // Resumo:
        //     Mailing State/Province
        //     Name: MailingState
        //     SF Type: string
        //     Nillable: True
        [JsonProperty(PropertyName = "mailingState")]
        public string MailingState { get; set; }
        //
        // Resumo:
        //     Mailing Zip/Postal Code
        //     Name: MailingPostalCode
        //     SF Type: string
        //     Nillable: True
        [JsonProperty(PropertyName = "mailingPostalCode")]
        public string MailingPostalCode { get; set; }
        //
        // Resumo:
        //     Mailing Country
        //     Name: MailingCountry
        //     SF Type: string
        //     Nillable: True
        [JsonProperty(PropertyName = "mailingCountry")]
        public string MailingCountry { get; set; }
        //
        // Resumo:
        //     Mailing Latitude
        //     Name: MailingLatitude
        //     SF Type: double
        //     Nillable: True
        [JsonProperty(PropertyName = "mailingLatitude")]
        public double? MailingLatitude { get; set; }
        //
        // Resumo:
        //     Mailing Longitude
        //     Name: MailingLongitude
        //     SF Type: double
        //     Nillable: True
        [JsonProperty(PropertyName = "mailingLongitude")]
        public double? MailingLongitude { get; set; }
        //
        // Resumo:
        //     Mailing Geocode Accuracy
        //     Name: MailingGeocodeAccuracy
        //     SF Type: picklist
        //     Nillable: True
        [JsonProperty(PropertyName = "mailingGeocodeAccuracy")]
        public string MailingGeocodeAccuracy { get; set; }
        //
        // Resumo:
        //     Mailing Address
        //     Name: MailingAddress
        //     SF Type: address
        //     Nillable: True
        [Createable(false)]
        [JsonProperty(PropertyName = "mailingAddress")]
        [Updateable(false)]
        public Address MailingAddress { get; set; }
        //
        // Resumo:
        //     Business Phone
        //     Name: Phone
        //     SF Type: phone
        //     Nillable: True
        [JsonProperty(PropertyName = "phone")]
        public string Phone { get; set; }
        //
        // Resumo:
        //     Business Fax
        //     Name: Fax
        //     SF Type: phone
        //     Nillable: True
        [JsonProperty(PropertyName = "fax")]
        public string Fax { get; set; }
        //
        // Resumo:
        //     Other Latitude
        //     Name: OtherLatitude
        //     SF Type: double
        //     Nillable: True
        [JsonProperty(PropertyName = "otherLatitude")]
        public double? OtherLatitude { get; set; }
        //
        // Resumo:
        //     ReferenceTo: Individual
        //     RelationshipName: Individual
        [Createable(false)]
        [JsonProperty(PropertyName = "individual")]
        [Updateable(false)]
        public SfIndividual Individual { get; set; }
    }
}