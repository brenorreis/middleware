﻿using System;
using Newtonsoft.Json;
using System.Reflection;
using NetCoreForce.Models;
using System.Collections.Generic;
using NetCoreForce.Client.Models;

namespace SfCustom.Models
{
    public partial class SfCustomContact : SfContactBase
    {
        public SfCustomContact()
        {
        }

        [JsonProperty(PropertyName = "CPF__c")]
        public string CPF { get; set; }

        [JsonProperty(PropertyName = "CardCode__c")]
        public string CardCode { get; set; }
        
        public static String BuildSelect()
        {
            List<String> retorno = new List<string>();

            List<String> removerCampos = new List<string>{"otherCity","otherState","otherPostalCode","otherCountry","otherLongitude","otherGeocodeAccuracy","otherAddress","mailingStreet","mailingCity","mailingState","mailingPostalCode","mailingCountry","mailingLatitude","mailingLongitude","mailingGeocodeAccuracy","mailingAddress","otherLatitude","individualId","individual","otherStreet", "account", "reportsTo", "masterRecord", "masterRecordId", "parentId", "parent", "owner", "createdBy", "lastModifiedBy", "cleanStatus", "dunsNumber", "tradestyle", "naicsCode", "naicsDesc", "yearStarted", "dandbCompanyId", "dandbCompany", "attributes", "billingStreet","billingCity", "billingState", "billingPostalCode", "billingCountry", "billingLatitude", "billingLongitude", "billingGeocodeAccuracy", "billingAddress", "shippingStreet", "shippingCity", "shippingState", "shippingPostalCode", "shippingCountry", "shippingLatitude", "shippingLongitude", "shippingGeocodeAccuracy", "shippingAddress"};

            PropertyInfo[] props = typeof(SfCustomContact).GetProperties();

            foreach (PropertyInfo prop in props)
            {
                object[] attrs = prop.GetCustomAttributes(true);
                foreach (object attr in attrs)
                {
                    JsonPropertyAttribute jSonAttr = attr as JsonPropertyAttribute;
                    if (jSonAttr != null)
                    {
                        retorno.Add(jSonAttr.PropertyName);
                    }
                }
            }

            foreach (String campo in removerCampos)
            {            
                retorno.Remove(campo);
            }

            return $"Select {String.Join(", ", retorno)} From Contact ";
        }

        public static implicit operator CreateResponse(SfCustomContact v)
        {
            throw new NotImplementedException();
        }

    }
}