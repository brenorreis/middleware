﻿using System;
using Newtonsoft.Json;

namespace SfCustom.Models
{
    public partial class SfCustomNotaFiscal : SfCustomObject
    {
        public SfCustomNotaFiscal()
        {
        }

        [JsonIgnore]
        public static string SObjectTypeName 
        {
            get 
            {
                return "Nota_Fiscal__c";
            } 
        }

        [JsonProperty(PropertyName = "Id_Externo__c")]
        public string Id_Externo__c { get; set; }

        [JsonProperty(PropertyName = "Conta__c")]
        public string Conta__c { get; set; }

        [JsonProperty(PropertyName = "Data_emissao_NF__c")]
        public DateTime? Data_emissao_NF__c { get; set; }

        [JsonProperty(PropertyName = "Oportunidade__c")]
        public string Oportunidade__c { get; set; }

    }
}