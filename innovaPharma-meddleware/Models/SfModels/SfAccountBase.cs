using System;
using Newtonsoft.Json;
using NetCoreForce.Client.Attributes;
using NetCoreForce.Client.Models;

namespace NetCoreForce.Models
{
    public class SfAccountBase : SObject
    {
        public static string SObjectTypeName { get; }

        [JsonProperty(PropertyName = "annualRevenue")]
        public decimal? AnnualRevenue { get; set; }
        //
        // Summary:
        //     Employees
        //     Name: NumberOfEmployees
        //     SF Type: int
        //     Nillable: True
        [JsonProperty(PropertyName = "numberOfEmployees")]
        public int? NumberOfEmployees { get; set; }
        
        //
        // Summary:
        //     Ticker Symbol
        //     Name: TickerSymbol
        //     SF Type: string
        //     Nillable: True
        [JsonProperty(PropertyName = "tickerSymbol")]
        public string TickerSymbol { get; set; }
        //
        // Summary:
        //     Account Description
        //     Name: Description
        //     SF Type: textarea
        //     Nillable: True
        [JsonProperty(PropertyName = "description")]
        public string Description { get; set; }
        //
        // Summary:
        //     Account Rating
        //     Name: Rating
        //     SF Type: picklist
        //     Nillable: True
        [JsonProperty(PropertyName = "rating")]
        public string Rating { get; set; }
        //
        // Summary:
        //     Account Site
        //     Name: Site
        //     SF Type: string
        //     Nillable: True
        [JsonProperty(PropertyName = "site")]
        public string Site { get; set; }
        //
        // Summary:
        //     Owner ID
        //     Name: OwnerId
        //     SF Type: reference
        //     Nillable: False
        [JsonProperty(PropertyName = "ownerId")]
        public string OwnerId { get; set; }
        //
        // Summary:
        //     Created Date
        //     Name: CreatedDate
        //     SF Type: datetime
        //     Nillable: False
        [Createable(false)]
        [JsonProperty(PropertyName = "createdDate")]
        [Updateable(false)]
        public DateTimeOffset? CreatedDate { get; set; }
        //
        // Summary:
        //     Created By ID
        //     Name: CreatedById
        //     SF Type: reference
        //     Nillable: False
        [Createable(false)]
        [JsonProperty(PropertyName = "createdById")]
        [Updateable(false)]
        public string CreatedById { get; set; }
        //
        // Summary:
        //     Last Modified Date
        //     Name: LastModifiedDate
        //     SF Type: datetime
        //     Nillable: False
        [Createable(false)]
        [JsonProperty(PropertyName = "lastModifiedDate")]
        [Updateable(false)]
        public DateTimeOffset? LastModifiedDate { get; set; }
        //
        // Summary:
        //     Last Modified By ID
        //     Name: LastModifiedById
        //     SF Type: reference
        //     Nillable: False
        [Createable(false)]
        [JsonProperty(PropertyName = "lastModifiedById")]
        [Updateable(false)]
        public string LastModifiedById { get; set; }
        //
        // Summary:
        //     System Modstamp
        //     Name: SystemModstamp
        //     SF Type: datetime
        //     Nillable: False
        [Createable(false)]
        [JsonProperty(PropertyName = "systemModstamp")]
        [Updateable(false)]
        public DateTimeOffset? SystemModstamp { get; set; }
        //
        // Summary:
        //     Last Activity
        //     Name: LastActivityDate
        //     SF Type: date
        //     Nillable: True
        [Createable(false)]
        [JsonProperty(PropertyName = "lastActivityDate")]
        [Updateable(false)]
        public DateTime? LastActivityDate { get; set; }
        //
        // Summary:
        //     Last Viewed Date
        //     Name: LastViewedDate
        //     SF Type: datetime
        //     Nillable: True
        [Createable(false)]
        [JsonProperty(PropertyName = "lastViewedDate")]
        [Updateable(false)]
        public DateTimeOffset? LastViewedDate { get; set; }
        //
        // Summary:
        //     Last Referenced Date
        //     Name: LastReferencedDate
        //     SF Type: datetime
        //     Nillable: True
        [Createable(false)]
        [JsonProperty(PropertyName = "lastReferencedDate")]
        [Updateable(false)]
        public DateTimeOffset? LastReferencedDate { get; set; }
        //
        // Summary:
        //     Data.com Key
        //     Name: Jigsaw
        //     SF Type: string
        //     Nillable: True
        [JsonProperty(PropertyName = "jigsaw")]
        public string Jigsaw { get; set; }
        //
        // Summary:
        //     Jigsaw Company ID
        //     Name: JigsawCompanyId
        //     SF Type: string
        //     Nillable: True
        [Createable(false)]
        [JsonProperty(PropertyName = "jigsawCompanyId")]
        [Updateable(false)]
        public string JigsawCompanyId { get; set; }
        //
        // Summary:
        //     Account Source
        //     Name: AccountSource
        //     SF Type: picklist
        //     Nillable: True
        [JsonProperty(PropertyName = "accountSource")]
        public string AccountSource { get; set; }
        //
        // Summary:
        //     SIC Description
        //     Name: SicDesc
        //     SF Type: string
        //     Nillable: True
        [JsonProperty(PropertyName = "sicDesc")]
        public string SicDesc { get; set; }
        //
        // Summary:
        //     Industry
        //     Name: Industry
        //     SF Type: picklist
        //     Nillable: True
        [JsonProperty(PropertyName = "industry")]
        public string Industry { get; set; }
        //
        // Summary:
        //     SIC Code
        //     Name: Sic
        //     SF Type: string
        //     Nillable: True
        [JsonProperty(PropertyName = "sic")]
        public string Sic { get; set; }
        //
        // Summary:
        //     Photo URL
        //     Name: PhotoUrl
        //     SF Type: url
        //     Nillable: True
        [Createable(false)]
        [JsonProperty(PropertyName = "photoUrl")]
        [Updateable(false)]
        public string PhotoUrl { get; set; }
        //
        // Summary:
        //     Website
        //     Name: Website
        //     SF Type: url
        //     Nillable: True
        [JsonProperty(PropertyName = "website")]
        public string Website { get; set; }
        //
        // Summary:
        //     Account ID
        //     Name: Id
        //     SF Type: id
        //     Nillable: False
        [Createable(false)]
        [JsonProperty(PropertyName = "id")]
        [Updateable(false)]
        public string Id { get; set; }
        //
        // Summary:
        //     Deleted
        //     Name: IsDeleted
        //     SF Type: boolean
        //     Nillable: False
        [Createable(false)]
        [JsonProperty(PropertyName = "isDeleted")]
        [Updateable(false)]
        public bool? IsDeleted { get; set; }
        //
        // Summary:
        //     Account Name
        //     Name: Name
        //     SF Type: string
        //     Nillable: False
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        //
        // Summary:
        //     Account Type
        //     Name: Type
        //     SF Type: picklist
        //     Nillable: True
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }
        //
        // Summary:
        //     Account Phone
        //     Name: Phone
        //     SF Type: phone
        //     Nillable: True
        [JsonProperty(PropertyName = "phone")]
        public string Phone { get; set; }
        //
        // Summary:
        //     Account Fax
        //     Name: Fax
        //     SF Type: phone
        //     Nillable: True
        [JsonProperty(PropertyName = "fax")]
        public string Fax { get; set; }
        //
        // Summary:
        //     Account Number
        //     Name: AccountNumber
        //     SF Type: string
        //     Nillable: True
        [JsonProperty(PropertyName = "accountNumber")]
        public string AccountNumber { get; set; }
    }
}