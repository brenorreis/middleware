﻿using Newtonsoft.Json;
using NetCoreForce.Client.Models;

namespace SfCustom.Models
{
    public partial class SfContentNote : SObject
    {
        public SfContentNote()
        {
        }

        [JsonIgnore]
        public static string SObjectTypeName { get; }

        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "Title")]
        public string Title { get; set; }
        
        [JsonProperty(PropertyName = "TextPreview")]
        public string TextPreview { get; set; }
    }
}
