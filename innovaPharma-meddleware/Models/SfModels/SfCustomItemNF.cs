﻿using Newtonsoft.Json;

namespace SfCustom.Models
{
    public partial class SfCustomItemNF : SfCustomObject    
    {
        public SfCustomItemNF()
        {
        }
        
        [JsonIgnore]
        public static string SObjectTypeName 
        {
            get 
            {
                return "Item_da_Nota_Fiscal__c";
            } 
        }

        [JsonProperty(PropertyName = "Contribuicao__c")]
        public decimal? Contribuicao__c { get; set; }

        [JsonProperty(PropertyName = "Devolucao__c")]
        public decimal? Devolucao__c { get; set; }

        [JsonProperty(PropertyName = "Imposto__c")]
        public decimal? Imposto__c { get; set; }

        [JsonProperty(PropertyName = "Quantidade__c")]
        public decimal? Quantidade__c { get; set; }

        [JsonProperty(PropertyName = "Receita_Liquida__c")]
        public decimal? Receita_Liquida__c { get; set; }

        [JsonProperty(PropertyName = "Valor_Liquido__c")]
        public decimal? Valor_Liquido__c { get; set; }

        [JsonProperty(PropertyName = "Valor_unitario__c")]
        public decimal? Valor_unitario__c { get; set; }

        [JsonProperty(PropertyName = "Custo_Mercadoria_Vendida__c")]
        public decimal? Custo_Mercadoria_Vendida__c { get; set; }

        [JsonProperty(PropertyName = "Id_Externo__c")]
        public string Id_Externo__c { get; set; }

        [JsonProperty(PropertyName = "Nota_Fiscal__c")]
        public string Nota_Fiscal__c { get; set;}

        [JsonProperty(PropertyName = "Nota_Fiscal__r")]
        public SfCustomNotaFiscal NotaFiscal { get; set; }

        [JsonProperty(PropertyName = "Produto__c")]
        public string Produto__c { get; set; }
    }
}