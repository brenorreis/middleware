﻿using Newtonsoft.Json;

namespace SfCustom.Models
{
    public partial class SfTransportadora : SfCustomObject
    {
        public SfTransportadora()
        {
        }

        [JsonIgnore]
        public static string SObjectTypeName { 
            get 
            {
                return "Transportadora__c";
            } 
        }

        [JsonProperty(PropertyName = "CardCode__c")]
        public string CardCode { get; set; }
    }
}
