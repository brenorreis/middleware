﻿using Newtonsoft.Json;

namespace SfCustom.Models
{
    public partial class SfSegmento : SfCustomObject
    {
        public SfSegmento()
        {
        }

        [JsonIgnore]
        public static string SObjectTypeName { 
            get 
            {
                return "Segmento__c";
            } 
        }

        [JsonProperty(PropertyName = "Codigo__c")]
        public string Codigo { get; set; }
    }
}
