using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using NetCoreForce.Client.Models;
using NetCoreForce.Models;
using Newtonsoft.Json;

namespace SfCustom.Models
{
    public partial class SfCustomAccount : SfAccount
    {
        public SfCustomAccount()
        {
        }
        
        [JsonProperty(PropertyName = "Id_Int__c")]
        public decimal? IdDec { get; set; }
        [JsonIgnore]
        public int IdInt => (int) (IdDec??0);
        
        [JsonProperty(PropertyName = "CardCode__c")]
        public string CardCode { get; set; }

        [JsonProperty(PropertyName = "Sincronizado__c")]
        public Boolean Sincronizado { get; set; }

        [JsonProperty(PropertyName = "Status_SAP__c")]
        public string StatusSAP { get; set; }
        
        [JsonProperty(PropertyName = "Limite_de_Credito__c")]
        public decimal? LimiteCredito { get; set; }
        
        [JsonProperty(PropertyName = "Saldo__c")]
        public decimal? Saldo { get; set; }
        
        [JsonProperty(PropertyName = "Saldo_SAP__c")]
        public decimal? SaldoSAP { get; set; }

        [JsonProperty(PropertyName = "Celular__c")]
        public string Celular { get; set; }
        
        [JsonProperty(PropertyName = "PersonMobilePhone")]
        public string CelularPessoal { get; set; }
        
        [JsonProperty(PropertyName = "N_do_Alvara__c")]
        public string NrAlvara { get; set; }
        
        [JsonProperty(PropertyName = "CNPJ_CPF__c")]
        public string CNPJCPF { get; set; }

        [JsonProperty(PropertyName = "SlpCode__c")]
        public decimal? SlpCodeSf { get; set; }
        
        [JsonIgnore]
        public int SlpCode => (int) (SlpCodeSf??0);

        [JsonProperty(PropertyName = "Nome_Fantasia__c")]
        public string Fantasia { get; set; }
        
        [JsonProperty(PropertyName = "Inscricao_Suframa__c")]
        public string Suframa { get; set; }
        
        [JsonProperty(PropertyName = "Subtituto_Tributario__c")]
        public string SubstitudoTributario { get; set; }
        
        [JsonProperty(PropertyName = "Inscricao_Estadual__c")]
        public string InscricaoEstadual { get; set; }
        
        [JsonProperty(PropertyName = "Tipo_IE__c")]
        public string TipoIE { get; set; }
        
        [JsonProperty(PropertyName = "Tipo_IE_INT__c")]
        public decimal? TipoIEIntSf { get; set; }
        [JsonIgnore]
        public int TipoIEInt => (int) (TipoIEIntSf??0);
        
        [JsonProperty(PropertyName = "Grupo_de_Clientes__c")]
        public string GrupoCliente { get; set; }
        
        [JsonProperty(PropertyName = "Inscricao_Municipal__c")]
        public string InscricaoMunicipal { get; set; }
        
        [JsonProperty(PropertyName = "Email_xML__c")]
        public string EmailXML { get; set; }
        
        [JsonProperty(PropertyName = "PersonEmail")]
        public string Email { get; set; }
        
        [JsonProperty(PropertyName = "RecordTypeId")]
        public string TipoDeRegistro { get; set; }
        
        [JsonProperty(PropertyName = "Tipo_de_Registro__c")]
        public decimal? TipoDeRegistroSf { get; set; }
        [JsonIgnore]
        public int TipoDeRegistroInt => (int) (TipoDeRegistroSf??0);
        
        [JsonProperty(PropertyName = "Observacoes_Gestor__c")]
        public string ObservacoesGestor { get; set; }
        
        [JsonProperty(PropertyName = "Observacoes_Vendedor__c")]
        public string ObservacoesVendedor { get; set; }
        
        [JsonProperty(PropertyName = "FirstName")]
        public string PrimeiroNome { get; set; }
        
        [JsonProperty(PropertyName = "MiddleName")]
        public string MeioNome { get; set; }
        
        [JsonProperty(PropertyName = "LastName")]
        public string Sobrenome { get; set; }
        
        [JsonProperty(PropertyName = "Rede__c")]
        public string Rede { get; set; }
        
        [JsonProperty(PropertyName = "Segmento__c")]
        public string Segmento { get; set; }
        
        [JsonProperty(PropertyName = "Segmento_Code__c")]
        
        public string SegmentoCod { get; set; }
        
        [JsonProperty(PropertyName = "Especialidade__c")]
        public string Especialidade { get; set; }


        [JsonProperty(PropertyName = "Nome_Especialidade__c")]
        public string EspecialidadeNome { get; set; }
        
        [JsonProperty(PropertyName = "Codigo_Especialidade__c")]
        public string EspecialidadeCode { get; set; }
        
        [JsonProperty(PropertyName = "Data_de_Nascimento__pc")]
        public DateTimeOffset? DataNascimento { get; set; }
        
        [JsonProperty(PropertyName = "Data_Alvara__c")]
        public DateTimeOffset? DataAlvara { get; set; }
        
        [JsonProperty(PropertyName = "Data_ultima_compra__c")]
        public DateTimeOffset? DataUltimaCompra { get; set; }
        
        [JsonProperty(PropertyName = "N_Registro_de_Conselho__pc")]
        public string NRegistroConselho { get; set; }
        
        [JsonProperty(PropertyName = "RG__pc")]
        public string RG { get; set; }
        
        [JsonProperty(PropertyName = "RG_UF__pc")]
        public string UfRg { get; set; }
        
        [JsonProperty(PropertyName = "Orgao_Expedidor__pc")]
        public string OrgExp { get; set; }
        
        [JsonProperty(PropertyName = "Referencias_Comerciais__c")]
        public string ReferenciasComerciais { get; set; }
        
        [JsonProperty(PropertyName = "Referencias_Bancarias__c")]
        public string ReferenciasBancarias { get; set; }
        
        [JsonProperty(PropertyName = "Status_Serasa__c")]
        public string StatusSerasa { get; set; }
        
        [JsonProperty(PropertyName = "Observacoes_Serasa__c")]
        public string ObservacoesSerasa { get; set; }

        [JsonProperty(PropertyName = "Data_Serasa__c")]
        public DateTimeOffset? DataSerasa { get; set; }
        
        [JsonProperty(PropertyName = "Indicador_da_IE__c")]
        public string Indicador_da_IE__c { get; set; }
        
        [JsonProperty(PropertyName = "Indicador_de_Natureza__c")]
        public string Indicador_de_Natureza__c { get; set; }
        
        [JsonProperty(PropertyName = "Indicador_de_Op_Consumidor__c")]
        public string Indicador_de_Op_Consumidor__c { get; set; }
        
        [JsonProperty(PropertyName = "Simples_Nacional__c")]
        public string Simples_Nacional__c { get; set; }
        
        [JsonProperty(PropertyName = "Medicamento__c")]
        public string Medicamento__c { get; set; }
        
        [JsonProperty(PropertyName = "Produto_Saude__c")]
        public string Produto_Saude__c { get; set; }
        
        [JsonProperty(PropertyName = "Situacao_IE__c")]
        public string SituacaoIE { get; set; }
        
        [JsonProperty(PropertyName = "Situacao_Cadastral_CNPJ__c")]
        public string SituacaoCadastralCnpj { get; set; }
        
        [JsonProperty(PropertyName = "Natureza_Juridica__c")]
        public string DescricaoNatureza { get; set; }
        
        [JsonProperty(PropertyName = "Capital_Social__c")]
        public decimal? CapitalSocial { get; set; }
        
        [JsonProperty(PropertyName = "Data_de_abertura_CNPJ__c")]
        public DateTimeOffset? DataAbertura { get; set; }
        
        [JsonProperty(PropertyName = "Categoria_de_Clientes__c")]
        public string CategoriaClientes { get; set; }
        
        [JsonProperty(PropertyName = "Pontos_Score__c")]
        public decimal? PontosScore { get; set; }
        
        [JsonProperty(PropertyName = "Porte_da_empresa__c")]
        public string PorteEmpresa { get; set; }
        
        [JsonProperty(PropertyName = "Transportadora__c")]
        public string Transportadora { get; set; }

        [JsonProperty(PropertyName = "Exclusao_LGPD__c")]
        public string ExclusaoLgpd { get; set; }

        [JsonProperty(PropertyName = "Aceita_email_marketing__c")]
        public string AceitaEmailMarketing { get; set; }

        [JsonProperty(PropertyName = "Consultor_T_cnico__c")]
        public string ConsultorTecnico { get; set; }

        [JsonProperty(PropertyName = "Distribuidor__c")]
        public string Distribuidor { get; set; }

        [JsonProperty(PropertyName = "VIP__c")]
        public Boolean VIP { get; set; }

        

        public static String BuildSelect()
        {
            List<String> retorno = new List<string>();

            List<String> removerCampos = new List<string>{"masterRecord", "masterRecordId", "parentId", "parent", "owner", "createdBy", "lastModifiedBy", "cleanStatus", "dunsNumber", "tradestyle", "naicsCode", "naicsDesc", "yearStarted", "dandbCompanyId", "dandbCompany", "attributes", "billingStreet","billingCity", "billingState", "billingPostalCode", "billingCountry", "billingLatitude", "billingLongitude", "billingGeocodeAccuracy", "billingAddress", "shippingStreet", "shippingCity", "shippingState", "shippingPostalCode", "shippingCountry", "shippingLatitude", "shippingLongitude", "shippingGeocodeAccuracy", "shippingAddress"};

            PropertyInfo[] props = typeof(SfCustomAccount).GetProperties();

            foreach (PropertyInfo prop in props)
            {
                object[] attrs = prop.GetCustomAttributes(true);
                foreach (object attr in attrs)
                {
                    JsonPropertyAttribute jSonAttr = attr as JsonPropertyAttribute;
                    if (jSonAttr != null)
                    {
                        retorno.Add(jSonAttr.PropertyName);
                    }
                }
            }

            foreach (String campo in removerCampos)
            {            
                retorno.Remove(campo);
            }

            return $"Select {String.Join(", ", retorno)} From Account ";
        }

        public static implicit operator CreateResponse(SfCustomAccount v)
        {
            throw new NotImplementedException();
        }
    }
}