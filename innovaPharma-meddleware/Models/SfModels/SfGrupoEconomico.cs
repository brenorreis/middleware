using System;
using System.Linq;
using Newtonsoft.Json;
using System.Reflection;
using NetCoreForce.Models;
using NetCoreForce.Client.Models;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SfCustom.Models
{
    public partial class SfGrupoEconomico : SfCustomObject
    {
        public SfGrupoEconomico(){}

        [JsonIgnore]
        public static string SObjectTypeName 
        {
            get 
            {
                return "Grupo_economico__c";
            } 
        }
        
        [JsonProperty(PropertyName = "Conta__c")]
        public string Conta { get; set; }
        
        [JsonProperty(PropertyName = "Socio__c")]
        public string Socio { get; set; }
        
        [JsonProperty(PropertyName = "do_Capital__c")]
        public decimal? percCapital { get; set; }
        
        [JsonProperty(PropertyName = "Capital_Social__c")]
        public decimal? CapitalSocial { get; set; }
        
        [JsonProperty(PropertyName = "Data_de_entrada__c")]
        public DateTimeOffset? DataEntrada { get; set; }
        
        [JsonProperty(PropertyName = "Nome_do_socio__c")]
        public string Nome_do_Socio { get; set; }
        
        [JsonProperty(PropertyName = "Documento__c")]
        public string Documento { get; set; }
        
        [JsonProperty(PropertyName = "Observacoes__c")]
        public string Observacoes { get; set; }
        
        [JsonProperty(PropertyName = "Id_Externo__c")]
        public string IdExterno { get; set; }

        public static String BuildSelect()
        {
            List<String> retorno = new List<string>();

            List<String> removerCampos = new List<string>{"masterRecord", "masterRecordId", "parentId", "parent", "owner", "createdBy", "lastModifiedBy", "cleanStatus", "dunsNumber", "tradestyle", "naicsCode", "naicsDesc", "yearStarted", "dandbCompanyId", "dandbCompany", "attributes", "billingStreet","billingCity", "billingState", "billingPostalCode", "billingCountry", "billingLatitude", "billingLongitude", "billingGeocodeAccuracy", "billingAddress", "shippingStreet", "shippingCity", "shippingState", "shippingPostalCode", "shippingCountry", "shippingLatitude", "shippingLongitude", "shippingGeocodeAccuracy", "shippingAddress"};

            PropertyInfo[] props = typeof(SfGrupoEconomico).GetProperties();

            foreach (PropertyInfo prop in props)
            {
                object[] attrs = prop.GetCustomAttributes(true);
                foreach (object attr in attrs)
                {
                    JsonPropertyAttribute jSonAttr = attr as JsonPropertyAttribute;
                    if (jSonAttr != null)
                    {
                        retorno.Add(jSonAttr.PropertyName);
                    }
                }
            }

            foreach (String campo in removerCampos)
            {            
                retorno.Remove(campo);
            }

            return $"Select {String.Join(", ", retorno)} From Grupo_economico__c ";
        }

        public static implicit operator CreateResponse(SfGrupoEconomico v)
        {
            throw new NotImplementedException();
        }
    }
}