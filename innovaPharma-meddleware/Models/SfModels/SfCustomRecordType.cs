﻿using Newtonsoft.Json;
using NetCoreForce.Models;

namespace SfCustom.Models
{
    public partial class SfCustomRecordType : SfRecordType
    {
        public SfCustomRecordType()
        {
        }
        
        [JsonProperty(PropertyName = "IsPersonType")]
        public bool IsPersonType { get; set; }
    }
}
