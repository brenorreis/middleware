﻿using System;
using Newtonsoft.Json;
using System.Reflection;
using NetCoreForce.Models;
using System.Collections.Generic;

namespace SfCustom.Models
{
    public partial class SfCustomOpportunityLineItem : SfOpportunityLineItem    
    {
        public SfCustomOpportunityLineItem()
        {
        }

        [JsonProperty(PropertyName = "Codigo_do_Produto__c")]
        public string CodigoProduto { get; set; }

        [JsonProperty(PropertyName = "Empresa__c")]
        public string Empresa { get; set; }

        [JsonProperty(PropertyName = "Contribuicao__c")]
        public decimal? Contribuicao { get; set; }

        [JsonProperty(PropertyName = "Devolucao__c")]
        public decimal? Devolucao { get; set; }

        [JsonProperty(PropertyName = "Imposto__c")]
        public decimal? Imposto { get; set; }

        [JsonProperty(PropertyName = "Quantidade_faturada__c")]
        public decimal? QuantidadeFaturada { get; set; }

        [JsonProperty(PropertyName = "Receita_Liquida__c")]
        public decimal? ReceitaLiquida { get; set; }

        [JsonProperty(PropertyName = "Custo_Mercadoria_Vendida__c")]
        public decimal? CustoMercadoriaVendida { get; set; }

        [JsonProperty(PropertyName = "Codigo__c")]
        public decimal? CodigoSf { get; set; }

        [JsonProperty(PropertyName = "Valor_total_no_combo__c")]
        public decimal? ValorCombo { get; set; }

        [JsonIgnore]
        public int Codigo => (int) (CodigoSf??0);

                public static String BuildSelect()
        {
            List<String> retorno = new List<string>();

            List<String> removerCampos = new List<string>{"masterRecord", "masterRecordId", "parentId", "parent", "owner", "createdBy", "lastModifiedBy", "campaign", "pricebook2", "cleanStatus", "dunsNumber", "tradestyle", "naicsCode", "naicsDesc", "yearStarted", "dandbCompanyId", "dandbCompany", "attributes", "account", "probability", "opportunity", "pricebookEntry", "product2", "Codigo"};

            PropertyInfo[] props = typeof(SfCustomOpportunityLineItem).GetProperties();

            foreach (PropertyInfo prop in props)
            {
                object[] attrs = prop.GetCustomAttributes(true);
                foreach (object attr in attrs)
                {
                    JsonPropertyAttribute jSonAttr = attr as JsonPropertyAttribute;
                    if (jSonAttr != null)
                    {
                        retorno.Add(jSonAttr.PropertyName);
                    }
                }
            }

            foreach (String campo in removerCampos)
            {            
                retorno.Remove(campo);
            }

            return $"Select {String.Join(", ", retorno)} From OpportunityLineItem";
        }

    }
}