﻿using System;
using Newtonsoft.Json;
using System.Reflection;
using NetCoreForce.Models;
using System.Collections.Generic;
using NetCoreForce.Client.Models;

namespace SfCustom.Models
{
    public partial class SfCustomUser : SfUser
    {
        public SfCustomUser()
        {
        }
        
        [JsonProperty(PropertyName = "Codigo_do_Vendedor__c")]
        public decimal? CodigoVendedor { get; set; }

        public int CodigoVendedorInt => (int) (CodigoVendedor??0);
        
        [JsonProperty(PropertyName = "Codigo_Vendedor_Area_Medica__c")]
        public decimal? CodigoVendedorMed { get; set; }

        public int CodigoVendedorMadInt => (int) (CodigoVendedorMed??0);
        
        [JsonProperty(PropertyName = "Codigo_SAP__c")]
        public string CodigoSAP { get; set; }
        
        [JsonProperty(PropertyName = "Codigo_Emitente__c")]
        public string CodigoEmitente { get; set; }

        public static String BuildSelect()
        {
            List<String> retorno = new List<string>();

            List<String> removerCampos = new List<string>{"masterRecord", "masterRecordId", "parentId", "parent", "owner", "createdBy", "lastModifiedBy", "cleanStatus", "dunsNumber", "tradestyle", "naicsCode", "naicsDesc", "yearStarted", "dandbCompanyId", "dandbCompany", "attributes", "billingStreet","billingCity", "billingState", "billingPostalCode", "billingCountry", "billingLatitude", "billingLongitude", "billingGeocodeAccuracy", "billingAddress", "shippingStreet", "shippingCity", "shippingState", "shippingPostalCode", "shippingCountry", "shippingLatitude", "shippingLongitude", "shippingGeocodeAccuracy", "shippingAddress"};

            PropertyInfo[] props = typeof(SfCustomUser).GetProperties();

            foreach (PropertyInfo prop in props)
            {
                object[] attrs = prop.GetCustomAttributes(true);
                foreach (object attr in attrs)
                {
                    JsonPropertyAttribute jSonAttr = attr as JsonPropertyAttribute;
                    if (jSonAttr != null)
                    {
                        retorno.Add(jSonAttr.PropertyName);
                    }
                }
            }

            foreach (String campo in removerCampos)
            {            
                retorno.Remove(campo);
            }

            return $"Select {String.Join(", ", retorno)} From User";
        }

        public static implicit operator CreateResponse(SfCustomUser v)
        {
            throw new NotImplementedException();
        }
    }
}