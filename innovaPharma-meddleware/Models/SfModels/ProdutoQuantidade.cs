﻿using System.ComponentModel.DataAnnotations;

namespace SfCustom.Models
{
    public partial class ProdutoQuantidade
    {
        public ProdutoQuantidade()
        {
        }
        
        public string IdProduto { get; set; }
        [Key]
        public string CodigoProduto { get; set; }
        public int? Quantidade { get; set; }
    }
}
