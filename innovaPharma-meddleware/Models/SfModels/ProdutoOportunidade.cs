﻿using System.ComponentModel.DataAnnotations;

namespace SfCustom.Models
{
    public partial class ProdutoOportunidade
    {
        public ProdutoOportunidade()
        {
        }
        
        [Key]
        public string Product2Id { get; set; }
        public string Quantidade { get; set; }
    }
}
