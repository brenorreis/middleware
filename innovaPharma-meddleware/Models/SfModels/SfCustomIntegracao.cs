﻿using System;
using Newtonsoft.Json;
using System.Reflection;
using System.Collections.Generic;

namespace SfCustom.Models
{
    public partial class SfCustomIntegracao : SfCustomObject
    {
        public SfCustomIntegracao()
        {
        }

        [JsonIgnore]
        public static string SObjectTypeName 
        {
            get 
            {
                return "Integracao__c";
            } 
        }
        
        [JsonProperty(PropertyName = "Metodo__c")]
        public string Metodo { get; set; }
        
        [JsonProperty(PropertyName = "Mensagem__c")]
        public string Mensagem { get; set; }

        [JsonProperty(PropertyName = "Id_Externo__c")]
        public string IdExterno { get; set; }
        
        public static String BuildSelect()
        {
            List<String> retorno = new List<string>();

            List<String> removerCampos = new List<string>{"masterRecord", "masterRecordId", "parentId", "parent", "owner", "createdBy", "lastModifiedBy", "cleanStatus", "dunsNumber", "tradestyle", "naicsCode", "naicsDesc", "yearStarted", "dandbCompanyId", "dandbCompany", "attributes", "billingStreet","billingCity", "billingState", "billingPostalCode", "billingCountry", "billingLatitude", "billingLongitude", "billingGeocodeAccuracy", "billingAddress", "shippingStreet", "shippingCity", "shippingState", "shippingPostalCode", "shippingCountry", "shippingLatitude", "shippingLongitude", "shippingGeocodeAccuracy", "shippingAddress"};

            PropertyInfo[] props = typeof(SfCustomIntegracao).GetProperties();

            foreach (PropertyInfo prop in props)
            {
                object[] attrs = prop.GetCustomAttributes(true);
                foreach (object attr in attrs)
                {
                    JsonPropertyAttribute jSonAttr = attr as JsonPropertyAttribute;
                    if (jSonAttr != null)
                    {
                        retorno.Add(jSonAttr.PropertyName);
                    }
                }
            }

            foreach (String campo in removerCampos)
            {            
                retorno.Remove(campo);
            }

            return $"Select {String.Join(", ", retorno)} From Integracao__c ";
        }
    }
}
