﻿using Newtonsoft.Json;

namespace SfCustom.Models
{
    public partial class SfProduto : SfCustomObject
    {
        public SfProduto()
        {
        }

        [JsonIgnore]
        public static string SObjectTypeName { 
            get 
            {
                return "Produto__c";
            } 
        }

        [JsonProperty(PropertyName = "Codigo_do_Produto__c")]
        public string Codigo { get; set; }
    }
}
