﻿using System;
using Newtonsoft.Json;
using System.Reflection;
using System.Collections.Generic;
using NetCoreForce.Client.Models;

namespace SfCustom.Models
{
    public partial class SfEndereco : SfCustomObject
    {
        public SfEndereco()
        {
        }

        [JsonIgnore]
        public static string SObjectTypeName 
        {
            get 
            {
                return "Endereco__c";
            } 
        }

        [JsonProperty(PropertyName = "CardCode__c")]
        public string CardCode { get; set; }

        [JsonProperty(PropertyName = "CardCodeVis__c")]
        public string CardCodeVis { get; set; }

        [JsonProperty(PropertyName = "Estado__c")]
        public string Estado { get; set; }
        
        [JsonProperty(PropertyName = "Numero__c")]
        public string Numero { get; set; }

        [JsonProperty(PropertyName = "Bairro__c")]
        public string Bairro { get; set; }
        
        [JsonProperty(PropertyName = "Codigo_Cidade__c")]
        public string CodigoCidade { get; set; }

        [JsonProperty(PropertyName = "CEP__c")]
        public string CEP { get; set; }

        [JsonProperty(PropertyName = "Cidade__c")]
        public string Cidade { get; set; }

        [JsonProperty(PropertyName = "Cidade_Pesquisa__c")]
        public string CidadePesquisa { get; set; }

        [JsonProperty(PropertyName = "Complemento__c")]
        public string Complemento { get; set; }

        [JsonProperty(PropertyName = "Conta__c")]
        public string Conta { get; set; }

        [JsonProperty(PropertyName = "Rua__c")]
        public string Rua { get; set; }
        
        [JsonProperty(PropertyName = "Tipo__c")]
        public string Tipo { get; set; }
        
        [JsonProperty(PropertyName = "Conta__r")]
        public SfCustomAccount ContaIdExterno { get; set; }

        public static String BuildSelect()
        {
            List<String> retorno = new List<string>();

            List<String> removerCampos = new List<string>{"Conta__r", "masterRecord", "masterRecordId", "parentId", "parent", "owner", "createdBy", "lastModifiedBy", "cleanStatus", "dunsNumber", "tradestyle", "naicsCode", "naicsDesc", "yearStarted", "dandbCompanyId", "dandbCompany", "attributes", "billingStreet","billingCity", "billingState", "billingPostalCode", "billingCountry", "billingLatitude", "billingLongitude", "billingGeocodeAccuracy", "billingAddress", "shippingStreet", "shippingCity", "shippingState", "shippingPostalCode", "shippingCountry", "shippingLatitude", "shippingLongitude", "shippingGeocodeAccuracy", "shippingAddress"};

            PropertyInfo[] props = typeof(SfEndereco).GetProperties();

            foreach (PropertyInfo prop in props)
            {
                object[] attrs = prop.GetCustomAttributes(true);
                foreach (object attr in attrs)
                {
                    JsonPropertyAttribute jSonAttr = attr as JsonPropertyAttribute;
                    if (jSonAttr != null)
                    {
                        retorno.Add(jSonAttr.PropertyName);
                    }
                }
            }

            foreach (String campo in removerCampos)
            {            
                retorno.Remove(campo);
            }

            return $"Select {String.Join(", ", retorno)} From Endereco__c ";
        }

        public static implicit operator CreateResponse(SfEndereco v)
        {
            throw new NotImplementedException();
        }
    }
}
