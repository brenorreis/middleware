﻿using System;
using Newtonsoft.Json;
using System.Reflection;
using NetCoreForce.Models;
using System.Collections.Generic;
using NetCoreForce.Client.Models;

namespace SfCustom.Models
{
    public partial class SfCustomHistoryOpportunity : SfOpportunityHistory
    {
        public SfCustomHistoryOpportunity()
        {
        }

        [JsonProperty(PropertyName = "opportunity")]
        public SfCustomOpportunity CustomOpportunity { get; set; }
        
        [JsonProperty(PropertyName = "Opportunity.Codigo__c")]
        public string Codigo { get; set; }

        [JsonProperty(PropertyName = "Opportunity.Empresa__c")]
        public string Empresa { get; set; }

        [JsonProperty(PropertyName = "Opportunity.Condicao__c")]
        public string Condicao { get; set; }
        

        public static String BuildSelect()
        {
            List<String> retorno = new List<string>();

            List<String> removerCampos = new List<string>{"opportunity", "opportunity","masterRecord", "masterRecordId", "parentId", "parent", "owner", "createdBy", "lastModifiedBy", "cleanStatus", "dunsNumber", "tradestyle", "naicsCode", "naicsDesc", "yearStarted", "dandbCompanyId", "dandbCompany", "attributes", "billingStreet","billingCity", "billingState", "billingPostalCode", "billingCountry", "billingLatitude", "billingLongitude", "billingGeocodeAccuracy", "billingAddress", "shippingStreet", "shippingCity", "shippingState", "shippingPostalCode", "shippingCountry", "shippingLatitude", "shippingLongitude", "shippingGeocodeAccuracy", "shippingAddress"};

            PropertyInfo[] props = typeof(SfCustomHistoryOpportunity).GetProperties();

            foreach (PropertyInfo prop in props)
            {
                object[] attrs = prop.GetCustomAttributes(true);
                foreach (object attr in attrs)
                {
                    JsonPropertyAttribute jSonAttr = attr as JsonPropertyAttribute;
                    if (jSonAttr != null)
                    {
                        retorno.Add(jSonAttr.PropertyName);
                    }
                }
            }

            foreach (String campo in removerCampos)
            {            
                retorno.Remove(campo);
            }

            return $"Select {String.Join(", ", retorno)} From OpportunityHistory ";
        }

        public static implicit operator CreateResponse(SfCustomHistoryOpportunity v)
        {
            throw new NotImplementedException();
        }
    }
}