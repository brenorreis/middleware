﻿using System;
using Newtonsoft.Json;
using System.Reflection;
using NetCoreForce.Models;
using System.Collections.Generic;
using NetCoreForce.Client.Models;

namespace SfCustom.Models
{
    public partial class SfAprovacao : SfCustomObject
    {
        public SfAprovacao()
        {
        }

        [JsonIgnore]
        public static string SObjectTypeName 
        {
            get 
            {
                return "Aprovacao__c";
            }
        }

        [JsonProperty(PropertyName = "Conta__c")]
        public string Conta__c { get; set; }

        [JsonProperty(PropertyName = "Nome_da_conta__c")]
        public string Nome_da_conta__c { get; set; }

        [JsonProperty(PropertyName = "CardCode__c")]
        public string CardCode__c { get; set; }
        
        [JsonProperty(PropertyName = "Data_da_Solicitacao__c")]
        public DateTime? Data_da_Solicitacao__c { get; set; }

        [JsonProperty(PropertyName = "Data_da_resposta__c")]
        public DateTime? Data_da_resposta__c { get; set; }
        
        [JsonProperty(PropertyName = "Solicitante__c")]
        public string Solicitante__c { get; set; }

        [JsonProperty(PropertyName = "Nome_do_solicitante__c")]
        public string Nome_do_solicitante__c { get; set; }

        [JsonProperty(PropertyName = "Aprovador__c")]
        public string Aprovador__c { get; set; }

        [JsonProperty(PropertyName = "Nome_do_aprovador__c")]
        public string Nome_do_aprovador__c { get; set; }

        [JsonProperty(PropertyName = "Etapa__c")]
        public string Etapa__c { get; set; }

        [JsonProperty(PropertyName = "Status__c")]
        public string Status__c { get; set; }

        [JsonProperty(PropertyName = "Motivo_de_recusa__c")]
        public string Motivo_de_recusa__c { get; set; }

        [JsonProperty(PropertyName = "Comentario__c")]
        public string Comentario__c { get; set; }

        [JsonProperty(PropertyName = "Tempo_decorrido_dias__c")]
        public decimal? Tempo_decorrido_dias__c { get; set; }        
        
        [JsonProperty(PropertyName = "Tempo_decorrido_horas__c")]
        public decimal? Tempo_decorrido_horas__c { get; set; }   

        [JsonProperty(PropertyName = "Tempo_decorrido_minutos__c")]
        public decimal? Tempo_decorrido_minutos__c { get; set; }

        [JsonProperty(PropertyName = "Tipo__c")]
        public string Tipo__c { get; set; }

        [JsonProperty(PropertyName = "Codigo_Oportunidade__c")]
        public string Codigo_Oportunidade__c { get; set; }  

        [JsonProperty(PropertyName = "Codigo_Solicitante__c")]
        public string Codigo_Solicitante__c { get; set; }   

        [JsonProperty(PropertyName = "Segmento__c")]
        public string Segmento__c { get; set; }   

        [JsonProperty(PropertyName = "Tipo_da_Oportunidade__c")]
        public string Tipo_da_Oportunidade__c { get; set; }       

        [JsonProperty(PropertyName = "Codigo_SAP__c")]
        public string Codigo_SAP__c { get; set; }
        
        [JsonProperty(PropertyName = "Oportunidade__r")]
        public SfCustomOpportunity CustomOpportunity { get; set; }

        [JsonProperty(PropertyName = "Oportunidade__r.Empresa__c")]
        public string Empresa { get; set; }

        [JsonProperty(PropertyName = "Oportunidade__r.Condicao__c")]
        public string Condicao { get; set; }

        public static String BuildSelect()
        {
            List<String> retorno = new List<string>();

            List<String> removerCampos = new List<string>{"opportunity", "Oportunidade__r","masterRecord", "masterRecordId", "parentId", "parent", "owner", "createdBy", "lastModifiedBy", "cleanStatus", "dunsNumber", "tradestyle", "naicsCode", "naicsDesc", "yearStarted", "dandbCompanyId", "dandbCompany", "attributes", "billingStreet","billingCity", "billingState", "billingPostalCode", "billingCountry", "billingLatitude", "billingLongitude", "billingGeocodeAccuracy", "billingAddress", "shippingStreet", "shippingCity", "shippingState", "shippingPostalCode", "shippingCountry", "shippingLatitude", "shippingLongitude", "shippingGeocodeAccuracy", "shippingAddress"};

            PropertyInfo[] props = typeof(SfAprovacao).GetProperties();

            foreach (PropertyInfo prop in props)
            {
                object[] attrs = prop.GetCustomAttributes(true);
                foreach (object attr in attrs)
                {
                    JsonPropertyAttribute jSonAttr = attr as JsonPropertyAttribute;
                    if (jSonAttr != null)
                    {
                        retorno.Add(jSonAttr.PropertyName);
                    }
                }
            }

            foreach (String campo in removerCampos)
            {            
                retorno.Remove(campo);
            }

            return $"Select {String.Join(", ", retorno)} From Aprovacao__c ";
        }

        public static implicit operator CreateResponse(SfAprovacao v)
        {
            throw new NotImplementedException();
        }
    }
}
