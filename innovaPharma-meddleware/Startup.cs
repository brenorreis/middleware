using System;
using Hangfire;
using Hangfire.SqlServer;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using InnovaPharmaMiddleware.Models;
using InnovaPharmaMiddleware.Helpers;
using Microsoft.Extensions.Configuration;
using innovaPharma_meddleware.Controllers;
using Microsoft.Extensions.DependencyInjection;

namespace innovaPharma_meddleware
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            Env = env;
        }

        public IConfiguration Configuration { get; }
        public IWebHostEnvironment Env {get;}

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<SalesforceController>();

            services.AddScoped<ProdutoHelper>();

            var cnn = Configuration.GetConnectionString("InnovaConnection");

            services.AddDbContext<InnovaDbContext>(options => options.UseSqlServer(cnn, sqlServerOptions => sqlServerOptions.CommandTimeout(1000)));

            services.AddMemoryCache();
            services.AddAuthorization();
            services.AddControllers();
            services.AddSwaggerGen();

            if (!Env.IsDevelopment())
            {
                services.AddHangfire(configuration => configuration
                .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                .UseSimpleAssemblyNameTypeSerializer()
                .UseRecommendedSerializerSettings()
                .UseSqlServerStorage(cnn, new SqlServerStorageOptions
                {
                    CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                    SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                    QueuePollInterval = TimeSpan.Zero,
                    UseRecommendedIsolationLevel = true,
                    UsePageLocksOnDequeue = true,
                    DisableGlobalLocks = true,
                }));

                services.AddHangfireServer();
            }
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "innovaPharma_meddleware v1"));
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHangfireDashboard();
                app.UseHsts();
                
                RecurringJob.AddOrUpdate<SalesforceController>( c => c.GetAprovacoes(), "0 * * * *", TimeZoneInfo.Local);
                RecurringJob.AddOrUpdate<SalesforceController>( c => c.SincronizarNFs(), "*/12 * * * *", TimeZoneInfo.Local);
                RecurringJob.AddOrUpdate<SalesforceController>( c => c.SincronizarHistorico(), "5 * * * *", TimeZoneInfo.Local);
                RecurringJob.AddOrUpdate<SalesforceController>( c => c.SincronizarProduto(), "*/11 * * * *", TimeZoneInfo.Local);
                RecurringJob.AddOrUpdate<SalesforceController>( c => c.UpsertEquipeContas(), "*/30 * * * *", TimeZoneInfo.Local);
                RecurringJob.AddOrUpdate<SalesforceController>( c => c.UpsertGrupoEconomico(), "7 */2 * * *", TimeZoneInfo.Local);
                RecurringJob.AddOrUpdate<SalesforceController>( c => c.SincronizarClientes(), "*/13 * * * *", TimeZoneInfo.Local);
                RecurringJob.AddOrUpdate<SalesforceController>( c => c.SincronizarPagamentos(), "2 */3 * * *", TimeZoneInfo.Local);
                RecurringJob.AddOrUpdate<SalesforceController>( c => c.SincronizarClienteNovo(), "*/20 * * * *", TimeZoneInfo.Local);
                RecurringJob.AddOrUpdate<SalesforceController>( c => c.SincronizarHistoricoFases(), "0 * * * *", TimeZoneInfo.Local);
                RecurringJob.AddOrUpdate<SalesforceController>( c => c.SincronizarStatusPedido(), "*/14 * * * *", TimeZoneInfo.Local);
                RecurringJob.AddOrUpdate<SalesforceController>( c => c.SincronizarOportunidades(), "*/12 * * * *", TimeZoneInfo.Local);
                RecurringJob.AddOrUpdate<SalesforceController>( c => c.DeletarPagamentosCancelados(), "3 */3 * * *", TimeZoneInfo.Local);
                RecurringJob.AddOrUpdate<SalesforceController>( c => c.SincronizarOportunidadesPIX(), "*/14 * * * *", TimeZoneInfo.Local);
                RecurringJob.AddOrUpdate<SalesforceController>( c => c.SincronizarStatusOportunidade(), "*/10 * * * *", TimeZoneInfo.Local);
                RecurringJob.AddOrUpdate<SalesforceController>( c => c.SincronizarOportunidadesCartao(), "*/15 * * * *", TimeZoneInfo.Local);
                RecurringJob.AddOrUpdate<SalesforceController>( c => c.RessincronizarOportunidadesPIX(), "*/25 * * * *", TimeZoneInfo.Local);
                RecurringJob.AddOrUpdate<SalesforceController>( c => c.DeletarPagamentosCanceladosMensal(), "0 3 * * *", TimeZoneInfo.Local);
                RecurringJob.AddOrUpdate<SalesforceController>( c => c.RessincronizarOportunidadesCartao(), "*/5 * * * *", TimeZoneInfo.Local);
                RecurringJob.AddOrUpdate<SalesforceController>( c => c.RessincronizarOportunidadesExportadas(), "*/10 * * * *", TimeZoneInfo.Local);
                
            }

            //app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
