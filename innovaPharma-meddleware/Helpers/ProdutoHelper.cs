using System.Linq;
using System.Collections.Generic;
using InnovaPharmaMiddleware.Models;

namespace InnovaPharmaMiddleware.Helpers
{
    public class ProdutoHelper
    {
        public List<Itens_VDM> ProdutosUtilizadosCache = null;

        public List<Familia_VDM> FamiliaCache = null;

        private InnovaDbContext innovaDbContext;

        public ProdutoHelper() : this(new InnovaDbContext())
        {
        }

        public ProdutoHelper(InnovaDbContext context)
        {
            innovaDbContext = context;

            InitializeProdutos();
        }

        private void InitializeProdutos()
        {
            FamiliaCache = innovaDbContext.Familia_VDM.ToList();
        }

        internal string GetFamilia(string u_Familia)
        {
            if(FamiliaCache.Any(f => f.Code == u_Familia))
            {
                return FamiliaCache.FirstOrDefault(f => f.Code == u_Familia).Name;
            }

            return null;
        }
    }
}