using NetCoreForce.Client;

namespace InnovaPharmaMiddleware.Helpers
{
    public static class SalesforceHelper
    {
        public  static ForceClient newClient() 
        {
            AuthenticationClient auth = new AuthenticationClient();

            auth.UsernamePassword(
            "3MVG9KsVczVNcM8zuwO6FjCC4pjmWFRVjbHT4Ez5rU6byYQYu4FQLtugfZbqCsCnGOJXIafRZMrrocV8RsDDJ",
            "2CF2E928F690A1C28276E3D1755A2264B0070382531564C6A19F2EADAE6BD771",
            "admin@innovapharma.com", 
            "vrs123456");

            return new ForceClient(auth.AccessInfo.InstanceUrl, auth.ApiVersion, auth.AccessInfo.AccessToken);
        }
    }
}
